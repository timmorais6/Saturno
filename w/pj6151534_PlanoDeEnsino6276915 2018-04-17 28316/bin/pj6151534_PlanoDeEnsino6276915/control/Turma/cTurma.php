<?php
/**
@author Artur Romao rocha
 */
$pasta="Turma"; 
require "model/Turma/mTurma.php";

class cTurma extends mTurma { 
    private $pasta = "Turma";
    public function __construct() {
        $this->model = new mTurma();
    }
    public function ivoke() {
        if (isset($_GET["visualizar"])) {
            $all = $this->ls($_GET["ID"]);
            $totalPagina = $this->getTotalpaginas();
            include "view/$this->pasta/visualizar.php";
        } elseif (isset($_GET["deletar"])) {
            if (isset($_GET["concordo"])) {
                $this->del($_GET["ID"]);
            }
            $all = $this->ls($_GET["ID"]);
            $total = $this->getTotalpaginas();
            include "view/$this->pasta/deletar.php";
        } elseif (isset($_GET["adicionar"])) {
            if (@$_GET["add"] == "true"  && isset($_POST[substr( $this->campochave,1,-1) ])) {
         

             
                $this->add(); 
                      $c=  $this->lstU();
                    $tabela = "pessoa_turma1";  
                    $campo = "turma,pessoa"; 
                    $limt = $_POST["FKqtd1"];
                    for ($i = 1; $i <= $limt; $i++) {
                          if(isset($_POST["FK1c".$i])){
                        $valor="$c,".$_POST["FK1c".$i];
                        $this->addFk($tabela, $campo, $valor);
                        }
                        

                    
                }
                 
                include "control/Turma/FmExe1.php";
                    }
            include "view/$this->pasta/adicionar.php";
        } elseif (isset($_GET["editar"])) {
            if (@$_GET["edt"] == "true"  && isset($_POST[substr( $this->campochave,1,-1) ])) {    $this->up($_GET["ID"]); 
                    @$c=1;
                    $tabela = "pessoa_turma1";
                    $campoDel = "Turma";
                    @$id = $_GET["ID"];
                    $campo = "turma,pessoa";
                    $this->delFk($id, $tabela, $campoDel);
                    $limt = $_POST["FKqtd1"];
                    for ($i = 1; $i <= $limt; $i++) {
                          if(isset($_POST["FK".$c."c".$i])){
                        $valor="$id,".$_POST["FK".$c."c".$i];
                        $this->addFk($tabela, $campo, $valor);
                        }
                       
                }
                
                  
                    @$c=$id;
                        
                        include "control/Turma/FmExe1.php";
                     }
            $all = $this->ls($_GET["ID"]);
            include "view/$this->pasta/editar.php";
        } elseif (isset($_GET["listar"])) {
            $all = $this->lst();
            $totalPagina = $this->getTotalpaginas();
            include "view/$this->pasta/listar.php";
        } else {
            $all = $this->lst();
            $totalPagina = $this->getTotalpaginas();
            include "view/$this->pasta/listar.php";
        }
    }
}

