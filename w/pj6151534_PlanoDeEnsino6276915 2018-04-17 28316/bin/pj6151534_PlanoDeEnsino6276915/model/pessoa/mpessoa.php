<?php
/**
 * Description of 
 *
 * @author Artur Romão Rocha
 */
require "model/pessoa/pessoa.php";

class mpessoa extends pessoa {

    private $rs; 
    private $tipo;
    public $all;
    private $tabela = "pessoa";   
    public $campochave = "`pessoaNome`";  
    public $campochave1 = "`pessoanome`";  
    public $campochaveID = "pessoaid"; 
    private $campoupdate = "`pessoanome`=#$%s#$,`pessoaidade`=#$%s#$,`pessoadia`=#$%s#$";
   
    private $campoInsert = "`pessoanome`,`pessoaidade`,`pessoadia`";  
    private $campoInsertValor = "#$%s#$,#$%s#$,#$%s#$";  

    public function __construct() {
        $this->maxTipoPage = 40; 
    }

    public function add() {
        $tabela = $this->tabela;
        $campo = $this->campoInsert;
        $this->setCampoValor();

        $valor = $this->campoInsertValor;

        $this->adicionar($tabela, $campo, $valor);
    }

    public function del($id) {
        $tabela = $this->tabela;
        $campochaveID = $this->campochaveID;
        $this->deletar($id, $tabela, $campochaveID);
    }

    public function up($id) {
        $tabela = $this->tabela;
        $campoChaveID = $this->campochaveID;
        $this->valorUpdade();
        $valores = $this->campoupdate;

        $this->editar($tabela, $valores, $id, $campoChaveID);
    }

    public function ls($id) {
        $tabela = $this->tabela;
        $campochaveID = $this->campochaveID;
        $this->rs = $this->listarPorId($id, $tabela, $campochaveID);
        $this->criarobjeto();
        return $this->all;
    }

    public function lst($l=true) {
        $tabela = $this->tabela;
        $campochave = $this->campochave1;
        $this->rs = $this->listaTodos($tabela, $campochave,$l);
        $this->criarobjeto();
        return $this->all;
    }
    

    public function lstU() {
        $tabela = $this->tabela;
        $campochaveID = $this->campochaveID;
        $sql="select * from $tabela order by $campochaveID desc;";
        $this->rs = $this->RunSelect($sql);     
        return $this->rs[0]["$campochaveID"];
    }

    public function lstall() {
        $tabela = $this->tabela;
        $campochaveID = $this->campochaveID;
        $sql="select * from $tabela;";
        $this->rs = $this->RunSelect($sql);   
        $this->criarobjeto();
        return $this->all;
    }

    public function lstOr($id) {
        $tabela = $this->tabela;
        $campochave = $this->campochaveID;
        $this->rs = $this->listaOr($id,$tabela, $campochave);
        $this->criarobjeto();
        return $this->all;
    }
  
    public function delFk($id,$tabela,$campo) {
                $this->deletar($id, $tabela, $campo);
    }
    
    public function addFk($tabela,$campo,$valor) {
          $this->adicionar($tabela, $campo, $valor);
    }
    


    private function valor($s) {
        $valor = $this->setCampoValor($s);
        $this->campoInsertValor = $valor;
    }

    private function valorUpdade() {
        $this->campoupdate = sprintf($this->campoupdate, $_POST["pessoaNome"], $_POST["pessoaIdade"], $_POST["pessoadia"]);
        $this->campoupdate = $this->noSql($this->campoupdate);
    }

    public function criarobjeto() {
        $i = 0;
        foreach ($this->rs as $valor) {

            $this->all[$i] = new pessoa(
               "", $this->rs[$i]["pessoaid"], $this->rs[$i]["pessoanome"], $this->rs[$i]["pessoaidade"], $this->rs[$i]["pessoadia"]
            );
            $i++;
        }
        $this->setTotal($i--);
    }

    public function getAll() {
        return $this->all;
    }

    public function setCampoValor() {
        // $valSimb = "1&*&1";
        $this->campoInsertValor = sprintf($this->campoInsertValor, $_POST["pessoaNome"], $_POST["pessoaIdade"], $_POST["pessoadia"]);
        $this->campoInsertValor = $this->noSql($this->campoInsertValor);
    }
  
}

