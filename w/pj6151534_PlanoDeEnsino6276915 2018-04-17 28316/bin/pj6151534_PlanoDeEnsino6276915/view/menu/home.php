 
<div class="col-lg-6">
    <div class="panel panel-success">
        <div class="panel-heading">
            <h1 class="panel-title">Sejá bem-vindo!!!</h1>
        </div>
        <div class="panel-body">
            <h3>Este sistema foi gerado automaticamente pelo sistema Satuno V 3.03, com o objetivo de auxiliar na administração desta empresa  </h3>
        </div>
    </div>
</div>

<div class="col-lg-6">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h1 class="panel-title">Editar</h1>
        </div>
        <div class="panel-body">
            <h3>  
                Edita um registro pré selecionado. Atenção não é possivel restaurar estado anterior do registro depois de salvar edição.
            </h3>
        </div>
    </div>
</div>
<div class="col-lg-6">
    <div class="panel panel-info">
        <div class="panel-heading">
            <h1 class="panel-title">Adicionar</h1>
        </div>
        <div class="panel-body">
            <h3> 
                Adiciona um registro ao sistema.
            </h3>
        </div>
    </div>
</div>
<div class="col-lg-6">
    <div class="panel panel-danger">
        <div class="panel-heading">
            <h1 class="panel-title">Apagar</h1>
        </div>
        <div class="panel-body">
            <h3>
                Deleta permanentemente o registro. Atenção não sera possivel restauralo use com caltela   
            </h3>
        </div>
    </div>
</div>