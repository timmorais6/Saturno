<?php 
@session_start();
 class UploadFoto{ 
 private $arquivo; 
 private $altura; 
 private $largura; 
 private $pasta;
  function __construct($arquivo, $altura, $largura, $pasta){
	   $this->arquivo = $arquivo; 
	   $this->altura = $altura; 
	   $this->largura = $largura; 
	   $this->pasta = $pasta;
	    } 

 private function getExtensao(){ //retorna a extensao da imagem	 
 	if(isset( $this->arquivo["name"]) && $this->arquivo["name"]!=''){
$f=$this->arquivo["name"];
   @$extensao = strtolower(end(explode(".", $f)));
  
	}else{
 $extensao ='file';
    }
              		 return  $extensao;
              		  }


private function ehImagem($extensao){

    $extensoes = array("gif", "jpeg", "jpg", "png");	 
// extensoes permitidas 
    //removendo verificação de arquivo 
    //if (in_array($extensao, $extensoes)) 
      return true;	
    } 



//largura, altura, tipo, localizacao da imagem original private
 function redimensionar($imgLarg, $imgAlt, $tipo, $img_localizacao){
	  //descobrir novo tamanho sem perder a proporcao 
	  if ( $imgLarg > $imgAlt ){
	        $novaLarg = $this->largura; 
	        $novaAlt = round( ($novaLarg / $imgLarg) * $imgAlt );

	     } elseif ( $imgAlt > $imgLarg ){ 
	   	        $novaAlt = $this->altura; 
	   	        $novaLarg = round( ($novaAlt / $imgAlt) * $imgLarg );
	   	         } else 
				     // altura == largura
	                  $novaAltura = $novaLargura = max($this->largura, $this->altura); //redimencionar a imagem 
        	       	   //cria uma nova imagem com o novo tamanho
	               	 $novaimagem = imagecreatetruecolor($novaLarg, $novaAlt);
		 
		 
		 
		 
		  switch ($tipo){
		   case 1:	
		   // gif
		     $origem = imagecreatefromgif($img_localizacao); 
		     imagecopyresampled($novaimagem, $origem, 0, 0, 0, 0, $novaLarg, $novaAlt, $imgLarg, $imgAlt);
		     imagegif($novaimagem, $img_localizacao);
		   break; 
		  
		  
		  
		  case 2:
		  	// jpg 
			  $origem = imagecreatefromjpeg($img_localizacao); 
			  imagecopyresampled($novaimagem, $origem, 0, 0, 0, 0, $novaLarg, $novaAlt, $imgLarg, $imgAlt);
			  imagejpeg($novaimagem, $img_localizacao); 
		  break; 
		  
		  
		  case 3:	// png
		   $origem = imagecreatefrompng($img_localizacao);
		   imagecopyresampled($novaimagem, $origem, 0, 0, 0, 0, $novaLarg, $novaAlt, $imgLarg, $imgAlt); 
		   imagepng($novaimagem, $img_localizacao); 
		   
		   break; 
	              } 

		   //destroi as imagens criadas
		    imagedestroy($novaimagem);
			 imagedestroy($origem); 
	} 
			
			

		public function salvar(){	

		     $extensao = $this->getExtensao();
			
			 //gera um nome unico para a imagem em funcao do tempo 

			 $novo_nome = rand()."."; 

			 //localizacao do arquivo 
		    $temp=$this->pasta.$novo_nome.".";

            $novo_nome =$novo_nome.".saturno.".$extensao;

			 $destino = $this->pasta . $novo_nome;
              unset($_SESSION["CaminhoParaArquivo"]);
			  if($destino==$temp){
			  		$_SESSION["CaminhoParaArquivo"]="erro";
 					$_SESSION["CaminhoParaArquivo"]='./img/semimagem.png';
			  	}
			  else{ 
			  	 $_SESSION["CaminhoParaArquivo"]=$destino;
			  	}
			     

			
			 //move o arquivo 
			 if(isset($this->arquivo["tmp_name"])){ 
						 if (! move_uploaded_file($this->arquivo["tmp_name"], $destino)){

								 $_SESSION["CaminhoParaArquivo"]='./img/semimagem.png';
							   if ($this->arquivo["error"] == 1) 
							  	 return "Tamanho excede o permitido";
							   else 
							     return "Erro " . $this->arquivo["error"]; 
						  }

			}else{
				
 $_SESSION["CaminhoParaArquivo"]='./img/semimagem.png';
			}
 
				  }  
				}
?>