-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Tempo de geração: 26/04/2018 às 18:48
-- Versão do servidor: 10.1.32-MariaDB
-- Versão do PHP: 7.0.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `u673048033_pe`
--

-- --------------------------------------------------------

--
-- Estrutura para tabela `atividade`
--

CREATE TABLE `atividade` (
  `atividadeid` int(11) NOT NULL COMMENT 'Identificador',
  `atividadedescricao` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'DescriÃ§Ã£o',
  `atividadeatividade` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura para tabela `atividade_conjatividade1`
--

CREATE TABLE `atividade_conjatividade1` (
  `atividade` int(11) NOT NULL,
  `conjatividade` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura para tabela `atp`
--

CREATE TABLE `atp` (
  `atpid` int(11) NOT NULL COMMENT 'Identificador',
  `atpdescricao` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'DescriÃ§Ã£o'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `atp`
--

INSERT INTO `atp` (`atpid`, `atpdescricao`) VALUES
(1, 'Atvps 1'),
(2, 'Atvps 2'),
(3, 'Atvps 3'),
(4, 'Atvps 4'),
(5, 'Atvps 8'),
(6, 'Atvps 9');

-- --------------------------------------------------------

--
-- Estrutura para tabela `atvps`
--

CREATE TABLE `atvps` (
  `atvpsid` int(11) NOT NULL COMMENT 'Identificador',
  `atvpsdescricao` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'DescriÃ§Ã£o'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `atvps`
--

INSERT INTO `atvps` (`atvpsid`, `atvpsdescricao`) VALUES
(1, 'Atvps 2'),
(2, 'Atvps 3'),
(3, 'Atvps 2'),
(4, 'Atvps 1'),
(5, 'Atvps 1'),
(6, 'Atvps 8');

-- --------------------------------------------------------

--
-- Estrutura para tabela `atvssasemana`
--

CREATE TABLE `atvssasemana` (
  `atvssasemanaid` int(11) NOT NULL COMMENT 'Identificador',
  `atvssasemanadescricao` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'DescriÃ§Ã£o'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura para tabela `bibliografia`
--

CREATE TABLE `bibliografia` (
  `bibliografiaid` int(11) NOT NULL COMMENT 'Identificador',
  `bibliografiadescricao` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'DescriÃ§Ã£o'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `bibliografia`
--

INSERT INTO `bibliografia` (`bibliografiaid`, `bibliografiadescricao`) VALUES
(1, 'Modelo padrão'),
(2, 'Modelo padrão'),
(3, 'Modelo padrão'),
(4, 'Modelo padrão'),
(5, 'Modelo padrão'),
(6, 'Modelo padrão'),
(7, 'Modelo padrão'),
(8, 'Modelo padrão'),
(9, 'Modelo padrãoa'),
(10, 'Modelo padrãoa'),
(11, 'Modelo padrão'),
(12, 'Modelo padrão'),
(13, 'Modelo padrão');

-- --------------------------------------------------------

--
-- Estrutura para tabela `bloco`
--

CREATE TABLE `bloco` (
  `blocoid` int(11) NOT NULL COMMENT 'Identificador',
  `blocobloco` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'bloco'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `bloco`
--

INSERT INTO `bloco` (`blocoid`, `blocobloco`) VALUES
(1, 'Bloco 1'),
(2, 'Bloco 2');

-- --------------------------------------------------------

--
-- Estrutura para tabela `conjatividade`
--

CREATE TABLE `conjatividade` (
  `conjatividadeid` int(11) NOT NULL COMMENT 'Identificador',
  `conjatividadedescricao` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'DescriÃ§Ã£o',
  `conjatividadeopsemanaid2` int(11) NOT NULL COMMENT 'Semana'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura para tabela `conjatividade_atvssasemana1`
--

CREATE TABLE `conjatividade_atvssasemana1` (
  `conjatividade` int(11) NOT NULL,
  `atvssasemana` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura para tabela `cp`
--

CREATE TABLE `cp` (
  `cpid` int(11) NOT NULL COMMENT 'Identificador',
  `cpnome` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Nome da unidade'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `cp`
--

INSERT INTO `cp` (`cpid`, `cpnome`) VALUES
(1, 'asASD'),
(2, 'Nome da unidade 2 '),
(3, 'Unidade Vazia'),
(4, 'Unidade Vazia'),
(5, 'Sistema Circulatório'),
(6, 'Nome da unidade 2'),
(7, 'Unidade Vazia'),
(8, 'Nome da unidade 1'),
(9, 'Nome da unidade 2'),
(10, 'Coração'),
(11, 'Nome da unidade 2'),
(12, 'Nome da unidade 1'),
(13, 'Nome da unidade 2'),
(14, 'Estatistica Descritiva'),
(15, 'Nome da unidade 2'),
(16, 'Unidade Vazia'),
(17, 'HISTÓRIA DA COMPUTAÇÃO'),
(18, 'Nome da unidade 2'),
(19, 'Unidade Vazia');

-- --------------------------------------------------------

--
-- Estrutura para tabela `cp_planodeensino8`
--

CREATE TABLE `cp_planodeensino8` (
  `cp` int(11) NOT NULL,
  `planodeensino` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `cp_planodeensino8`
--

INSERT INTO `cp_planodeensino8` (`cp`, `planodeensino`) VALUES
(1, 4),
(2, 4),
(3, 4),
(4, 4),
(5, 3),
(6, 3),
(7, 3),
(8, 5),
(9, 5),
(10, 1),
(11, 1),
(12, 2),
(13, 2),
(14, 8),
(15, 8),
(16, 4),
(17, 9),
(18, 9),
(19, 9);

-- --------------------------------------------------------

--
-- Estrutura para tabela `cronogramaav`
--

CREATE TABLE `cronogramaav` (
  `cronogramaavid` int(11) NOT NULL COMMENT 'Identificador',
  `cronogramaavn11` date DEFAULT NULL COMMENT 'N1.1',
  `cronogramaavn12` date DEFAULT NULL COMMENT 'N1.2',
  `cronogramaavn13` date DEFAULT NULL COMMENT 'N1.3',
  `cronogramaavn21` date DEFAULT NULL COMMENT 'N2.1',
  `cronogramaavn22` date DEFAULT NULL COMMENT 'N2.2',
  `cronogramaavn23` date DEFAULT NULL COMMENT 'N2.3',
  `cronogramaavn24` date DEFAULT NULL COMMENT 'N2.4',
  `cronogramaavn25` date DEFAULT NULL COMMENT 'N2.5',
  `cronogramaavdescricao` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'DescriÃ§Ã£o',
  `cronogramaavmetavaliativosid1` int(11) NOT NULL COMMENT 'N1.1 MÃ©todo de avaliaÃ§Ã£o',
  `cronogramaavmetavaliativosid2` int(11) NOT NULL COMMENT 'N1.2 MÃ©todo de avaliaÃ§Ã£o',
  `cronogramaavmetavaliativosid3` int(11) NOT NULL COMMENT 'N1.3 MÃ©todo de avaliaÃ§Ã£o',
  `cronogramaavmetavaliativosid4` int(11) NOT NULL COMMENT 'N2.1 MÃ©todo de avaliaÃ§Ã£o',
  `cronogramaavmetavaliativosid5` int(11) NOT NULL COMMENT 'N2.2 MÃ©todo de avaliaÃ§Ã£o',
  `cronogramaavmetavaliativosid6` int(11) NOT NULL COMMENT 'N2.3 MÃ©todo de avaliaÃ§Ã£o',
  `cronogramaavmetavaliativosid7` int(11) NOT NULL COMMENT 'N2.4 MÃ©todo de avaliaÃ§Ã£o',
  `cronogramaavmetavaliativosid8` int(11) NOT NULL COMMENT 'N2.5 MÃ©todo de avaliaÃ§Ã£o'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `cronogramaav`
--

INSERT INTO `cronogramaav` (`cronogramaavid`, `cronogramaavn11`, `cronogramaavn12`, `cronogramaavn13`, `cronogramaavn21`, `cronogramaavn22`, `cronogramaavn23`, `cronogramaavn24`, `cronogramaavn25`, `cronogramaavdescricao`, `cronogramaavmetavaliativosid1`, `cronogramaavmetavaliativosid2`, `cronogramaavmetavaliativosid3`, `cronogramaavmetavaliativosid4`, `cronogramaavmetavaliativosid5`, `cronogramaavmetavaliativosid6`, `cronogramaavmetavaliativosid7`, `cronogramaavmetavaliativosid8`) VALUES
(6, '2018-04-23', NULL, NULL, '2018-04-23', NULL, NULL, NULL, NULL, NULL, 1, 1, 3, 4, 5, 6, 1, 8),
(7, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 2, 3, 4, 5, 6, 7, 8),
(8, NULL, NULL, NULL, NULL, NULL, '2018-12-04', NULL, NULL, NULL, 1, 2, 3, 4, 5, 6, 7, 8),
(9, '2018-04-23', '2018-04-23', '2018-04-23', NULL, NULL, NULL, NULL, NULL, NULL, 1, 2, 3, 4, 5, 6, 7, 8),
(10, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 2, 3, 4, 5, 6, 7, 8),
(11, NULL, '2018-04-20', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 3, 3, 4, 5, 6, 7, 8);

-- --------------------------------------------------------

--
-- Estrutura para tabela `curso`
--

CREATE TABLE `curso` (
  `cursoid` int(11) NOT NULL COMMENT 'Identificador',
  `cursocurso` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Curso'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `curso`
--

INSERT INTO `curso` (`cursoid`, `cursocurso`) VALUES
(1, 'FARMÁCIA'),
(2, 'ADMINISTRAÇÃO');

-- --------------------------------------------------------

--
-- Estrutura para tabela `dificuldade`
--

CREATE TABLE `dificuldade` (
  `dificuldadeid` int(11) NOT NULL COMMENT 'Identificador',
  `dificuldadegrau` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Grau'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `dificuldade`
--

INSERT INTO `dificuldade` (`dificuldadeid`, `dificuldadegrau`) VALUES
(1, 'Fácil'),
(2, 'Médio'),
(3, 'Dificil');

-- --------------------------------------------------------

--
-- Estrutura para tabela `dificuldade_semana5`
--

CREATE TABLE `dificuldade_semana5` (
  `dificuldade` int(11) NOT NULL,
  `semana` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `dificuldade_semana5`
--

INSERT INTO `dificuldade_semana5` (`dificuldade`, `semana`) VALUES
(1, 209),
(2, 212),
(2, 213),
(3, 208),
(3, 261);

-- --------------------------------------------------------

--
-- Estrutura para tabela `identificacao`
--

CREATE TABLE `identificacao` (
  `identificacaoid` int(11) NOT NULL COMMENT 'Identificador',
  `identificacaouc` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Unidade Curricular ',
  `identificacaochtt` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Carga HorÃ¡ria Total (Horas)',
  `identificacaocht` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Carga HorÃ¡ria TeÃ³rica (Horas/Aula)',
  `identificacaochp` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Carga HorÃ¡ria PrÃ¡tica l (Horas/Aula)',
  `identificacaochaol` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Carga HorÃ¡ria AOL (Horas/Aula)',
  `identificacaochaps` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Carga HorÃ¡ria APS (Horas/Aula)',
  `identificacaoementa` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'EMENTA',
  `identificacaoobjgeral` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'OBJETIVO GERAL',
  `identificacaousuarioid1` int(11) NOT NULL COMMENT 'Docente',
  `identificacaocursoid2` int(11) NOT NULL COMMENT 'Curso',
  `identificacaoturmaid3` int(11) NOT NULL COMMENT 'Turma'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `identificacao`
--

INSERT INTO `identificacao` (`identificacaoid`, `identificacaouc`, `identificacaochtt`, `identificacaocht`, `identificacaochp`, `identificacaochaol`, `identificacaochaps`, `identificacaoementa`, `identificacaoobjgeral`, `identificacaousuarioid1`, `identificacaocursoid2`, `identificacaoturmaid3`) VALUES
(1, 'Artur Romão Rocha', '3', '0', '0', '0', '0', '<p><br><br></p>', '<p>asasdfasfasdf</p><table class=\'table table-bordered\'><tbody><tr><td>as</td><td>a</td></tr><tr><td>as\'&nbsp;</td><td>as</td></tr><tr><td>\'</td><td><br></td></tr><tr><td><br></td><td><br></td></tr></tbody></table><p>asd</p>', 1, 2, 1),
(2, 'ad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1),
(3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1),
(4, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1),
(5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1),
(6, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1),
(7, 'Coração', NULL, NULL, NULL, NULL, NULL, '<p>gfh</p>', '<p>sgdfsg</p>', 1, 1, 1),
(8, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1),
(9, 'asq', NULL, NULL, NULL, NULL, NULL, NULL, '<p>asdasda</p>', 1, 1, 2),
(10, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1),
(11, 'a', '1', '12', '1', '12', '12', '<p><span style=\'background-color: rgb(255, 255, 0);\'>asdfsadfsadfsadfasfd</span></p>', '<p>asdfasdfsdfa</p>', 1, 2, 1),
(12, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1),
(13, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1),
(14, 'Bioestatística', '40', '32', NULL, NULL, '8', '<p>Estatistica descritiva</p>', '<p>Simular</p>', 1, 1, 1),
(15, 'artur  dse', '12', '12', '1', '2', '1', '<p>bla bla bla&nbsp;</p>', '<p>Bla bla&nbsp;</p>', 1, 2, 1);

-- --------------------------------------------------------

--
-- Estrutura para tabela `itemeqt`
--

CREATE TABLE `itemeqt` (
  `itemeqtid` int(11) NOT NULL COMMENT 'Identificador',
  `itemeqtdescricao` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'DescriÃ§Ã£o',
  `itemeqtitem` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Material',
  `itemeqtquantidade` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Quantidade'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura para tabela `itemeqt_pap3`
--

CREATE TABLE `itemeqt_pap3` (
  `itemeqt` int(11) NOT NULL,
  `pap` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura para tabela `laboratorios`
--

CREATE TABLE `laboratorios` (
  `laboratoriosid` int(11) NOT NULL COMMENT 'Identificador',
  `laboratorioslab` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'LaboratÃ³rio',
  `laboratoriosdesc` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'DescriÃ§Ã£o'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `laboratorios`
--

INSERT INTO `laboratorios` (`laboratoriosid`, `laboratorioslab`, `laboratoriosdesc`) VALUES
(1, 'Informática', ''),
(2, 'Química ', '');

-- --------------------------------------------------------

--
-- Estrutura para tabela `metavaliativos`
--

CREATE TABLE `metavaliativos` (
  `metavaliativosid` int(11) NOT NULL COMMENT 'Identificador',
  `metavaliativosmetodo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'MÃ©todo',
  `metavaliativospeso` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Peso'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `metavaliativos`
--

INSERT INTO `metavaliativos` (`metavaliativosid`, `metavaliativosmetodo`, `metavaliativospeso`) VALUES
(1, 'Prova', '10'),
(2, 'N1.2 XXX', ''),
(3, 'N1.2 XXX', ''),
(4, 'n2', ''),
(5, 'n2', ''),
(6, 'n2', ''),
(7, 'n2', ''),
(8, 'n2', ''),
(9, 'n2', '');

-- --------------------------------------------------------

--
-- Estrutura para tabela `nivelamento`
--

CREATE TABLE `nivelamento` (
  `nivelamentoid` int(11) NOT NULL COMMENT 'Identificador',
  `nivelamentodescricao` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'DescriÃ§Ã£o'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `nivelamento`
--

INSERT INTO `nivelamento` (`nivelamentoid`, `nivelamentodescricao`) VALUES
(3, 'Nivelamento1'),
(4, 'Nivelamento2'),
(5, 'Nivelamento3'),
(6, 'Nivelamento4'),
(7, 'Nivelamento8'),
(8, 'Nivelamento9');

-- --------------------------------------------------------

--
-- Estrutura para tabela `opsemana`
--

CREATE TABLE `opsemana` (
  `opsemanaid` int(11) NOT NULL COMMENT 'Identificador',
  `opsemanaop` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Semana'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `opsemana`
--

INSERT INTO `opsemana` (`opsemanaid`, `opsemanaop`) VALUES
(1, 'Semana 1'),
(2, 'Semana 2'),
(3, 'Semana 3'),
(4, 'Semana 4'),
(5, 'Semana 5'),
(6, 'Semana 6'),
(7, 'Semana 7'),
(8, 'Semana 8'),
(9, 'Semana 9'),
(10, 'Semana 10'),
(11, 'Não se aplica');

-- --------------------------------------------------------

--
-- Estrutura para tabela `opsemana_semana2`
--

CREATE TABLE `opsemana_semana2` (
  `opsemana` int(11) NOT NULL,
  `semana` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `opsemana_semana2`
--

INSERT INTO `opsemana_semana2` (`opsemana`, `semana`) VALUES
(1, 1),
(1, 17),
(1, 18),
(1, 59),
(1, 69),
(1, 117),
(1, 138),
(1, 156),
(1, 158),
(1, 159),
(1, 170),
(1, 172),
(1, 173),
(1, 176),
(1, 177),
(1, 183),
(1, 208),
(1, 212),
(1, 214),
(1, 261),
(1, 277),
(2, 1),
(2, 164),
(2, 170),
(2, 172),
(2, 173),
(2, 176),
(2, 177),
(2, 183),
(2, 184),
(2, 208),
(2, 277),
(3, 1),
(3, 117),
(3, 138),
(3, 164),
(3, 165),
(3, 170),
(3, 172),
(3, 173),
(3, 176),
(3, 177),
(3, 184),
(3, 208),
(3, 209),
(4, 1),
(4, 117),
(4, 159),
(4, 162),
(4, 164),
(4, 165),
(4, 170),
(4, 172),
(4, 173),
(4, 176),
(4, 177),
(4, 178),
(4, 180),
(4, 181),
(4, 182),
(4, 208),
(4, 209),
(5, 1),
(5, 158),
(5, 159),
(5, 164),
(5, 165),
(5, 170),
(5, 172),
(5, 173),
(5, 176),
(5, 177),
(5, 178),
(5, 180),
(5, 181),
(5, 182),
(5, 185),
(6, 1),
(6, 163),
(6, 164),
(6, 170),
(6, 172),
(6, 173),
(6, 176),
(6, 177),
(6, 178),
(6, 180),
(6, 181),
(6, 182),
(6, 183),
(6, 185),
(6, 213),
(7, 1),
(7, 159),
(7, 160),
(7, 163),
(7, 170),
(7, 172),
(7, 173),
(7, 176),
(7, 177),
(7, 178),
(7, 179),
(7, 180),
(7, 181),
(8, 1),
(8, 159),
(8, 170),
(8, 172),
(8, 173),
(8, 176),
(8, 177),
(8, 178),
(8, 179),
(8, 180),
(8, 181),
(9, 1),
(9, 160),
(9, 170),
(9, 172),
(9, 173),
(9, 176),
(9, 177),
(9, 178),
(9, 179),
(9, 180),
(9, 181),
(10, 1),
(10, 160),
(10, 170),
(10, 172),
(10, 173),
(10, 176),
(10, 177),
(10, 178),
(10, 179),
(10, 180),
(10, 181),
(10, 183),
(11, 170),
(11, 176),
(11, 177),
(11, 178),
(11, 179),
(11, 287);

-- --------------------------------------------------------

--
-- Estrutura para tabela `opsemana_semana4`
--

CREATE TABLE `opsemana_semana4` (
  `opsemana` int(11) NOT NULL,
  `semana` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `opsemana_semana4`
--

INSERT INTO `opsemana_semana4` (`opsemana`, `semana`) VALUES
(1, 59),
(1, 163),
(1, 179),
(2, 117),
(2, 138),
(2, 156),
(2, 159),
(2, 160),
(2, 162),
(2, 176),
(2, 179),
(2, 209),
(2, 261),
(3, 156),
(3, 179),
(3, 261),
(4, 138),
(4, 156),
(4, 179),
(4, 183),
(4, 261),
(5, 117),
(5, 158),
(5, 162),
(5, 179),
(5, 180),
(5, 181),
(5, 209),
(5, 261),
(6, 156),
(6, 179),
(6, 180),
(6, 181),
(6, 261),
(7, 156),
(7, 163),
(7, 179),
(7, 180),
(7, 181),
(7, 261),
(8, 156),
(8, 180),
(8, 181),
(8, 183),
(9, 181),
(11, 17),
(11, 18),
(11, 212),
(11, 213),
(11, 214),
(11, 287);

-- --------------------------------------------------------

--
-- Estrutura para tabela `pap`
--

CREATE TABLE `pap` (
  `papid` int(11) NOT NULL COMMENT 'Identificador',
  `papnum` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'NÃšMERO DO PROTOCOLO',
  `papdia` date DEFAULT NULL COMMENT 'DATA',
  `paptitulo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'TÃTULO',
  `papobj` text COLLATE utf8_unicode_ci,
  `papfunteo` text COLLATE utf8_unicode_ci,
  `pappromet` text COLLATE utf8_unicode_ci,
  `papresespvalref` text COLLATE utf8_unicode_ci,
  `papdescricao` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'DescriÃ§Ã£o',
  `papturnosid1` int(11) NOT NULL COMMENT 'TURNO DE EXECUÃ‡ÃƒO',
  `paplaboratoriosid2` int(11) NOT NULL COMMENT 'LABORATÃ“RIO'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura para tabela `pap_planodeensino10`
--

CREATE TABLE `pap_planodeensino10` (
  `pap` int(11) NOT NULL,
  `planodeensino` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura para tabela `planodeensino`
--

CREATE TABLE `planodeensino` (
  `planodeensinoid` int(11) NOT NULL COMMENT 'Identificador',
  `planodeensinodescricao` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Descrição',
  `planodeensinodia` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Data e hora',
  `planodeensinousuarioid1` int(11) DEFAULT NULL COMMENT 'Docente',
  `planodeensinocursoid2` int(11) DEFAULT NULL COMMENT 'Curso',
  `planodeensinoidentificacaoid3` int(11) DEFAULT NULL COMMENT 'Identificação',
  `planodeensinoatvpsid4` int(11) DEFAULT NULL COMMENT 'Atividade Prática Supervisionada',
  `planodeensinoprojetoid5` int(11) DEFAULT NULL COMMENT 'Projeto ou Plano de ação (Estágio)',
  `planodeensinonivelamentoid6` int(11) DEFAULT NULL COMMENT 'Nivelamento',
  `planodeensinobibliografiaid7` int(11) DEFAULT NULL COMMENT 'Bibliografia',
  `planodeensinoatpid9` int(11) DEFAULT NULL COMMENT 'Atividades previstas',
  `planodeensinocronogramaavid11` int(11) DEFAULT NULL COMMENT 'Cronograma de avaliação',
  `planodeensinoatvssasemanaid12` int(11) DEFAULT NULL COMMENT 'Semanas (Atividades Sagah, Siga ou SSA)',
  `planodeensinoativo` int(11) DEFAULT NULL COMMENT 'ativo?'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `planodeensino`
--

INSERT INTO `planodeensino` (`planodeensinoid`, `planodeensinodescricao`, `planodeensinodia`, `planodeensinousuarioid1`, `planodeensinocursoid2`, `planodeensinoidentificacaoid3`, `planodeensinoatvpsid4`, `planodeensinoprojetoid5`, `planodeensinonivelamentoid6`, `planodeensinobibliografiaid7`, `planodeensinoatpid9`, `planodeensinocronogramaavid11`, `planodeensinoatvssasemanaid12`, `planodeensinoativo`) VALUES
(1, 'Introdução a Informatica', NULL, 0, NULL, 9, 4, 2, 3, 9, 1, 6, NULL, NULL),
(2, 'a', NULL, 0, NULL, 2, 3, 3, 4, 8, 2, 7, NULL, NULL),
(3, 'Anatomia', NULL, 0, NULL, 7, 2, 1, 5, 6, 3, 8, NULL, NULL),
(4, 'Conexão', NULL, 0, NULL, 11, NULL, 5, 6, 10, 4, 9, NULL, NULL),
(5, '', NULL, 0, NULL, 13, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(6, 'ff', NULL, 0, NULL, 12, NULL, NULL, NULL, 11, NULL, NULL, NULL, NULL),
(7, 'aa', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(8, 'Bioestatística', NULL, 0, NULL, 14, 6, 4, 7, 12, 5, 10, NULL, NULL),
(9, 'teste x', NULL, 0, NULL, 15, NULL, NULL, 8, 13, 6, 11, NULL, NULL),
(10, 'OI', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura para tabela `projeto`
--

CREATE TABLE `projeto` (
  `projetoid` int(11) NOT NULL COMMENT 'Identificador',
  `projetodescricao` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'DescriÃ§Ã£o'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `projeto`
--

INSERT INTO `projeto` (`projetoid`, `projetodescricao`) VALUES
(1, 'Projeto3'),
(2, 'Projeto1'),
(3, 'Projeto2'),
(4, 'Projeto8'),
(5, 'Projeto4');

-- --------------------------------------------------------

--
-- Estrutura para tabela `prova`
--

CREATE TABLE `prova` (
  `provaid` int(11) NOT NULL COMMENT 'Identificador',
  `provaprova` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Prova'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `prova`
--

INSERT INTO `prova` (`provaid`, `provaprova`) VALUES
(1, 'Prova 1'),
(2, 'Prova 2');

-- --------------------------------------------------------

--
-- Estrutura para tabela `prova_cp1`
--

CREATE TABLE `prova_cp1` (
  `prova` int(11) NOT NULL,
  `cp` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `prova_cp1`
--

INSERT INTO `prova_cp1` (`prova`, `cp`) VALUES
(1, 10),
(1, 14),
(1, 17),
(2, 1),
(2, 17);

-- --------------------------------------------------------

--
-- Estrutura para tabela `semana`
--

CREATE TABLE `semana` (
  `semanaid` int(11) NOT NULL COMMENT 'Identificador',
  `semanadescricao` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'DescriÃ§Ã£o',
  `semanaatividade` text COLLATE utf8_unicode_ci,
  `semanablocoid1` int(11) NOT NULL COMMENT 'Bloco1',
  `semanablocoid3` int(11) NOT NULL COMMENT 'Bloco 2'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `semana`
--

INSERT INTO `semana` (`semanaid`, `semanadescricao`, `semanaatividade`, `semanablocoid1`, `semanablocoid3`) VALUES
(1, '', 'dxed', 1, 1),
(2, 'Bibliografia Básica 1', NULL, 1, 2),
(3, 'Bibliografia Básica 2', NULL, 1, 2),
(4, 'Bibliografia Básica 3', NULL, 1, 2),
(5, 'Bibliografia Básica 4', NULL, 1, 2),
(6, 'Bibliografia Básica 4', NULL, 1, 2),
(7, 'Bibliografia Básica 1', NULL, 1, 2),
(8, 'Bibliografia Básica 2', NULL, 1, 2),
(9, 'Bibliografia Básica 3', NULL, 1, 2),
(10, 'Bibliografia Básica 4', NULL, 1, 2),
(11, 'Bibliografia Básica 4', NULL, 1, 2),
(12, 'Bibliografia Básica 1', NULL, 1, 2),
(13, 'Bibliografia Básica 2', NULL, 1, 2),
(14, 'Bibliografia Básica 3', NULL, 1, 2),
(15, 'Bibliografia Básica 4', NULL, 1, 2),
(16, 'Bibliografia Básica 4', NULL, 1, 2),
(17, 'Bibliografia Básica 1', 'egr', 1, 2),
(18, 'Bibliografia Básica 2', 'k', 1, 2),
(19, 'Bibliografia Básica 3', NULL, 1, 2),
(20, 'Bibliografia Básica 4', NULL, 1, 2),
(21, 'Bibliografia Complementar 1', NULL, 1, 2),
(22, 'Bibliografia Complementar 2', NULL, 1, 2),
(23, 'Bibliografia Complementar 3', NULL, 1, 2),
(24, 'Bibliografia Complementar 4', NULL, 1, 2),
(25, 'Bibliografia Complementar 5', NULL, 1, 2),
(26, 'Bibliografia Artigo 1', NULL, 1, 2),
(27, 'Bibliografia Artigo 2', '', 1, 2),
(28, 'Bibliografia Artigo 3', NULL, 1, 2),
(29, 'Bibliografia Artigo 4', NULL, 1, 2),
(30, 'Bibliografia Artigo 5', NULL, 1, 2),
(31, 'Bibliografia Básica 1', NULL, 1, 2),
(32, 'Bibliografia Básica 2', NULL, 1, 2),
(33, 'Bibliografia Básica 3', NULL, 1, 2),
(34, 'Bibliografia Básica 4', NULL, 1, 2),
(35, 'Bibliografia Complementar 1', NULL, 1, 2),
(36, 'Bibliografia Complementar 2', NULL, 1, 2),
(37, 'Bibliografia Complementar 3', NULL, 1, 2),
(38, 'Bibliografia Complementar 4', NULL, 1, 2),
(39, 'Bibliografia Complementar 5', NULL, 1, 2),
(40, 'Bibliografia Artigo 1', NULL, 1, 2),
(41, 'Bibliografia Artigo 2', NULL, 1, 2),
(42, 'Bibliografia Artigo 3', NULL, 1, 2),
(43, 'Bibliografia Artigo 4', NULL, 1, 2),
(44, 'Bibliografia Artigo 5', NULL, 1, 2),
(45, 'Bibliografia Básica 1', NULL, 1, 2),
(46, 'Bibliografia Básica 2', NULL, 1, 2),
(47, 'Bibliografia Básica 3', NULL, 1, 2),
(48, 'Bibliografia Básica 4', NULL, 1, 2),
(49, 'Bibliografia Complementar 1', NULL, 1, 2),
(50, 'Bibliografia Complementar 2', NULL, 1, 2),
(51, 'Bibliografia Complementar 3', NULL, 1, 2),
(52, 'Bibliografia Complementar 4', NULL, 1, 2),
(53, 'Bibliografia Complementar 5', NULL, 1, 2),
(54, 'Bibliografia Artigo 1', NULL, 1, 2),
(55, 'Bibliografia Artigo 2', NULL, 1, 2),
(56, 'Bibliografia Artigo 3', NULL, 1, 2),
(57, 'Bibliografia Artigo 4', NULL, 1, 2),
(58, 'Bibliografia Artigo 5', NULL, 1, 2),
(59, 'Bibliografia Básica 1', NULL, 1, 2),
(60, 'Bibliografia Básica 2', NULL, 1, 2),
(61, 'Bibliografia Básica 3', NULL, 1, 2),
(62, 'Bibliografia Básica 4', NULL, 1, 2),
(63, 'Bibliografia Complementar 1', NULL, 1, 2),
(64, 'Bibliografia Complementar 2', NULL, 1, 2),
(65, 'Bibliografia Complementar 3', NULL, 1, 2),
(66, 'Bibliografia Complementar 4', NULL, 1, 2),
(67, 'Bibliografia Complementar 5', NULL, 1, 2),
(68, 'Bibliografia Artigo 1', NULL, 1, 2),
(69, 'Bibliografia Artigo 2', NULL, 1, 2),
(70, 'Bibliografia Artigo 3', NULL, 1, 2),
(71, 'Bibliografia Artigo 4', NULL, 1, 2),
(72, 'Bibliografia Artigo 5', NULL, 1, 2),
(73, 'Item 1', NULL, 1, 2),
(74, 'Item 3', NULL, 1, 2),
(75, 'Item 4', NULL, 1, 2),
(76, 'Item 5', NULL, 1, 2),
(77, 'Item 6', NULL, 1, 2),
(78, 'Item 1', NULL, 1, 2),
(79, 'Item 3', NULL, 1, 2),
(80, 'Item 4', NULL, 1, 2),
(81, 'Item 5', NULL, 1, 2),
(82, 'Item 6', NULL, 1, 2),
(83, 'Item 1', 'aw', 1, 2),
(84, 'Item 2', NULL, 1, 2),
(85, 'Item 3', NULL, 1, 2),
(86, 'Item 4', NULL, 1, 2),
(87, 'Item 5', NULL, 1, 2),
(88, 'Item 6', NULL, 1, 2),
(89, 'Item 1', 'ds', 1, 2),
(90, 'Item 2', NULL, 1, 2),
(91, 'Item 3', NULL, 1, 2),
(92, 'Item 4', NULL, 1, 2),
(93, 'Item 5', NULL, 1, 2),
(94, 'Item 6', NULL, 1, 2),
(95, 'Item 1', NULL, 1, 2),
(96, 'Item 2', NULL, 1, 2),
(97, 'Item 3', NULL, 1, 2),
(98, 'Item 4', NULL, 1, 2),
(99, 'Item 5', NULL, 1, 2),
(100, 'Item 6', NULL, 1, 2),
(101, 'Item 1', NULL, 1, 2),
(102, 'Item 2', NULL, 1, 2),
(103, 'Item 3', NULL, 1, 2),
(104, 'Item 4', NULL, 1, 2),
(105, 'Item 5', NULL, 1, 2),
(106, 'Item 6', NULL, 1, 2),
(107, 'Item 1', NULL, 1, 2),
(108, 'Item 2', NULL, 1, 2),
(109, 'Item 3', NULL, 1, 2),
(110, 'Item 4', NULL, 1, 2),
(111, 'Item 5', NULL, 1, 2),
(112, 'Item 6', NULL, 1, 2),
(113, 'Item 1', NULL, 1, 2),
(114, 'Item 2', NULL, 1, 2),
(115, 'Item 3', NULL, 1, 2),
(116, 'Item 4', NULL, 1, 2),
(117, 'Item 5', 'asd', 1, 2),
(118, 'Item 6', NULL, 1, 2),
(119, 'Item 1', NULL, 1, 2),
(120, 'Item 2', NULL, 1, 2),
(121, 'Item 3', NULL, 1, 2),
(122, 'Item 4', NULL, 1, 2),
(123, 'Item 5', NULL, 1, 2),
(124, 'Item 6', NULL, 1, 2),
(125, 'Item 1', NULL, 1, 2),
(126, 'Item 2', NULL, 1, 2),
(127, 'Item 3', NULL, 1, 2),
(128, 'Item 4', NULL, 1, 2),
(129, 'Item 5', NULL, 1, 2),
(130, 'Item 6', NULL, 1, 2),
(131, 'Item 1', NULL, 1, 2),
(132, 'Item 2', NULL, 1, 2),
(133, 'Item 3', NULL, 1, 2),
(134, 'Item 4', NULL, 1, 2),
(135, 'Item 5', NULL, 1, 2),
(136, 'Item 6', NULL, 1, 2),
(137, 'APS', NULL, 1, 2),
(138, 'ATV a Critério do Docente', '5', 1, 2),
(139, 'Aula Prática', '4', 1, 2),
(140, 'CHAT', NULL, 1, 2),
(141, 'Item 5', NULL, 1, 2),
(142, 'ENADE/Fórum', NULL, 1, 2),
(143, 'Glossário', NULL, 1, 2),
(144, 'Fórum', NULL, 1, 2),
(145, 'Projeto', NULL, 1, 2),
(146, 'WIKI', NULL, 1, 2),
(147, 'APS', NULL, 1, 2),
(148, 'ATV a Critério do Docente', NULL, 1, 2),
(149, 'Aula Prática', NULL, 1, 2),
(150, 'CHAT', NULL, 1, 2),
(151, 'ENADE/Fórum', NULL, 1, 2),
(152, 'Glossário', NULL, 1, 2),
(153, 'Fórum', NULL, 1, 2),
(154, 'Projeto', NULL, 1, 2),
(155, 'WIKI', NULL, 1, 2),
(156, 'Bibliografia Básica 1', 'asd', 1, 2),
(157, 'Bibliografia Básica 2', NULL, 1, 2),
(158, 'Bibliografia Básica 3', 'fgh', 1, 2),
(159, 'Bibliografia Básica 4', 'f', 1, 2),
(160, 'Bibliografia Complementar 1', 'dfg', 1, 2),
(161, 'Bibliografia Complementar 2', NULL, 1, 2),
(162, 'Bibliografia Complementar 3', 'jkl', 1, 2),
(163, 'Bibliografia Complementar 4', 'k', 1, 2),
(164, 'Bibliografia Complementar 5', 'kl', 1, 2),
(165, 'Bibliografia Artigo 1', 'oi', 1, 2),
(166, 'Bibliografia Artigo 2', NULL, 1, 2),
(167, 'Bibliografia Artigo 3', NULL, 1, 2),
(168, 'Bibliografia Artigo 4', NULL, 1, 2),
(169, 'Bibliografia Artigo 5', NULL, 1, 2),
(170, 'Item 1', 'fd', 1, 2),
(171, 'Item 2', NULL, 1, 2),
(172, 'Item 3', NULL, 1, 2),
(173, 'Item 4', NULL, 1, 2),
(174, 'Item 5', NULL, 1, 2),
(175, 'Item 6', NULL, 1, 2),
(176, 'APS', 'hgjg', 1, 2),
(177, 'ATV a Critério do Docente', NULL, 1, 2),
(178, 'Aula Prática', NULL, 1, 2),
(179, 'CHAT', NULL, 1, 2),
(180, 'ENADE/Fórum', NULL, 1, 2),
(181, 'Glossário', NULL, 1, 2),
(182, 'Fórum', NULL, 1, 2),
(183, 'Projeto', NULL, 1, 2),
(184, 'WIKI', NULL, 1, 2),
(185, 'Bibliografia Básica 1', NULL, 1, 2),
(186, 'Bibliografia Básica 2', NULL, 1, 2),
(187, 'Bibliografia Básica 3', NULL, 1, 2),
(188, 'Bibliografia Básica 4', NULL, 1, 2),
(189, 'Bibliografia Complementar 1', NULL, 1, 2),
(190, 'Bibliografia Complementar 2', NULL, 1, 2),
(191, 'Bibliografia Complementar 3', NULL, 1, 2),
(192, 'Bibliografia Complementar 4', NULL, 1, 2),
(193, 'Bibliografia Complementar 5', NULL, 1, 2),
(194, 'Bibliografia Artigo 1', NULL, 1, 2),
(195, 'Bibliografia Artigo 2', NULL, 1, 2),
(196, 'Bibliografia Artigo 3', NULL, 1, 2),
(197, 'Bibliografia Artigo 4', NULL, 1, 2),
(198, 'Bibliografia Artigo 5', NULL, 1, 2),
(199, 'Subunidade', NULL, 1, 2),
(200, 'Subunidade', NULL, 1, 2),
(201, 'Subunidade', NULL, 1, 2),
(202, 'Subunidade', NULL, 1, 2),
(203, 'Subunidade', NULL, 1, 2),
(204, 'Subunidade', NULL, 1, 2),
(205, 'Subunidade', NULL, 1, 2),
(206, 'Subunidade', NULL, 1, 2),
(207, 'Subunidade', NULL, 1, 2),
(208, 'Subunidade', 'História da computaçãoasas', 1, 2),
(209, 'Subunidade', 'ad', 1, 2),
(210, 'Subunidade', NULL, 1, 2),
(211, 'Subunidade', NULL, 1, 2),
(212, 'Subunidade', 'Amostra', 1, 2),
(213, 'Subunidade', 'Gráficos', 1, 2),
(214, 'Bibliografia Básica 1', 'fffffffffffffffff', 1, 2),
(215, 'Bibliografia Básica 2', NULL, 1, 2),
(216, 'Bibliografia Básica 3', NULL, 1, 2),
(217, 'Bibliografia Básica 4', NULL, 1, 2),
(218, 'Bibliografia Complementar 1', NULL, 1, 2),
(219, 'Bibliografia Complementar 2', NULL, 1, 2),
(220, 'Bibliografia Complementar 3', NULL, 1, 2),
(221, 'Bibliografia Complementar 4', NULL, 1, 2),
(222, 'Bibliografia Complementar 5', NULL, 1, 2),
(223, 'Bibliografia Artigo 1', NULL, 1, 2),
(224, 'Bibliografia Artigo 2', NULL, 1, 2),
(225, 'Bibliografia Artigo 3', NULL, 1, 2),
(226, 'Bibliografia Artigo 4', NULL, 1, 2),
(227, 'Bibliografia Artigo 5', NULL, 1, 2),
(228, 'Item 1', NULL, 1, 2),
(229, 'Item 2', NULL, 1, 2),
(230, 'Item 3', NULL, 1, 2),
(231, 'Item 4', NULL, 1, 2),
(232, 'Item 5', NULL, 1, 2),
(233, 'Item 6', NULL, 1, 2),
(234, 'Item 1', NULL, 1, 2),
(235, 'Item 2', NULL, 1, 2),
(236, 'Item 3', NULL, 1, 2),
(237, 'Item 4', NULL, 1, 2),
(238, 'Item 5', NULL, 1, 2),
(239, 'Item 6', NULL, 1, 2),
(240, 'Item 1', NULL, 1, 2),
(241, 'Item 2', NULL, 1, 2),
(242, 'Item 3', NULL, 1, 2),
(243, 'Item 4', NULL, 1, 2),
(244, 'Item 5', NULL, 1, 2),
(245, 'Item 6', NULL, 1, 2),
(246, 'APS', NULL, 1, 2),
(247, 'ATV a Critério do Docente', NULL, 1, 2),
(248, 'Aula Prática', NULL, 1, 2),
(249, 'CHAT', NULL, 1, 2),
(250, 'ENADE/Fórum', NULL, 1, 2),
(251, 'Glossário', NULL, 1, 2),
(252, 'Fórum', NULL, 1, 2),
(253, 'Projeto', NULL, 1, 2),
(254, 'WIKI', NULL, 1, 2),
(255, 'Item 1', NULL, 1, 2),
(256, 'Item 2', NULL, 1, 2),
(257, 'Item 3', NULL, 1, 2),
(258, 'Item 4', NULL, 1, 2),
(259, 'Item 5', NULL, 1, 2),
(260, 'Item 6', NULL, 1, 2),
(261, 'Subunidade', 'ENIAK', 1, 2),
(262, 'Subunidade', NULL, 1, 2),
(263, 'Bibliografia Básica 1', NULL, 1, 2),
(264, 'Bibliografia Básica 2', NULL, 1, 2),
(265, 'Bibliografia Básica 3', NULL, 1, 2),
(266, 'Bibliografia Básica 4', NULL, 1, 2),
(267, 'Bibliografia Complementar 1', NULL, 1, 2),
(268, 'Bibliografia Complementar 2', NULL, 1, 2),
(269, 'Bibliografia Complementar 3', NULL, 1, 2),
(270, 'Bibliografia Complementar 4', NULL, 1, 2),
(271, 'Bibliografia Complementar 5', NULL, 1, 2),
(272, 'Bibliografia Artigo 1', NULL, 1, 2),
(273, 'Bibliografia Artigo 2', NULL, 1, 2),
(274, 'Bibliografia Artigo 3', NULL, 1, 2),
(275, 'Bibliografia Artigo 4', NULL, 1, 2),
(276, 'Bibliografia Artigo 5', NULL, 1, 2),
(277, 'Item 1', 'INFORMATICA NA SOCIEDA....', 1, 2),
(278, 'Item 2', NULL, 1, 2),
(279, 'Item 3', NULL, 1, 2),
(280, 'Item 4', NULL, 1, 2),
(281, 'Item 5', NULL, 1, 2),
(282, 'Item 6', NULL, 1, 2),
(283, 'APS', 'Relacionada ao SSA', 1, 2),
(284, 'ATV a Critério do Docente', NULL, 1, 2),
(285, 'Aula Prática', NULL, 1, 2),
(286, 'CHAT', NULL, 1, 2),
(287, 'ENADE/Fórum', NULL, 1, 2),
(288, 'Glossário', NULL, 1, 2),
(289, 'Fórum', NULL, 1, 2),
(290, 'Projeto', NULL, 1, 2),
(291, 'WIKI', NULL, 1, 2);

-- --------------------------------------------------------

--
-- Estrutura para tabela `semana_atp1`
--

CREATE TABLE `semana_atp1` (
  `semana` int(11) NOT NULL,
  `atp` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `semana_atp1`
--

INSERT INTO `semana_atp1` (`semana`, `atp`) VALUES
(131, 1),
(132, 1),
(133, 1),
(134, 1),
(135, 1),
(136, 1),
(138, 2),
(139, 2),
(140, 2),
(141, 2),
(142, 2),
(143, 2),
(144, 2),
(145, 2),
(146, 2),
(147, 3),
(148, 3),
(149, 3),
(150, 3),
(151, 3),
(152, 3),
(153, 3),
(154, 3),
(155, 3),
(176, 4),
(177, 4),
(178, 4),
(179, 4),
(180, 4),
(181, 4),
(182, 4),
(183, 4),
(184, 4),
(246, 5),
(247, 5),
(248, 5),
(249, 5),
(250, 5),
(251, 5),
(252, 5),
(253, 5),
(254, 5),
(283, 6),
(284, 6),
(285, 6),
(286, 6),
(287, 6),
(288, 6),
(289, 6),
(290, 6),
(291, 6);

-- --------------------------------------------------------

--
-- Estrutura para tabela `semana_atvps1`
--

CREATE TABLE `semana_atvps1` (
  `semana` int(11) NOT NULL,
  `atvps` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `semana_atvps1`
--

INSERT INTO `semana_atvps1` (`semana`, `atvps`) VALUES
(107, 2),
(108, 2),
(109, 2),
(110, 2),
(111, 2),
(112, 2),
(113, 3),
(114, 3),
(115, 3),
(116, 3),
(117, 3),
(118, 3),
(119, 4),
(120, 4),
(121, 4),
(122, 4),
(123, 4),
(124, 4),
(240, 6),
(241, 6),
(242, 6),
(243, 6),
(244, 6),
(245, 6);

-- --------------------------------------------------------

--
-- Estrutura para tabela `semana_bibliografia1`
--

CREATE TABLE `semana_bibliografia1` (
  `semana` int(11) NOT NULL,
  `bibliografia` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `semana_bibliografia1`
--

INSERT INTO `semana_bibliografia1` (`semana`, `bibliografia`) VALUES
(12, 5),
(13, 5),
(14, 5),
(15, 5),
(16, 5),
(17, 6),
(18, 6),
(19, 6),
(20, 6),
(31, 7),
(32, 7),
(33, 7),
(34, 7),
(45, 8),
(46, 8),
(47, 8),
(48, 8),
(59, 9),
(60, 9),
(61, 9),
(62, 9),
(156, 10),
(157, 10),
(158, 10),
(159, 10),
(185, 11),
(186, 11),
(187, 11),
(188, 11),
(214, 12),
(215, 12),
(216, 12),
(217, 12),
(263, 13),
(264, 13),
(265, 13),
(266, 13);

-- --------------------------------------------------------

--
-- Estrutura para tabela `semana_bibliografia2`
--

CREATE TABLE `semana_bibliografia2` (
  `semana` int(11) NOT NULL,
  `bibliografia` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `semana_bibliografia2`
--

INSERT INTO `semana_bibliografia2` (`semana`, `bibliografia`) VALUES
(21, 6),
(22, 6),
(23, 6),
(24, 6),
(25, 6),
(35, 7),
(36, 7),
(37, 7),
(38, 7),
(39, 7),
(49, 8),
(50, 8),
(51, 8),
(52, 8),
(53, 8),
(63, 9),
(64, 9),
(65, 9),
(66, 9),
(67, 9),
(160, 10),
(161, 10),
(162, 10),
(163, 10),
(164, 10),
(189, 11),
(190, 11),
(191, 11),
(192, 11),
(193, 11),
(218, 12),
(219, 12),
(220, 12),
(221, 12),
(222, 12),
(267, 13),
(268, 13),
(269, 13),
(270, 13),
(271, 13);

-- --------------------------------------------------------

--
-- Estrutura para tabela `semana_bibliografia3`
--

CREATE TABLE `semana_bibliografia3` (
  `semana` int(11) NOT NULL,
  `bibliografia` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `semana_bibliografia3`
--

INSERT INTO `semana_bibliografia3` (`semana`, `bibliografia`) VALUES
(26, 6),
(27, 6),
(28, 6),
(29, 6),
(30, 6),
(40, 7),
(41, 7),
(42, 7),
(43, 7),
(44, 7),
(54, 8),
(55, 8),
(56, 8),
(57, 8),
(58, 8),
(68, 9),
(69, 9),
(70, 9),
(71, 9),
(72, 9),
(165, 10),
(166, 10),
(167, 10),
(168, 10),
(169, 10),
(194, 11),
(195, 11),
(196, 11),
(197, 11),
(198, 11),
(223, 12),
(224, 12),
(225, 12),
(226, 12),
(227, 12),
(272, 13),
(273, 13),
(274, 13),
(275, 13),
(276, 13);

-- --------------------------------------------------------

--
-- Estrutura para tabela `semana_cp2`
--

CREATE TABLE `semana_cp2` (
  `semana` int(11) NOT NULL,
  `cp` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `semana_cp2`
--

INSERT INTO `semana_cp2` (`semana`, `cp`) VALUES
(1, 5),
(137, 5),
(147, 5),
(208, 1),
(209, 1),
(210, 10),
(211, 10),
(212, 14),
(213, 14),
(261, 17),
(262, 17);

-- --------------------------------------------------------

--
-- Estrutura para tabela `semana_nivelamento1`
--

CREATE TABLE `semana_nivelamento1` (
  `semana` int(11) NOT NULL,
  `nivelamento` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `semana_nivelamento1`
--

INSERT INTO `semana_nivelamento1` (`semana`, `nivelamento`) VALUES
(77, 4),
(78, 4),
(79, 4),
(80, 4),
(81, 4),
(82, 4),
(83, 5),
(84, 5),
(85, 5),
(86, 5),
(87, 5),
(88, 5),
(170, 6),
(171, 6),
(172, 6),
(173, 6),
(174, 6),
(175, 6),
(228, 7),
(229, 7),
(230, 7),
(231, 7),
(232, 7),
(233, 7),
(277, 8),
(278, 8),
(279, 8),
(280, 8),
(281, 8),
(282, 8);

-- --------------------------------------------------------

--
-- Estrutura para tabela `semana_projeto`
--

CREATE TABLE `semana_projeto` (
  `semana` int(11) NOT NULL,
  `projeto` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura para tabela `semana_projeto1`
--

CREATE TABLE `semana_projeto1` (
  `semana` int(11) NOT NULL,
  `projeto` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `semana_projeto1`
--

INSERT INTO `semana_projeto1` (`semana`, `projeto`) VALUES
(89, 1),
(90, 1),
(91, 1),
(92, 1),
(93, 1),
(94, 1),
(95, 2),
(96, 2),
(97, 2),
(98, 2),
(99, 2),
(100, 2),
(101, 3),
(102, 3),
(103, 3),
(104, 3),
(105, 3),
(106, 3),
(234, 4),
(235, 4),
(236, 4),
(237, 4),
(238, 4),
(239, 4),
(255, 5),
(256, 5),
(257, 5),
(258, 5),
(259, 5),
(260, 5);

-- --------------------------------------------------------

--
-- Estrutura para tabela `tempos`
--

CREATE TABLE `tempos` (
  `temposid` int(11) NOT NULL COMMENT 'Identificador',
  `temposdescricao` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'DescriÃ§Ã£o',
  `tempostempo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Tempo (1 Âª...)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura para tabela `tempos_turnos1`
--

CREATE TABLE `tempos_turnos1` (
  `tempos` int(11) NOT NULL,
  `turnos` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura para tabela `tempos_turnos2`
--

CREATE TABLE `tempos_turnos2` (
  `tempos` int(11) NOT NULL,
  `turnos` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura para tabela `tempos_turnos3`
--

CREATE TABLE `tempos_turnos3` (
  `tempos` int(11) NOT NULL,
  `turnos` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura para tabela `turma`
--

CREATE TABLE `turma` (
  `turmaid` int(11) NOT NULL COMMENT 'Identificador',
  `turmaturma` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Turma'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `turma`
--

INSERT INTO `turma` (`turmaid`, `turmaturma`) VALUES
(1, '2018/1'),
(2, '2018/2');

-- --------------------------------------------------------

--
-- Estrutura para tabela `turnos`
--

CREATE TABLE `turnos` (
  `turnosid` int(11) NOT NULL COMMENT 'Identificador',
  `turnosdescricao` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'DescriÃ§Ã£o'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura para tabela `usuario`
--

CREATE TABLE `usuario` (
  `usuarioid` int(11) NOT NULL COMMENT 'Identificador',
  `usuarionome` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Nome',
  `usuarioemail` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'E-MAIL',
  `usuariosenha` text COLLATE utf8_unicode_ci,
  `usuariomatricula` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'matricula'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `usuario`
--

INSERT INTO `usuario` (`usuarioid`, `usuarionome`, `usuarioemail`, `usuariosenha`, `usuariomatricula`) VALUES
(1, 'Artur Romão Rocha', 'arturromaorocha@gmail.com', '223311', '29375');

-- --------------------------------------------------------

--
-- Estrutura para tabela `verbos`
--

CREATE TABLE `verbos` (
  `verbosid` int(11) NOT NULL COMMENT 'Identificador',
  `verbosverbo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Verbo'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `verbos`
--

INSERT INTO `verbos` (`verbosid`, `verbosverbo`) VALUES
(1, 'Executar'),
(2, 'Avaliar'),
(3, 'Analizar'),
(4, 'Reconhecer'),
(5, 'Identificar'),
(6, 'Comparar');

-- --------------------------------------------------------

--
-- Estrutura para tabela `verbos_dificuldade1`
--

CREATE TABLE `verbos_dificuldade1` (
  `verbos` int(11) NOT NULL,
  `dificuldade` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `verbos_dificuldade1`
--

INSERT INTO `verbos_dificuldade1` (`verbos`, `dificuldade`) VALUES
(1, 3),
(2, 1),
(2, 3),
(3, 1),
(3, 3),
(4, 2),
(4, 3),
(5, 2),
(5, 3),
(6, 3);

-- --------------------------------------------------------

--
-- Estrutura para tabela `verbos_semana6`
--

CREATE TABLE `verbos_semana6` (
  `verbos` int(11) NOT NULL,
  `semana` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `verbos_semana6`
--

INSERT INTO `verbos_semana6` (`verbos`, `semana`) VALUES
(2, 208),
(2, 261),
(3, 209),
(4, 212),
(5, 213);

--
-- Índices de tabelas apagadas
--

--
-- Índices de tabela `atividade`
--
ALTER TABLE `atividade`
  ADD PRIMARY KEY (`atividadeid`);

--
-- Índices de tabela `atividade_conjatividade1`
--
ALTER TABLE `atividade_conjatividade1`
  ADD PRIMARY KEY (`atividade`,`conjatividade`),
  ADD KEY `fk_conjatividadeatividadeconjatividade1` (`conjatividade`);

--
-- Índices de tabela `atp`
--
ALTER TABLE `atp`
  ADD PRIMARY KEY (`atpid`);

--
-- Índices de tabela `atvps`
--
ALTER TABLE `atvps`
  ADD PRIMARY KEY (`atvpsid`);

--
-- Índices de tabela `atvssasemana`
--
ALTER TABLE `atvssasemana`
  ADD PRIMARY KEY (`atvssasemanaid`);

--
-- Índices de tabela `bibliografia`
--
ALTER TABLE `bibliografia`
  ADD PRIMARY KEY (`bibliografiaid`);

--
-- Índices de tabela `bloco`
--
ALTER TABLE `bloco`
  ADD PRIMARY KEY (`blocoid`);

--
-- Índices de tabela `conjatividade`
--
ALTER TABLE `conjatividade`
  ADD PRIMARY KEY (`conjatividadeid`,`conjatividadeopsemanaid2`),
  ADD KEY `fk_conjatividadeopsemana2` (`conjatividadeopsemanaid2`);

--
-- Índices de tabela `conjatividade_atvssasemana1`
--
ALTER TABLE `conjatividade_atvssasemana1`
  ADD PRIMARY KEY (`conjatividade`,`atvssasemana`),
  ADD KEY `fk_atvssasemanaconjatividadeatvssasemana1` (`atvssasemana`);

--
-- Índices de tabela `cp`
--
ALTER TABLE `cp`
  ADD PRIMARY KEY (`cpid`);

--
-- Índices de tabela `cp_planodeensino8`
--
ALTER TABLE `cp_planodeensino8`
  ADD PRIMARY KEY (`cp`,`planodeensino`),
  ADD KEY `fk_planodeensinocpplanodeensino8` (`planodeensino`);

--
-- Índices de tabela `cronogramaav`
--
ALTER TABLE `cronogramaav`
  ADD PRIMARY KEY (`cronogramaavid`,`cronogramaavmetavaliativosid1`,`cronogramaavmetavaliativosid2`,`cronogramaavmetavaliativosid3`,`cronogramaavmetavaliativosid4`,`cronogramaavmetavaliativosid5`,`cronogramaavmetavaliativosid6`,`cronogramaavmetavaliativosid7`,`cronogramaavmetavaliativosid8`),
  ADD KEY `fk_cronogramaavmetavaliativos1` (`cronogramaavmetavaliativosid1`),
  ADD KEY `fk_cronogramaavmetavaliativos2` (`cronogramaavmetavaliativosid2`),
  ADD KEY `fk_cronogramaavmetavaliativos3` (`cronogramaavmetavaliativosid3`),
  ADD KEY `fk_cronogramaavmetavaliativos4` (`cronogramaavmetavaliativosid4`),
  ADD KEY `fk_cronogramaavmetavaliativos5` (`cronogramaavmetavaliativosid5`),
  ADD KEY `fk_cronogramaavmetavaliativos6` (`cronogramaavmetavaliativosid6`),
  ADD KEY `fk_cronogramaavmetavaliativos7` (`cronogramaavmetavaliativosid7`),
  ADD KEY `fk_cronogramaavmetavaliativos8` (`cronogramaavmetavaliativosid8`);

--
-- Índices de tabela `curso`
--
ALTER TABLE `curso`
  ADD PRIMARY KEY (`cursoid`);

--
-- Índices de tabela `dificuldade`
--
ALTER TABLE `dificuldade`
  ADD PRIMARY KEY (`dificuldadeid`);

--
-- Índices de tabela `dificuldade_semana5`
--
ALTER TABLE `dificuldade_semana5`
  ADD PRIMARY KEY (`dificuldade`,`semana`),
  ADD KEY `fk_semanadificuldadesemana5` (`semana`);

--
-- Índices de tabela `identificacao`
--
ALTER TABLE `identificacao`
  ADD PRIMARY KEY (`identificacaoid`,`identificacaousuarioid1`,`identificacaocursoid2`,`identificacaoturmaid3`),
  ADD KEY `fk_identificacaousuario1` (`identificacaousuarioid1`),
  ADD KEY `fk_identificacaocurso2` (`identificacaocursoid2`),
  ADD KEY `fk_identificacaoturma3` (`identificacaoturmaid3`);

--
-- Índices de tabela `itemeqt`
--
ALTER TABLE `itemeqt`
  ADD PRIMARY KEY (`itemeqtid`);

--
-- Índices de tabela `itemeqt_pap3`
--
ALTER TABLE `itemeqt_pap3`
  ADD PRIMARY KEY (`itemeqt`,`pap`),
  ADD KEY `fk_papitemeqtpap3` (`pap`);

--
-- Índices de tabela `laboratorios`
--
ALTER TABLE `laboratorios`
  ADD PRIMARY KEY (`laboratoriosid`);

--
-- Índices de tabela `metavaliativos`
--
ALTER TABLE `metavaliativos`
  ADD PRIMARY KEY (`metavaliativosid`);

--
-- Índices de tabela `nivelamento`
--
ALTER TABLE `nivelamento`
  ADD PRIMARY KEY (`nivelamentoid`);

--
-- Índices de tabela `opsemana`
--
ALTER TABLE `opsemana`
  ADD PRIMARY KEY (`opsemanaid`);

--
-- Índices de tabela `opsemana_semana2`
--
ALTER TABLE `opsemana_semana2`
  ADD PRIMARY KEY (`opsemana`,`semana`),
  ADD KEY `fk_semanaopsemanasemana2` (`semana`);

--
-- Índices de tabela `opsemana_semana4`
--
ALTER TABLE `opsemana_semana4`
  ADD PRIMARY KEY (`opsemana`,`semana`),
  ADD KEY `fk_semanaopsemanasemana4` (`semana`);

--
-- Índices de tabela `pap`
--
ALTER TABLE `pap`
  ADD PRIMARY KEY (`papid`,`papturnosid1`,`paplaboratoriosid2`),
  ADD KEY `fk_papturnos1` (`papturnosid1`),
  ADD KEY `fk_paplaboratorios2` (`paplaboratoriosid2`);

--
-- Índices de tabela `pap_planodeensino10`
--
ALTER TABLE `pap_planodeensino10`
  ADD PRIMARY KEY (`pap`,`planodeensino`),
  ADD KEY `fk_planodeensinopapplanodeensino10` (`planodeensino`);

--
-- Índices de tabela `planodeensino`
--
ALTER TABLE `planodeensino`
  ADD PRIMARY KEY (`planodeensinoid`);

--
-- Índices de tabela `projeto`
--
ALTER TABLE `projeto`
  ADD PRIMARY KEY (`projetoid`);

--
-- Índices de tabela `prova`
--
ALTER TABLE `prova`
  ADD PRIMARY KEY (`provaid`);

--
-- Índices de tabela `prova_cp1`
--
ALTER TABLE `prova_cp1`
  ADD PRIMARY KEY (`prova`,`cp`),
  ADD KEY `fk_cpprovacp1` (`cp`);

--
-- Índices de tabela `semana`
--
ALTER TABLE `semana`
  ADD PRIMARY KEY (`semanaid`,`semanablocoid1`,`semanablocoid3`),
  ADD KEY `fk_semanabloco1` (`semanablocoid1`),
  ADD KEY `fk_semanabloco3` (`semanablocoid3`);

--
-- Índices de tabela `semana_atp1`
--
ALTER TABLE `semana_atp1`
  ADD PRIMARY KEY (`semana`,`atp`),
  ADD KEY `fk_atpsemanaatp1` (`atp`);

--
-- Índices de tabela `semana_atvps1`
--
ALTER TABLE `semana_atvps1`
  ADD PRIMARY KEY (`semana`,`atvps`),
  ADD KEY `fk_atvpssemanaatvps1` (`atvps`);

--
-- Índices de tabela `semana_bibliografia1`
--
ALTER TABLE `semana_bibliografia1`
  ADD PRIMARY KEY (`semana`,`bibliografia`),
  ADD KEY `fk_bibliografiasemanabibliografia1` (`bibliografia`);

--
-- Índices de tabela `semana_bibliografia2`
--
ALTER TABLE `semana_bibliografia2`
  ADD PRIMARY KEY (`semana`,`bibliografia`),
  ADD KEY `fk_bibliografiasemanabibliografia2` (`bibliografia`);

--
-- Índices de tabela `semana_bibliografia3`
--
ALTER TABLE `semana_bibliografia3`
  ADD PRIMARY KEY (`semana`,`bibliografia`),
  ADD KEY `fk_bibliografiasemanabibliografia3` (`bibliografia`);

--
-- Índices de tabela `semana_cp2`
--
ALTER TABLE `semana_cp2`
  ADD PRIMARY KEY (`semana`,`cp`),
  ADD KEY `fk_cpsemanacp2` (`cp`);

--
-- Índices de tabela `semana_nivelamento1`
--
ALTER TABLE `semana_nivelamento1`
  ADD PRIMARY KEY (`semana`,`nivelamento`),
  ADD KEY `fk_nivelamentosemananivelamento1` (`nivelamento`);

--
-- Índices de tabela `semana_projeto1`
--
ALTER TABLE `semana_projeto1`
  ADD PRIMARY KEY (`semana`,`projeto`),
  ADD KEY `fk_npj1` (`projeto`);

--
-- Índices de tabela `tempos`
--
ALTER TABLE `tempos`
  ADD PRIMARY KEY (`temposid`);

--
-- Índices de tabela `tempos_turnos1`
--
ALTER TABLE `tempos_turnos1`
  ADD PRIMARY KEY (`tempos`,`turnos`),
  ADD KEY `fk_turnostemposturnos1` (`turnos`);

--
-- Índices de tabela `tempos_turnos2`
--
ALTER TABLE `tempos_turnos2`
  ADD PRIMARY KEY (`tempos`,`turnos`),
  ADD KEY `fk_turnostemposturnos2` (`turnos`);

--
-- Índices de tabela `tempos_turnos3`
--
ALTER TABLE `tempos_turnos3`
  ADD PRIMARY KEY (`tempos`,`turnos`),
  ADD KEY `fk_turnostemposturnos3` (`turnos`);

--
-- Índices de tabela `turma`
--
ALTER TABLE `turma`
  ADD PRIMARY KEY (`turmaid`);

--
-- Índices de tabela `turnos`
--
ALTER TABLE `turnos`
  ADD PRIMARY KEY (`turnosid`);

--
-- Índices de tabela `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`usuarioid`);

--
-- Índices de tabela `verbos`
--
ALTER TABLE `verbos`
  ADD PRIMARY KEY (`verbosid`);

--
-- Índices de tabela `verbos_dificuldade1`
--
ALTER TABLE `verbos_dificuldade1`
  ADD PRIMARY KEY (`verbos`,`dificuldade`),
  ADD KEY `fk_dificuldadeverbosdificuldade1` (`dificuldade`);

--
-- Índices de tabela `verbos_semana6`
--
ALTER TABLE `verbos_semana6`
  ADD PRIMARY KEY (`verbos`,`semana`),
  ADD KEY `fk_semanaverbossemana6` (`semana`);

--
-- AUTO_INCREMENT de tabelas apagadas
--

--
-- AUTO_INCREMENT de tabela `atividade`
--
ALTER TABLE `atividade`
  MODIFY `atividadeid` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Identificador';

--
-- AUTO_INCREMENT de tabela `atp`
--
ALTER TABLE `atp`
  MODIFY `atpid` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Identificador', AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de tabela `atvps`
--
ALTER TABLE `atvps`
  MODIFY `atvpsid` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Identificador', AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de tabela `atvssasemana`
--
ALTER TABLE `atvssasemana`
  MODIFY `atvssasemanaid` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Identificador';

--
-- AUTO_INCREMENT de tabela `bibliografia`
--
ALTER TABLE `bibliografia`
  MODIFY `bibliografiaid` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Identificador', AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT de tabela `bloco`
--
ALTER TABLE `bloco`
  MODIFY `blocoid` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Identificador', AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de tabela `conjatividade`
--
ALTER TABLE `conjatividade`
  MODIFY `conjatividadeid` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Identificador';

--
-- AUTO_INCREMENT de tabela `cp`
--
ALTER TABLE `cp`
  MODIFY `cpid` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Identificador', AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT de tabela `cronogramaav`
--
ALTER TABLE `cronogramaav`
  MODIFY `cronogramaavid` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Identificador', AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT de tabela `curso`
--
ALTER TABLE `curso`
  MODIFY `cursoid` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Identificador', AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de tabela `dificuldade`
--
ALTER TABLE `dificuldade`
  MODIFY `dificuldadeid` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Identificador', AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de tabela `identificacao`
--
ALTER TABLE `identificacao`
  MODIFY `identificacaoid` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Identificador', AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT de tabela `itemeqt`
--
ALTER TABLE `itemeqt`
  MODIFY `itemeqtid` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Identificador';

--
-- AUTO_INCREMENT de tabela `laboratorios`
--
ALTER TABLE `laboratorios`
  MODIFY `laboratoriosid` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Identificador', AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de tabela `metavaliativos`
--
ALTER TABLE `metavaliativos`
  MODIFY `metavaliativosid` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Identificador', AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT de tabela `nivelamento`
--
ALTER TABLE `nivelamento`
  MODIFY `nivelamentoid` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Identificador', AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de tabela `opsemana`
--
ALTER TABLE `opsemana`
  MODIFY `opsemanaid` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Identificador', AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT de tabela `pap`
--
ALTER TABLE `pap`
  MODIFY `papid` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Identificador';

--
-- AUTO_INCREMENT de tabela `planodeensino`
--
ALTER TABLE `planodeensino`
  MODIFY `planodeensinoid` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Identificador', AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de tabela `projeto`
--
ALTER TABLE `projeto`
  MODIFY `projetoid` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Identificador', AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de tabela `prova`
--
ALTER TABLE `prova`
  MODIFY `provaid` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Identificador', AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de tabela `semana`
--
ALTER TABLE `semana`
  MODIFY `semanaid` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Identificador', AUTO_INCREMENT=292;

--
-- AUTO_INCREMENT de tabela `tempos`
--
ALTER TABLE `tempos`
  MODIFY `temposid` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Identificador';

--
-- AUTO_INCREMENT de tabela `turma`
--
ALTER TABLE `turma`
  MODIFY `turmaid` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Identificador', AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de tabela `turnos`
--
ALTER TABLE `turnos`
  MODIFY `turnosid` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Identificador';

--
-- AUTO_INCREMENT de tabela `usuario`
--
ALTER TABLE `usuario`
  MODIFY `usuarioid` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Identificador', AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de tabela `verbos`
--
ALTER TABLE `verbos`
  MODIFY `verbosid` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Identificador', AUTO_INCREMENT=7;

--
-- Restrições para dumps de tabelas
--

--
-- Restrições para tabelas `atividade_conjatividade1`
--
ALTER TABLE `atividade_conjatividade1`
  ADD CONSTRAINT `fk_conjatividadeatividadeatividade1` FOREIGN KEY (`atividade`) REFERENCES `atividade` (`atividadeid`),
  ADD CONSTRAINT `fk_conjatividadeatividadeconjatividade1` FOREIGN KEY (`conjatividade`) REFERENCES `conjatividade` (`conjatividadeid`);

--
-- Restrições para tabelas `conjatividade`
--
ALTER TABLE `conjatividade`
  ADD CONSTRAINT `fk_conjatividadeopsemana2` FOREIGN KEY (`conjatividadeopsemanaid2`) REFERENCES `opsemana` (`opsemanaid`);

--
-- Restrições para tabelas `conjatividade_atvssasemana1`
--
ALTER TABLE `conjatividade_atvssasemana1`
  ADD CONSTRAINT `fk_atvssasemanaconjatividadeatvssasemana1` FOREIGN KEY (`atvssasemana`) REFERENCES `atvssasemana` (`atvssasemanaid`),
  ADD CONSTRAINT `fk_atvssasemanaconjatividadeconjatividade1` FOREIGN KEY (`conjatividade`) REFERENCES `conjatividade` (`conjatividadeid`);

--
-- Restrições para tabelas `cp_planodeensino8`
--
ALTER TABLE `cp_planodeensino8`
  ADD CONSTRAINT `fk_planodeensinocpcp8` FOREIGN KEY (`cp`) REFERENCES `cp` (`cpid`),
  ADD CONSTRAINT `fk_planodeensinocpplanodeensino8` FOREIGN KEY (`planodeensino`) REFERENCES `planodeensino` (`planodeensinoid`);

--
-- Restrições para tabelas `cronogramaav`
--
ALTER TABLE `cronogramaav`
  ADD CONSTRAINT `fk_cronogramaavmetavaliativos1` FOREIGN KEY (`cronogramaavmetavaliativosid1`) REFERENCES `metavaliativos` (`metavaliativosid`),
  ADD CONSTRAINT `fk_cronogramaavmetavaliativos2` FOREIGN KEY (`cronogramaavmetavaliativosid2`) REFERENCES `metavaliativos` (`metavaliativosid`),
  ADD CONSTRAINT `fk_cronogramaavmetavaliativos3` FOREIGN KEY (`cronogramaavmetavaliativosid3`) REFERENCES `metavaliativos` (`metavaliativosid`),
  ADD CONSTRAINT `fk_cronogramaavmetavaliativos4` FOREIGN KEY (`cronogramaavmetavaliativosid4`) REFERENCES `metavaliativos` (`metavaliativosid`),
  ADD CONSTRAINT `fk_cronogramaavmetavaliativos5` FOREIGN KEY (`cronogramaavmetavaliativosid5`) REFERENCES `metavaliativos` (`metavaliativosid`),
  ADD CONSTRAINT `fk_cronogramaavmetavaliativos6` FOREIGN KEY (`cronogramaavmetavaliativosid6`) REFERENCES `metavaliativos` (`metavaliativosid`),
  ADD CONSTRAINT `fk_cronogramaavmetavaliativos7` FOREIGN KEY (`cronogramaavmetavaliativosid7`) REFERENCES `metavaliativos` (`metavaliativosid`),
  ADD CONSTRAINT `fk_cronogramaavmetavaliativos8` FOREIGN KEY (`cronogramaavmetavaliativosid8`) REFERENCES `metavaliativos` (`metavaliativosid`);

--
-- Restrições para tabelas `dificuldade_semana5`
--
ALTER TABLE `dificuldade_semana5`
  ADD CONSTRAINT `fk_semanadificuldadedificuldade5` FOREIGN KEY (`dificuldade`) REFERENCES `dificuldade` (`dificuldadeid`),
  ADD CONSTRAINT `fk_semanadificuldadesemana5` FOREIGN KEY (`semana`) REFERENCES `semana` (`semanaid`);

--
-- Restrições para tabelas `identificacao`
--
ALTER TABLE `identificacao`
  ADD CONSTRAINT `fk_identificacaocurso2` FOREIGN KEY (`identificacaocursoid2`) REFERENCES `curso` (`cursoid`),
  ADD CONSTRAINT `fk_identificacaoturma3` FOREIGN KEY (`identificacaoturmaid3`) REFERENCES `turma` (`turmaid`),
  ADD CONSTRAINT `fk_identificacaousuario1` FOREIGN KEY (`identificacaousuarioid1`) REFERENCES `usuario` (`usuarioid`);

--
-- Restrições para tabelas `itemeqt_pap3`
--
ALTER TABLE `itemeqt_pap3`
  ADD CONSTRAINT `fk_papitemeqtitemeqt3` FOREIGN KEY (`itemeqt`) REFERENCES `itemeqt` (`itemeqtid`),
  ADD CONSTRAINT `fk_papitemeqtpap3` FOREIGN KEY (`pap`) REFERENCES `pap` (`papid`);

--
-- Restrições para tabelas `opsemana_semana2`
--
ALTER TABLE `opsemana_semana2`
  ADD CONSTRAINT `fk_semanaopsemanaopsemana2` FOREIGN KEY (`opsemana`) REFERENCES `opsemana` (`opsemanaid`),
  ADD CONSTRAINT `fk_semanaopsemanasemana2` FOREIGN KEY (`semana`) REFERENCES `semana` (`semanaid`);

--
-- Restrições para tabelas `opsemana_semana4`
--
ALTER TABLE `opsemana_semana4`
  ADD CONSTRAINT `fk_semanaopsemanaopsemana4` FOREIGN KEY (`opsemana`) REFERENCES `opsemana` (`opsemanaid`),
  ADD CONSTRAINT `fk_semanaopsemanasemana4` FOREIGN KEY (`semana`) REFERENCES `semana` (`semanaid`);

--
-- Restrições para tabelas `pap`
--
ALTER TABLE `pap`
  ADD CONSTRAINT `fk_paplaboratorios2` FOREIGN KEY (`paplaboratoriosid2`) REFERENCES `laboratorios` (`laboratoriosid`),
  ADD CONSTRAINT `fk_papturnos1` FOREIGN KEY (`papturnosid1`) REFERENCES `turnos` (`turnosid`);

--
-- Restrições para tabelas `pap_planodeensino10`
--
ALTER TABLE `pap_planodeensino10`
  ADD CONSTRAINT `fk_planodeensinopappap10` FOREIGN KEY (`pap`) REFERENCES `pap` (`papid`),
  ADD CONSTRAINT `fk_planodeensinopapplanodeensino10` FOREIGN KEY (`planodeensino`) REFERENCES `planodeensino` (`planodeensinoid`);

--
-- Restrições para tabelas `prova_cp1`
--
ALTER TABLE `prova_cp1`
  ADD CONSTRAINT `fk_cpprovacp1` FOREIGN KEY (`cp`) REFERENCES `cp` (`cpid`),
  ADD CONSTRAINT `fk_cpprovaprova1` FOREIGN KEY (`prova`) REFERENCES `prova` (`provaid`);

--
-- Restrições para tabelas `semana`
--
ALTER TABLE `semana`
  ADD CONSTRAINT `fk_semanabloco1` FOREIGN KEY (`semanablocoid1`) REFERENCES `bloco` (`blocoid`),
  ADD CONSTRAINT `fk_semanabloco3` FOREIGN KEY (`semanablocoid3`) REFERENCES `bloco` (`blocoid`);

--
-- Restrições para tabelas `semana_atp1`
--
ALTER TABLE `semana_atp1`
  ADD CONSTRAINT `fk_atpsemanaatp1` FOREIGN KEY (`atp`) REFERENCES `atp` (`atpid`),
  ADD CONSTRAINT `fk_atpsemanasemana1` FOREIGN KEY (`semana`) REFERENCES `semana` (`semanaid`);

--
-- Restrições para tabelas `semana_atvps1`
--
ALTER TABLE `semana_atvps1`
  ADD CONSTRAINT `fk_atvpssemanaatvps1` FOREIGN KEY (`atvps`) REFERENCES `atvps` (`atvpsid`),
  ADD CONSTRAINT `fk_atvpssemanasemana1` FOREIGN KEY (`semana`) REFERENCES `semana` (`semanaid`);

--
-- Restrições para tabelas `semana_bibliografia1`
--
ALTER TABLE `semana_bibliografia1`
  ADD CONSTRAINT `fk_bibliografiasemanabibliografia1` FOREIGN KEY (`bibliografia`) REFERENCES `bibliografia` (`bibliografiaid`),
  ADD CONSTRAINT `fk_bibliografiasemanasemana1` FOREIGN KEY (`semana`) REFERENCES `semana` (`semanaid`);

--
-- Restrições para tabelas `semana_bibliografia2`
--
ALTER TABLE `semana_bibliografia2`
  ADD CONSTRAINT `fk_bibliografiasemanabibliografia2` FOREIGN KEY (`bibliografia`) REFERENCES `bibliografia` (`bibliografiaid`),
  ADD CONSTRAINT `fk_bibliografiasemanasemana2` FOREIGN KEY (`semana`) REFERENCES `semana` (`semanaid`);

--
-- Restrições para tabelas `semana_bibliografia3`
--
ALTER TABLE `semana_bibliografia3`
  ADD CONSTRAINT `fk_bibliografiasemanabibliografia3` FOREIGN KEY (`bibliografia`) REFERENCES `bibliografia` (`bibliografiaid`),
  ADD CONSTRAINT `fk_bibliografiasemanasemana3` FOREIGN KEY (`semana`) REFERENCES `semana` (`semanaid`);

--
-- Restrições para tabelas `semana_cp2`
--
ALTER TABLE `semana_cp2`
  ADD CONSTRAINT `fk_cpsemanacp2` FOREIGN KEY (`cp`) REFERENCES `cp` (`cpid`),
  ADD CONSTRAINT `fk_cpsemanasemana2` FOREIGN KEY (`semana`) REFERENCES `semana` (`semanaid`);

--
-- Restrições para tabelas `semana_nivelamento1`
--
ALTER TABLE `semana_nivelamento1`
  ADD CONSTRAINT `fk_nivelamentosemananivelamento1` FOREIGN KEY (`nivelamento`) REFERENCES `nivelamento` (`nivelamentoid`),
  ADD CONSTRAINT `fk_nivelamentosemanasemana1` FOREIGN KEY (`semana`) REFERENCES `semana` (`semanaid`);

--
-- Restrições para tabelas `semana_projeto1`
--
ALTER TABLE `semana_projeto1`
  ADD CONSTRAINT `fk_nivelamentosemananivelamentewo1` FOREIGN KEY (`projeto`) REFERENCES `projeto` (`projetoid`),
  ADD CONSTRAINT `fk_nivelamentosemanasemawna1` FOREIGN KEY (`semana`) REFERENCES `semana` (`semanaid`);

--
-- Restrições para tabelas `tempos_turnos1`
--
ALTER TABLE `tempos_turnos1`
  ADD CONSTRAINT `fk_turnostempostempos1` FOREIGN KEY (`tempos`) REFERENCES `tempos` (`temposid`),
  ADD CONSTRAINT `fk_turnostemposturnos1` FOREIGN KEY (`turnos`) REFERENCES `turnos` (`turnosid`);

--
-- Restrições para tabelas `tempos_turnos2`
--
ALTER TABLE `tempos_turnos2`
  ADD CONSTRAINT `fk_turnostempostempos2` FOREIGN KEY (`tempos`) REFERENCES `tempos` (`temposid`),
  ADD CONSTRAINT `fk_turnostemposturnos2` FOREIGN KEY (`turnos`) REFERENCES `turnos` (`turnosid`);

--
-- Restrições para tabelas `tempos_turnos3`
--
ALTER TABLE `tempos_turnos3`
  ADD CONSTRAINT `fk_turnostempostempos3` FOREIGN KEY (`tempos`) REFERENCES `tempos` (`temposid`),
  ADD CONSTRAINT `fk_turnostemposturnos3` FOREIGN KEY (`turnos`) REFERENCES `turnos` (`turnosid`);

--
-- Restrições para tabelas `verbos_dificuldade1`
--
ALTER TABLE `verbos_dificuldade1`
  ADD CONSTRAINT `fk_dificuldadeverbosdificuldade1` FOREIGN KEY (`dificuldade`) REFERENCES `dificuldade` (`dificuldadeid`),
  ADD CONSTRAINT `fk_dificuldadeverbosverbos1` FOREIGN KEY (`verbos`) REFERENCES `verbos` (`verbosid`);

--
-- Restrições para tabelas `verbos_semana6`
--
ALTER TABLE `verbos_semana6`
  ADD CONSTRAINT `fk_semanaverbossemana6` FOREIGN KEY (`semana`) REFERENCES `semana` (`semanaid`),
  ADD CONSTRAINT `fk_semanaverbosverbos6` FOREIGN KEY (`verbos`) REFERENCES `verbos` (`verbosid`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
