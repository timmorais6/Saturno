-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Tempo de geração: 23/04/2018 às 18:37
-- Versão do servidor: 10.1.32-MariaDB
-- Versão do PHP: 7.0.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `u673048033_pe`
--

-- --------------------------------------------------------

--
-- Estrutura para tabela `atividade`
--

CREATE TABLE `atividade` (
  `atividadeid` int(11) NOT NULL COMMENT 'Identificador',
  `atividadedescricao` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'DescriÃ§Ã£o',
  `atividadeatividade` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura para tabela `atividade_conjatividade1`
--

CREATE TABLE `atividade_conjatividade1` (
  `atividade` int(11) NOT NULL,
  `conjatividade` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura para tabela `atp`
--

CREATE TABLE `atp` (
  `atpid` int(11) NOT NULL COMMENT 'Identificador',
  `atpdescricao` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'DescriÃ§Ã£o'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura para tabela `atvps`
--

CREATE TABLE `atvps` (
  `atvpsid` int(11) NOT NULL COMMENT 'Identificador',
  `atvpsdescricao` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'DescriÃ§Ã£o',
  `atvpssemanaid7` int(11) NOT NULL COMMENT 'FÃ³rum (OrientaÃ§Ã£o e SupervisÃ£o)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura para tabela `atvssasemana`
--

CREATE TABLE `atvssasemana` (
  `atvssasemanaid` int(11) NOT NULL COMMENT 'Identificador',
  `atvssasemanadescricao` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'DescriÃ§Ã£o'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura para tabela `bibliografia`
--

CREATE TABLE `bibliografia` (
  `bibliografiaid` int(11) NOT NULL COMMENT 'Identificador',
  `bibliografiadescricao` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'DescriÃ§Ã£o'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `bibliografia`
--

INSERT INTO `bibliografia` (`bibliografiaid`, `bibliografiadescricao`) VALUES
(1, 'Modelo padrão'),
(2, 'Modelo padrão'),
(3, 'Modelo padrão'),
(4, 'Modelo padrão'),
(5, 'Modelo padrão'),
(6, 'Modelo padrão'),
(7, 'Modelo padrão'),
(8, 'Modelo padrão'),
(9, 'Modelo padrãoa');

-- --------------------------------------------------------

--
-- Estrutura para tabela `bloco`
--

CREATE TABLE `bloco` (
  `blocoid` int(11) NOT NULL COMMENT 'Identificador',
  `blocobloco` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'bloco'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `bloco`
--

INSERT INTO `bloco` (`blocoid`, `blocobloco`) VALUES
(1, 'Bloco 1'),
(2, 'Bloco 2');

-- --------------------------------------------------------

--
-- Estrutura para tabela `conjatividade`
--

CREATE TABLE `conjatividade` (
  `conjatividadeid` int(11) NOT NULL COMMENT 'Identificador',
  `conjatividadedescricao` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'DescriÃ§Ã£o',
  `conjatividadeopsemanaid2` int(11) NOT NULL COMMENT 'Semana'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura para tabela `conjatividade_atvssasemana1`
--

CREATE TABLE `conjatividade_atvssasemana1` (
  `conjatividade` int(11) NOT NULL,
  `atvssasemana` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura para tabela `cp`
--

CREATE TABLE `cp` (
  `cpid` int(11) NOT NULL COMMENT 'Identificador',
  `cpnome` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Nome da unidade'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura para tabela `cp_planodeensino8`
--

CREATE TABLE `cp_planodeensino8` (
  `cp` int(11) NOT NULL,
  `planodeensino` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura para tabela `cronogramaav`
--

CREATE TABLE `cronogramaav` (
  `cronogramaavid` int(11) NOT NULL COMMENT 'Identificador',
  `cronogramaavn11` date DEFAULT NULL COMMENT 'N1.1',
  `cronogramaavn12` date DEFAULT NULL COMMENT 'N1.2',
  `cronogramaavn13` date DEFAULT NULL COMMENT 'N1.3',
  `cronogramaavn21` date DEFAULT NULL COMMENT 'N2.1',
  `cronogramaavn22` date DEFAULT NULL COMMENT 'N2.2',
  `cronogramaavn23` date DEFAULT NULL COMMENT 'N2.3',
  `cronogramaavn24` date DEFAULT NULL COMMENT 'N2.4',
  `cronogramaavn25` date DEFAULT NULL COMMENT 'N2.5',
  `cronogramaavdescricao` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'DescriÃ§Ã£o',
  `cronogramaavmetavaliativosid1` int(11) NOT NULL COMMENT 'N1.1 MÃ©todo de avaliaÃ§Ã£o',
  `cronogramaavmetavaliativosid2` int(11) NOT NULL COMMENT 'N1.2 MÃ©todo de avaliaÃ§Ã£o',
  `cronogramaavmetavaliativosid3` int(11) NOT NULL COMMENT 'N1.3 MÃ©todo de avaliaÃ§Ã£o',
  `cronogramaavmetavaliativosid4` int(11) NOT NULL COMMENT 'N2.1 MÃ©todo de avaliaÃ§Ã£o',
  `cronogramaavmetavaliativosid5` int(11) NOT NULL COMMENT 'N2.2 MÃ©todo de avaliaÃ§Ã£o',
  `cronogramaavmetavaliativosid6` int(11) NOT NULL COMMENT 'N2.3 MÃ©todo de avaliaÃ§Ã£o',
  `cronogramaavmetavaliativosid7` int(11) NOT NULL COMMENT 'N2.4 MÃ©todo de avaliaÃ§Ã£o',
  `cronogramaavmetavaliativosid8` int(11) NOT NULL COMMENT 'N2.5 MÃ©todo de avaliaÃ§Ã£o'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura para tabela `curso`
--

CREATE TABLE `curso` (
  `cursoid` int(11) NOT NULL COMMENT 'Identificador',
  `cursocurso` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Curso'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `curso`
--

INSERT INTO `curso` (`cursoid`, `cursocurso`) VALUES
(1, 'FARMÁCIA'),
(2, 'ADMINISTRAÇÃO');

-- --------------------------------------------------------

--
-- Estrutura para tabela `dificuldade`
--

CREATE TABLE `dificuldade` (
  `dificuldadeid` int(11) NOT NULL COMMENT 'Identificador',
  `dificuldadegrau` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Grau'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `dificuldade`
--

INSERT INTO `dificuldade` (`dificuldadeid`, `dificuldadegrau`) VALUES
(1, 'Médio');

-- --------------------------------------------------------

--
-- Estrutura para tabela `dificuldade_semana5`
--

CREATE TABLE `dificuldade_semana5` (
  `dificuldade` int(11) NOT NULL,
  `semana` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura para tabela `identificacao`
--

CREATE TABLE `identificacao` (
  `identificacaoid` int(11) NOT NULL COMMENT 'Identificador',
  `identificacaouc` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Unidade Curricular ',
  `identificacaochtt` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Carga HorÃ¡ria Total (Horas)',
  `identificacaocht` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Carga HorÃ¡ria TeÃ³rica (Horas/Aula)',
  `identificacaochp` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Carga HorÃ¡ria PrÃ¡tica l (Horas/Aula)',
  `identificacaochaol` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Carga HorÃ¡ria AOL (Horas/Aula)',
  `identificacaochaps` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Carga HorÃ¡ria APS (Horas/Aula)',
  `identificacaoementa` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'EMENTA',
  `identificacaoobjgeral` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'OBJETIVO GERAL',
  `identificacaousuarioid1` int(11) NOT NULL COMMENT 'Docente',
  `identificacaocursoid2` int(11) NOT NULL COMMENT 'Curso',
  `identificacaoturmaid3` int(11) NOT NULL COMMENT 'Turma'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `identificacao`
--

INSERT INTO `identificacao` (`identificacaoid`, `identificacaouc`, `identificacaochtt`, `identificacaocht`, `identificacaochp`, `identificacaochaol`, `identificacaochaps`, `identificacaoementa`, `identificacaoobjgeral`, `identificacaousuarioid1`, `identificacaocursoid2`, `identificacaoturmaid3`) VALUES
(1, 'Artur Romão Rocha', '3', '0', '0', '0', '0', '<p><br><br></p>', '<p>asasdfasfasdf</p><table class=\'table table-bordered\'><tbody><tr><td>as</td><td>a</td></tr><tr><td>as\'&nbsp;</td><td>as</td></tr><tr><td>\'</td><td><br></td></tr><tr><td><br></td><td><br></td></tr></tbody></table><p>asd</p>', 1, 2, 1),
(2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1),
(3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1),
(4, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1),
(5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1),
(6, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1),
(7, 'Coração', NULL, NULL, NULL, NULL, NULL, NULL, '<p>nkdnkdfngkdngdkfngdkngdkngdkdknwg10 n]´2-,ngreglg</p>', 1, 1, 1),
(8, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1),
(9, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1),
(10, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1);

-- --------------------------------------------------------

--
-- Estrutura para tabela `itemeqt`
--

CREATE TABLE `itemeqt` (
  `itemeqtid` int(11) NOT NULL COMMENT 'Identificador',
  `itemeqtdescricao` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'DescriÃ§Ã£o',
  `itemeqtitem` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Material',
  `itemeqtquantidade` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Quantidade'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura para tabela `itemeqt_pap3`
--

CREATE TABLE `itemeqt_pap3` (
  `itemeqt` int(11) NOT NULL,
  `pap` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura para tabela `laboratorios`
--

CREATE TABLE `laboratorios` (
  `laboratoriosid` int(11) NOT NULL COMMENT 'Identificador',
  `laboratorioslab` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'LaboratÃ³rio',
  `laboratoriosdesc` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'DescriÃ§Ã£o'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `laboratorios`
--

INSERT INTO `laboratorios` (`laboratoriosid`, `laboratorioslab`, `laboratoriosdesc`) VALUES
(1, 'Informática', ''),
(2, 'Informática', '');

-- --------------------------------------------------------

--
-- Estrutura para tabela `metavaliativos`
--

CREATE TABLE `metavaliativos` (
  `metavaliativosid` int(11) NOT NULL COMMENT 'Identificador',
  `metavaliativosmetodo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'MÃ©todo',
  `metavaliativospeso` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Peso'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `metavaliativos`
--

INSERT INTO `metavaliativos` (`metavaliativosid`, `metavaliativosmetodo`, `metavaliativospeso`) VALUES
(1, 'Prova', '10');

-- --------------------------------------------------------

--
-- Estrutura para tabela `nivelamento`
--

CREATE TABLE `nivelamento` (
  `nivelamentoid` int(11) NOT NULL COMMENT 'Identificador',
  `nivelamentodescricao` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'DescriÃ§Ã£o'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura para tabela `opsemana`
--

CREATE TABLE `opsemana` (
  `opsemanaid` int(11) NOT NULL COMMENT 'Identificador',
  `opsemanaop` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Semana'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `opsemana`
--

INSERT INTO `opsemana` (`opsemanaid`, `opsemanaop`) VALUES
(1, 'Semana 1'),
(2, 'Semana 2'),
(3, 'Semana 3'),
(4, 'Semana 4'),
(5, 'Semana 5'),
(6, 'Semana 6'),
(7, 'Semana 7'),
(8, 'Semana 8'),
(9, 'Semana 9'),
(10, 'Semana 10');

-- --------------------------------------------------------

--
-- Estrutura para tabela `opsemana_semana2`
--

CREATE TABLE `opsemana_semana2` (
  `opsemana` int(11) NOT NULL,
  `semana` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `opsemana_semana2`
--

INSERT INTO `opsemana_semana2` (`opsemana`, `semana`) VALUES
(1, 1),
(1, 59),
(1, 69),
(2, 1),
(3, 1),
(4, 1),
(5, 1),
(6, 1),
(7, 1),
(8, 1),
(9, 1),
(10, 1);

-- --------------------------------------------------------

--
-- Estrutura para tabela `opsemana_semana4`
--

CREATE TABLE `opsemana_semana4` (
  `opsemana` int(11) NOT NULL,
  `semana` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `opsemana_semana4`
--

INSERT INTO `opsemana_semana4` (`opsemana`, `semana`) VALUES
(1, 59);

-- --------------------------------------------------------

--
-- Estrutura para tabela `pap`
--

CREATE TABLE `pap` (
  `papid` int(11) NOT NULL COMMENT 'Identificador',
  `papnum` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'NÃšMERO DO PROTOCOLO',
  `papdia` date DEFAULT NULL COMMENT 'DATA',
  `paptitulo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'TÃTULO',
  `papobj` text COLLATE utf8_unicode_ci,
  `papfunteo` text COLLATE utf8_unicode_ci,
  `pappromet` text COLLATE utf8_unicode_ci,
  `papresespvalref` text COLLATE utf8_unicode_ci,
  `papdescricao` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'DescriÃ§Ã£o',
  `papturnosid1` int(11) NOT NULL COMMENT 'TURNO DE EXECUÃ‡ÃƒO',
  `paplaboratoriosid2` int(11) NOT NULL COMMENT 'LABORATÃ“RIO'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura para tabela `pap_planodeensino10`
--

CREATE TABLE `pap_planodeensino10` (
  `pap` int(11) NOT NULL,
  `planodeensino` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura para tabela `planodeensino`
--

CREATE TABLE `planodeensino` (
  `planodeensinoid` int(11) NOT NULL COMMENT 'Identificador',
  `planodeensinodescricao` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Descrição',
  `planodeensinodia` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Data e hora',
  `planodeensinousuarioid1` int(11) DEFAULT NULL COMMENT 'Docente',
  `planodeensinocursoid2` int(11) DEFAULT NULL COMMENT 'Curso',
  `planodeensinoidentificacaoid3` int(11) DEFAULT NULL COMMENT 'Identificação',
  `planodeensinoatvpsid4` int(11) DEFAULT NULL COMMENT 'Atividade Prática Supervisionada',
  `planodeensinoprojetoid5` int(11) DEFAULT NULL COMMENT 'Projeto ou Plano de ação (Estágio)',
  `planodeensinonivelamentoid6` int(11) DEFAULT NULL COMMENT 'Nivelamento',
  `planodeensinobibliografiaid7` int(11) DEFAULT NULL COMMENT 'Bibliografia',
  `planodeensinoatpid9` int(11) DEFAULT NULL COMMENT 'Atividades previstas',
  `planodeensinocronogramaavid11` int(11) DEFAULT NULL COMMENT 'Cronograma de avaliação',
  `planodeensinoatvssasemanaid12` int(11) DEFAULT NULL COMMENT 'Semanas (Atividades Sagah, Siga ou SSA)',
  `planodeensinoativo` int(11) DEFAULT NULL COMMENT 'ativo?'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `planodeensino`
--

INSERT INTO `planodeensino` (`planodeensinoid`, `planodeensinodescricao`, `planodeensinodia`, `planodeensinousuarioid1`, `planodeensinocursoid2`, `planodeensinoidentificacaoid3`, `planodeensinoatvpsid4`, `planodeensinoprojetoid5`, `planodeensinonivelamentoid6`, `planodeensinobibliografiaid7`, `planodeensinoatpid9`, `planodeensinocronogramaavid11`, `planodeensinoatvssasemanaid12`, `planodeensinoativo`) VALUES
(1, 'Introdução a Informatica', NULL, 0, NULL, 9, NULL, NULL, NULL, 9, NULL, NULL, NULL, NULL),
(2, 'a', NULL, 0, NULL, 2, NULL, NULL, NULL, 8, NULL, NULL, NULL, NULL),
(3, 'Anatomia', NULL, 0, NULL, 7, NULL, NULL, NULL, 6, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura para tabela `projeto`
--

CREATE TABLE `projeto` (
  `projetoid` int(11) NOT NULL COMMENT 'Identificador',
  `projetodescricao` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'DescriÃ§Ã£o'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura para tabela `prova`
--

CREATE TABLE `prova` (
  `provaid` int(11) NOT NULL COMMENT 'Identificador',
  `provaprova` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Prova'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `prova`
--

INSERT INTO `prova` (`provaid`, `provaprova`) VALUES
(1, 'Prova 1'),
(2, 'Prova 2');

-- --------------------------------------------------------

--
-- Estrutura para tabela `prova_cp1`
--

CREATE TABLE `prova_cp1` (
  `prova` int(11) NOT NULL,
  `cp` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura para tabela `semana`
--

CREATE TABLE `semana` (
  `semanaid` int(11) NOT NULL COMMENT 'Identificador',
  `semanadescricao` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'DescriÃ§Ã£o',
  `semanaatividade` text COLLATE utf8_unicode_ci,
  `semanablocoid1` int(11) NOT NULL COMMENT 'Bloco1',
  `semanablocoid3` int(11) NOT NULL COMMENT 'Bloco 2'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `semana`
--

INSERT INTO `semana` (`semanaid`, `semanadescricao`, `semanaatividade`, `semanablocoid1`, `semanablocoid3`) VALUES
(1, '', '', 1, 1),
(2, 'Bibliografia Básica 1', NULL, 1, 2),
(3, 'Bibliografia Básica 2', NULL, 1, 2),
(4, 'Bibliografia Básica 3', NULL, 1, 2),
(5, 'Bibliografia Básica 4', NULL, 1, 2),
(6, 'Bibliografia Básica 4', NULL, 1, 2),
(7, 'Bibliografia Básica 1', NULL, 1, 2),
(8, 'Bibliografia Básica 2', NULL, 1, 2),
(9, 'Bibliografia Básica 3', NULL, 1, 2),
(10, 'Bibliografia Básica 4', NULL, 1, 2),
(11, 'Bibliografia Básica 4', NULL, 1, 2),
(12, 'Bibliografia Básica 1', NULL, 1, 2),
(13, 'Bibliografia Básica 2', NULL, 1, 2),
(14, 'Bibliografia Básica 3', NULL, 1, 2),
(15, 'Bibliografia Básica 4', NULL, 1, 2),
(16, 'Bibliografia Básica 4', NULL, 1, 2),
(17, 'Bibliografia Básica 1', NULL, 1, 2),
(18, 'Bibliografia Básica 2', NULL, 1, 2),
(19, 'Bibliografia Básica 3', NULL, 1, 2),
(20, 'Bibliografia Básica 4', NULL, 1, 2),
(21, 'Bibliografia Complementar 1', NULL, 1, 2),
(22, 'Bibliografia Complementar 2', NULL, 1, 2),
(23, 'Bibliografia Complementar 3', NULL, 1, 2),
(24, 'Bibliografia Complementar 4', NULL, 1, 2),
(25, 'Bibliografia Complementar 5', NULL, 1, 2),
(26, 'Bibliografia Artigo 1', NULL, 1, 2),
(27, 'Bibliografia Artigo 2', '', 1, 2),
(28, 'Bibliografia Artigo 3', NULL, 1, 2),
(29, 'Bibliografia Artigo 4', NULL, 1, 2),
(30, 'Bibliografia Artigo 5', NULL, 1, 2),
(31, 'Bibliografia Básica 1', NULL, 1, 2),
(32, 'Bibliografia Básica 2', NULL, 1, 2),
(33, 'Bibliografia Básica 3', NULL, 1, 2),
(34, 'Bibliografia Básica 4', NULL, 1, 2),
(35, 'Bibliografia Complementar 1', NULL, 1, 2),
(36, 'Bibliografia Complementar 2', NULL, 1, 2),
(37, 'Bibliografia Complementar 3', NULL, 1, 2),
(38, 'Bibliografia Complementar 4', NULL, 1, 2),
(39, 'Bibliografia Complementar 5', NULL, 1, 2),
(40, 'Bibliografia Artigo 1', NULL, 1, 2),
(41, 'Bibliografia Artigo 2', NULL, 1, 2),
(42, 'Bibliografia Artigo 3', NULL, 1, 2),
(43, 'Bibliografia Artigo 4', NULL, 1, 2),
(44, 'Bibliografia Artigo 5', NULL, 1, 2),
(45, 'Bibliografia Básica 1', NULL, 1, 2),
(46, 'Bibliografia Básica 2', NULL, 1, 2),
(47, 'Bibliografia Básica 3', NULL, 1, 2),
(48, 'Bibliografia Básica 4', NULL, 1, 2),
(49, 'Bibliografia Complementar 1', NULL, 1, 2),
(50, 'Bibliografia Complementar 2', NULL, 1, 2),
(51, 'Bibliografia Complementar 3', NULL, 1, 2),
(52, 'Bibliografia Complementar 4', NULL, 1, 2),
(53, 'Bibliografia Complementar 5', NULL, 1, 2),
(54, 'Bibliografia Artigo 1', NULL, 1, 2),
(55, 'Bibliografia Artigo 2', NULL, 1, 2),
(56, 'Bibliografia Artigo 3', NULL, 1, 2),
(57, 'Bibliografia Artigo 4', NULL, 1, 2),
(58, 'Bibliografia Artigo 5', NULL, 1, 2),
(59, 'Bibliografia Básica 1', NULL, 1, 2),
(60, 'Bibliografia Básica 2', NULL, 1, 2),
(61, 'Bibliografia Básica 3', NULL, 1, 2),
(62, 'Bibliografia Básica 4', NULL, 1, 2),
(63, 'Bibliografia Complementar 1', NULL, 1, 2),
(64, 'Bibliografia Complementar 2', NULL, 1, 2),
(65, 'Bibliografia Complementar 3', NULL, 1, 2),
(66, 'Bibliografia Complementar 4', NULL, 1, 2),
(67, 'Bibliografia Complementar 5', NULL, 1, 2),
(68, 'Bibliografia Artigo 1', NULL, 1, 2),
(69, 'Bibliografia Artigo 2', NULL, 1, 2),
(70, 'Bibliografia Artigo 3', NULL, 1, 2),
(71, 'Bibliografia Artigo 4', NULL, 1, 2),
(72, 'Bibliografia Artigo 5', NULL, 1, 2);

-- --------------------------------------------------------

--
-- Estrutura para tabela `semana_atp1`
--

CREATE TABLE `semana_atp1` (
  `semana` int(11) NOT NULL,
  `atp` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


CREATE TABLE `semana_projeto` (
  `semana` int(11) NOT NULL,
  `projeto` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura para tabela `semana_atvps1`
--

CREATE TABLE `semana_atvps1` (
  `semana` int(11) NOT NULL,
  `atvps` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura para tabela `semana_bibliografia1`
--

CREATE TABLE `semana_bibliografia1` (
  `semana` int(11) NOT NULL,
  `bibliografia` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `semana_bibliografia1`
--

INSERT INTO `semana_bibliografia1` (`semana`, `bibliografia`) VALUES
(12, 5),
(13, 5),
(14, 5),
(15, 5),
(16, 5),
(17, 6),
(18, 6),
(19, 6),
(20, 6),
(31, 7),
(32, 7),
(33, 7),
(34, 7),
(45, 8),
(46, 8),
(47, 8),
(48, 8),
(59, 9),
(60, 9),
(61, 9),
(62, 9);

-- --------------------------------------------------------

--
-- Estrutura para tabela `semana_bibliografia2`
--

CREATE TABLE `semana_bibliografia2` (
  `semana` int(11) NOT NULL,
  `bibliografia` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `semana_bibliografia2`
--

INSERT INTO `semana_bibliografia2` (`semana`, `bibliografia`) VALUES
(21, 6),
(22, 6),
(23, 6),
(24, 6),
(25, 6),
(35, 7),
(36, 7),
(37, 7),
(38, 7),
(39, 7),
(49, 8),
(50, 8),
(51, 8),
(52, 8),
(53, 8),
(63, 9),
(64, 9),
(65, 9),
(66, 9),
(67, 9);

-- --------------------------------------------------------

--
-- Estrutura para tabela `semana_bibliografia3`
--

CREATE TABLE `semana_bibliografia3` (
  `semana` int(11) NOT NULL,
  `bibliografia` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `semana_bibliografia3`
--

INSERT INTO `semana_bibliografia3` (`semana`, `bibliografia`) VALUES
(26, 6),
(27, 6),
(28, 6),
(29, 6),
(30, 6),
(40, 7),
(41, 7),
(42, 7),
(43, 7),
(44, 7),
(54, 8),
(55, 8),
(56, 8),
(57, 8),
(58, 8),
(68, 9),
(69, 9),
(70, 9),
(71, 9),
(72, 9);

-- --------------------------------------------------------

--
-- Estrutura para tabela `semana_cp2`
--

CREATE TABLE `semana_cp2` (
  `semana` int(11) NOT NULL,
  `cp` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura para tabela `semana_nivelamento1`
--

CREATE TABLE `semana_nivelamento1` (
  `semana` int(11) NOT NULL,
  `nivelamento` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


CREATE TABLE `semana_projeto1` (
  `semana` int(11) NOT NULL,
  `projeto` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura para tabela `tempos`
--

CREATE TABLE `tempos` (
  `temposid` int(11) NOT NULL COMMENT 'Identificador',
  `temposdescricao` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'DescriÃ§Ã£o',
  `tempostempo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Tempo (1 Âª...)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura para tabela `tempos_turnos1`
--

CREATE TABLE `tempos_turnos1` (
  `tempos` int(11) NOT NULL,
  `turnos` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura para tabela `tempos_turnos2`
--

CREATE TABLE `tempos_turnos2` (
  `tempos` int(11) NOT NULL,
  `turnos` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura para tabela `tempos_turnos3`
--

CREATE TABLE `tempos_turnos3` (
  `tempos` int(11) NOT NULL,
  `turnos` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura para tabela `turma`
--

CREATE TABLE `turma` (
  `turmaid` int(11) NOT NULL COMMENT 'Identificador',
  `turmaturma` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Turma'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `turma`
--

INSERT INTO `turma` (`turmaid`, `turmaturma`) VALUES
(1, '2018/1');

-- --------------------------------------------------------

--
-- Estrutura para tabela `turnos`
--

CREATE TABLE `turnos` (
  `turnosid` int(11) NOT NULL COMMENT 'Identificador',
  `turnosdescricao` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'DescriÃ§Ã£o'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura para tabela `usuario`
--

CREATE TABLE `usuario` (
  `usuarioid` int(11) NOT NULL COMMENT 'Identificador',
  `usuarionome` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Nome',
  `usuarioemail` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'E-MAIL',
  `usuariosenha` text COLLATE utf8_unicode_ci,
  `usuariomatricula` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'matricula'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `usuario`
--

INSERT INTO `usuario` (`usuarioid`, `usuarionome`, `usuarioemail`, `usuariosenha`, `usuariomatricula`) VALUES
(1, 'Artur Romão Rocha', 'arturromaorocha@gmail.com', '223311', '29375');

-- --------------------------------------------------------

--
-- Estrutura para tabela `verbos`
--

CREATE TABLE `verbos` (
  `verbosid` int(11) NOT NULL COMMENT 'Identificador',
  `verbosverbo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Verbo'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `verbos`
--

INSERT INTO `verbos` (`verbosid`, `verbosverbo`) VALUES
(1, 'Verbo 1');

-- --------------------------------------------------------

--
-- Estrutura para tabela `verbos_dificuldade1`
--

CREATE TABLE `verbos_dificuldade1` (
  `verbos` int(11) NOT NULL,
  `dificuldade` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura para tabela `verbos_semana6`
--

CREATE TABLE `verbos_semana6` (
  `verbos` int(11) NOT NULL,
  `semana` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Índices de tabelas apagadas
--

--
-- Índices de tabela `atividade`
--
ALTER TABLE `atividade`
  ADD PRIMARY KEY (`atividadeid`);

--
-- Índices de tabela `atividade_conjatividade1`
--
ALTER TABLE `atividade_conjatividade1`
  ADD PRIMARY KEY (`atividade`,`conjatividade`),
  ADD KEY `fk_conjatividadeatividadeconjatividade1` (`conjatividade`);

--
-- Índices de tabela `atp`
--
ALTER TABLE `atp`
  ADD PRIMARY KEY (`atpid`);

--
-- Índices de tabela `atvps`
--
ALTER TABLE `atvps`
  ADD PRIMARY KEY (`atvpsid`,`atvpssemanaid7`),
  ADD KEY `fk_atvpssemana7` (`atvpssemanaid7`);

--
-- Índices de tabela `atvssasemana`
--
ALTER TABLE `atvssasemana`
  ADD PRIMARY KEY (`atvssasemanaid`);

--
-- Índices de tabela `bibliografia`
--
ALTER TABLE `bibliografia`
  ADD PRIMARY KEY (`bibliografiaid`);

--
-- Índices de tabela `bloco`
--
ALTER TABLE `bloco`
  ADD PRIMARY KEY (`blocoid`);

--
-- Índices de tabela `conjatividade`
--
ALTER TABLE `conjatividade`
  ADD PRIMARY KEY (`conjatividadeid`,`conjatividadeopsemanaid2`),
  ADD KEY `fk_conjatividadeopsemana2` (`conjatividadeopsemanaid2`);

--
-- Índices de tabela `conjatividade_atvssasemana1`
--
ALTER TABLE `conjatividade_atvssasemana1`
  ADD PRIMARY KEY (`conjatividade`,`atvssasemana`),
  ADD KEY `fk_atvssasemanaconjatividadeatvssasemana1` (`atvssasemana`);

--
-- Índices de tabela `cp`
--
ALTER TABLE `cp`
  ADD PRIMARY KEY (`cpid`);

--
-- Índices de tabela `cp_planodeensino8`
--
ALTER TABLE `cp_planodeensino8`
  ADD PRIMARY KEY (`cp`,`planodeensino`),
  ADD KEY `fk_planodeensinocpplanodeensino8` (`planodeensino`);

--
-- Índices de tabela `cronogramaav`
--
ALTER TABLE `cronogramaav`
  ADD PRIMARY KEY (`cronogramaavid`,`cronogramaavmetavaliativosid1`,`cronogramaavmetavaliativosid2`,`cronogramaavmetavaliativosid3`,`cronogramaavmetavaliativosid4`,`cronogramaavmetavaliativosid5`,`cronogramaavmetavaliativosid6`,`cronogramaavmetavaliativosid7`,`cronogramaavmetavaliativosid8`),
  ADD KEY `fk_cronogramaavmetavaliativos1` (`cronogramaavmetavaliativosid1`),
  ADD KEY `fk_cronogramaavmetavaliativos2` (`cronogramaavmetavaliativosid2`),
  ADD KEY `fk_cronogramaavmetavaliativos3` (`cronogramaavmetavaliativosid3`),
  ADD KEY `fk_cronogramaavmetavaliativos4` (`cronogramaavmetavaliativosid4`),
  ADD KEY `fk_cronogramaavmetavaliativos5` (`cronogramaavmetavaliativosid5`),
  ADD KEY `fk_cronogramaavmetavaliativos6` (`cronogramaavmetavaliativosid6`),
  ADD KEY `fk_cronogramaavmetavaliativos7` (`cronogramaavmetavaliativosid7`),
  ADD KEY `fk_cronogramaavmetavaliativos8` (`cronogramaavmetavaliativosid8`);

--
-- Índices de tabela `curso`
--
ALTER TABLE `curso`
  ADD PRIMARY KEY (`cursoid`);

--
-- Índices de tabela `dificuldade`
--
ALTER TABLE `dificuldade`
  ADD PRIMARY KEY (`dificuldadeid`);

--
-- Índices de tabela `dificuldade_semana5`
--
ALTER TABLE `dificuldade_semana5`
  ADD PRIMARY KEY (`dificuldade`,`semana`),
  ADD KEY `fk_semanadificuldadesemana5` (`semana`);

--
-- Índices de tabela `identificacao`
--
ALTER TABLE `identificacao`
  ADD PRIMARY KEY (`identificacaoid`,`identificacaousuarioid1`,`identificacaocursoid2`,`identificacaoturmaid3`),
  ADD KEY `fk_identificacaousuario1` (`identificacaousuarioid1`),
  ADD KEY `fk_identificacaocurso2` (`identificacaocursoid2`),
  ADD KEY `fk_identificacaoturma3` (`identificacaoturmaid3`);

--
-- Índices de tabela `itemeqt`
--
ALTER TABLE `itemeqt`
  ADD PRIMARY KEY (`itemeqtid`);

--
-- Índices de tabela `itemeqt_pap3`
--
ALTER TABLE `itemeqt_pap3`
  ADD PRIMARY KEY (`itemeqt`,`pap`),
  ADD KEY `fk_papitemeqtpap3` (`pap`);

--
-- Índices de tabela `laboratorios`
--
ALTER TABLE `laboratorios`
  ADD PRIMARY KEY (`laboratoriosid`);

--
-- Índices de tabela `metavaliativos`
--
ALTER TABLE `metavaliativos`
  ADD PRIMARY KEY (`metavaliativosid`);

--
-- Índices de tabela `nivelamento`
--
ALTER TABLE `nivelamento`
  ADD PRIMARY KEY (`nivelamentoid`);

--
-- Índices de tabela `opsemana`
--
ALTER TABLE `opsemana`
  ADD PRIMARY KEY (`opsemanaid`);

--
-- Índices de tabela `opsemana_semana2`
--
ALTER TABLE `opsemana_semana2`
  ADD PRIMARY KEY (`opsemana`,`semana`),
  ADD KEY `fk_semanaopsemanasemana2` (`semana`);

--
-- Índices de tabela `opsemana_semana4`
--
ALTER TABLE `opsemana_semana4`
  ADD PRIMARY KEY (`opsemana`,`semana`),
  ADD KEY `fk_semanaopsemanasemana4` (`semana`);

--
-- Índices de tabela `pap`
--
ALTER TABLE `pap`
  ADD PRIMARY KEY (`papid`,`papturnosid1`,`paplaboratoriosid2`),
  ADD KEY `fk_papturnos1` (`papturnosid1`),
  ADD KEY `fk_paplaboratorios2` (`paplaboratoriosid2`);

--
-- Índices de tabela `pap_planodeensino10`
--
ALTER TABLE `pap_planodeensino10`
  ADD PRIMARY KEY (`pap`,`planodeensino`),
  ADD KEY `fk_planodeensinopapplanodeensino10` (`planodeensino`);

--
-- Índices de tabela `planodeensino`
--
ALTER TABLE `planodeensino`
  ADD PRIMARY KEY (`planodeensinoid`);

--
-- Índices de tabela `projeto`
--
ALTER TABLE `projeto`
  ADD PRIMARY KEY (`projetoid`);

--
-- Índices de tabela `prova`
--
ALTER TABLE `prova`
  ADD PRIMARY KEY (`provaid`);

--
-- Índices de tabela `prova_cp1`
--
ALTER TABLE `prova_cp1`
  ADD PRIMARY KEY (`prova`,`cp`),
  ADD KEY `fk_cpprovacp1` (`cp`);

--
-- Índices de tabela `semana`
--
ALTER TABLE `semana`
  ADD PRIMARY KEY (`semanaid`,`semanablocoid1`,`semanablocoid3`),
  ADD KEY `fk_semanabloco1` (`semanablocoid1`),
  ADD KEY `fk_semanabloco3` (`semanablocoid3`);

--
-- Índices de tabela `semana_atp1`
--
ALTER TABLE `semana_atp1`
  ADD PRIMARY KEY (`semana`,`atp`),
  ADD KEY `fk_atpsemanaatp1` (`atp`);

--
-- Índices de tabela `semana_atvps1`
--
ALTER TABLE `semana_atvps1`
  ADD PRIMARY KEY (`semana`,`atvps`),
  ADD KEY `fk_atvpssemanaatvps1` (`atvps`);

--
-- Índices de tabela `semana_bibliografia1`
--
ALTER TABLE `semana_bibliografia1`
  ADD PRIMARY KEY (`semana`,`bibliografia`),
  ADD KEY `fk_bibliografiasemanabibliografia1` (`bibliografia`);

--
-- Índices de tabela `semana_bibliografia2`
--
ALTER TABLE `semana_bibliografia2`
  ADD PRIMARY KEY (`semana`,`bibliografia`),
  ADD KEY `fk_bibliografiasemanabibliografia2` (`bibliografia`);

--
-- Índices de tabela `semana_bibliografia3`
--
ALTER TABLE `semana_bibliografia3`
  ADD PRIMARY KEY (`semana`,`bibliografia`),
  ADD KEY `fk_bibliografiasemanabibliografia3` (`bibliografia`);

--
-- Índices de tabela `semana_cp2`
--
ALTER TABLE `semana_cp2`
  ADD PRIMARY KEY (`semana`,`cp`),
  ADD KEY `fk_cpsemanacp2` (`cp`);

--
-- Índices de tabela `semana_nivelamento1`
--
ALTER TABLE `semana_nivelamento1`
  ADD PRIMARY KEY (`semana`,`nivelamento`),
  ADD KEY `fk_nivelamentosemananivelamento1` (`nivelamento`);

--
--
-- Índices de tabela `semana_nivelamento1`
--
ALTER TABLE `semana_projeto1`
  ADD PRIMARY KEY (`semana`,`projeto`),
  ADD KEY `fk_npj1` (`projeto`);

--
-- Índices de tabela `tempos`
--
ALTER TABLE `tempos`
  ADD PRIMARY KEY (`temposid`);

--
-- Índices de tabela `tempos_turnos1`
--
ALTER TABLE `tempos_turnos1`
  ADD PRIMARY KEY (`tempos`,`turnos`),
  ADD KEY `fk_turnostemposturnos1` (`turnos`);

--
-- Índices de tabela `tempos_turnos2`
--
ALTER TABLE `tempos_turnos2`
  ADD PRIMARY KEY (`tempos`,`turnos`),
  ADD KEY `fk_turnostemposturnos2` (`turnos`);

--
-- Índices de tabela `tempos_turnos3`
--
ALTER TABLE `tempos_turnos3`
  ADD PRIMARY KEY (`tempos`,`turnos`),
  ADD KEY `fk_turnostemposturnos3` (`turnos`);

--
-- Índices de tabela `turma`
--
ALTER TABLE `turma`
  ADD PRIMARY KEY (`turmaid`);

--
-- Índices de tabela `turnos`
--
ALTER TABLE `turnos`
  ADD PRIMARY KEY (`turnosid`);

--
-- Índices de tabela `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`usuarioid`);

--
-- Índices de tabela `verbos`
--
ALTER TABLE `verbos`
  ADD PRIMARY KEY (`verbosid`);

--
-- Índices de tabela `verbos_dificuldade1`
--
ALTER TABLE `verbos_dificuldade1`
  ADD PRIMARY KEY (`verbos`,`dificuldade`),
  ADD KEY `fk_dificuldadeverbosdificuldade1` (`dificuldade`);

--
-- Índices de tabela `verbos_semana6`
--
ALTER TABLE `verbos_semana6`
  ADD PRIMARY KEY (`verbos`,`semana`),
  ADD KEY `fk_semanaverbossemana6` (`semana`);

--
-- AUTO_INCREMENT de tabelas apagadas
--

--
-- AUTO_INCREMENT de tabela `atividade`
--
ALTER TABLE `atividade`
  MODIFY `atividadeid` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Identificador';

--
-- AUTO_INCREMENT de tabela `atp`
--
ALTER TABLE `atp`
  MODIFY `atpid` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Identificador';

--
-- AUTO_INCREMENT de tabela `atvps`
--
ALTER TABLE `atvps`
  MODIFY `atvpsid` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Identificador';

--
-- AUTO_INCREMENT de tabela `atvssasemana`
--
ALTER TABLE `atvssasemana`
  MODIFY `atvssasemanaid` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Identificador';

--
-- AUTO_INCREMENT de tabela `bibliografia`
--
ALTER TABLE `bibliografia`
  MODIFY `bibliografiaid` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Identificador', AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT de tabela `bloco`
--
ALTER TABLE `bloco`
  MODIFY `blocoid` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Identificador', AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de tabela `conjatividade`
--
ALTER TABLE `conjatividade`
  MODIFY `conjatividadeid` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Identificador';

--
-- AUTO_INCREMENT de tabela `cp`
--
ALTER TABLE `cp`
  MODIFY `cpid` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Identificador';

--
-- AUTO_INCREMENT de tabela `cronogramaav`
--
ALTER TABLE `cronogramaav`
  MODIFY `cronogramaavid` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Identificador';

--
-- AUTO_INCREMENT de tabela `curso`
--
ALTER TABLE `curso`
  MODIFY `cursoid` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Identificador', AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de tabela `dificuldade`
--
ALTER TABLE `dificuldade`
  MODIFY `dificuldadeid` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Identificador', AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de tabela `identificacao`
--
ALTER TABLE `identificacao`
  MODIFY `identificacaoid` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Identificador', AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de tabela `itemeqt`
--
ALTER TABLE `itemeqt`
  MODIFY `itemeqtid` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Identificador';

--
-- AUTO_INCREMENT de tabela `laboratorios`
--
ALTER TABLE `laboratorios`
  MODIFY `laboratoriosid` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Identificador', AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de tabela `metavaliativos`
--
ALTER TABLE `metavaliativos`
  MODIFY `metavaliativosid` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Identificador', AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de tabela `nivelamento`
--
ALTER TABLE `nivelamento`
  MODIFY `nivelamentoid` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Identificador';

--
-- AUTO_INCREMENT de tabela `opsemana`
--
ALTER TABLE `opsemana`
  MODIFY `opsemanaid` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Identificador', AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de tabela `pap`
--
ALTER TABLE `pap`
  MODIFY `papid` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Identificador';

--
-- AUTO_INCREMENT de tabela `planodeensino`
--
ALTER TABLE `planodeensino`
  MODIFY `planodeensinoid` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Identificador', AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de tabela `projeto`
--
ALTER TABLE `projeto`
  MODIFY `projetoid` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Identificador';

--
-- AUTO_INCREMENT de tabela `prova`
--
ALTER TABLE `prova`
  MODIFY `provaid` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Identificador', AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de tabela `semana`
--
ALTER TABLE `semana`
  MODIFY `semanaid` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Identificador', AUTO_INCREMENT=73;

--
-- AUTO_INCREMENT de tabela `tempos`
--
ALTER TABLE `tempos`
  MODIFY `temposid` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Identificador';

--
-- AUTO_INCREMENT de tabela `turma`
--
ALTER TABLE `turma`
  MODIFY `turmaid` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Identificador', AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de tabela `turnos`
--
ALTER TABLE `turnos`
  MODIFY `turnosid` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Identificador';

--
-- AUTO_INCREMENT de tabela `usuario`
--
ALTER TABLE `usuario`
  MODIFY `usuarioid` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Identificador', AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de tabela `verbos`
--
ALTER TABLE `verbos`
  MODIFY `verbosid` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Identificador', AUTO_INCREMENT=2;

--
-- Restrições para dumps de tabelas
--

--
-- Restrições para tabelas `atividade_conjatividade1`
--
ALTER TABLE `atividade_conjatividade1`
  ADD CONSTRAINT `fk_conjatividadeatividadeatividade1` FOREIGN KEY (`atividade`) REFERENCES `atividade` (`atividadeid`),
  ADD CONSTRAINT `fk_conjatividadeatividadeconjatividade1` FOREIGN KEY (`conjatividade`) REFERENCES `conjatividade` (`conjatividadeid`);

--
-- Restrições para tabelas `atvps`
--
ALTER TABLE `atvps`
  ADD CONSTRAINT `fk_atvpssemana7` FOREIGN KEY (`atvpssemanaid7`) REFERENCES `semana` (`semanaid`);

--
-- Restrições para tabelas `conjatividade`
--
ALTER TABLE `conjatividade`
  ADD CONSTRAINT `fk_conjatividadeopsemana2` FOREIGN KEY (`conjatividadeopsemanaid2`) REFERENCES `opsemana` (`opsemanaid`);

--
-- Restrições para tabelas `conjatividade_atvssasemana1`
--
ALTER TABLE `conjatividade_atvssasemana1`
  ADD CONSTRAINT `fk_atvssasemanaconjatividadeatvssasemana1` FOREIGN KEY (`atvssasemana`) REFERENCES `atvssasemana` (`atvssasemanaid`),
  ADD CONSTRAINT `fk_atvssasemanaconjatividadeconjatividade1` FOREIGN KEY (`conjatividade`) REFERENCES `conjatividade` (`conjatividadeid`);

--
-- Restrições para tabelas `cp_planodeensino8`
--
ALTER TABLE `cp_planodeensino8`
  ADD CONSTRAINT `fk_planodeensinocpcp8` FOREIGN KEY (`cp`) REFERENCES `cp` (`cpid`),
  ADD CONSTRAINT `fk_planodeensinocpplanodeensino8` FOREIGN KEY (`planodeensino`) REFERENCES `planodeensino` (`planodeensinoid`);

--
-- Restrições para tabelas `cronogramaav`
--
ALTER TABLE `cronogramaav`
  ADD CONSTRAINT `fk_cronogramaavmetavaliativos1` FOREIGN KEY (`cronogramaavmetavaliativosid1`) REFERENCES `metavaliativos` (`metavaliativosid`),
  ADD CONSTRAINT `fk_cronogramaavmetavaliativos2` FOREIGN KEY (`cronogramaavmetavaliativosid2`) REFERENCES `metavaliativos` (`metavaliativosid`),
  ADD CONSTRAINT `fk_cronogramaavmetavaliativos3` FOREIGN KEY (`cronogramaavmetavaliativosid3`) REFERENCES `metavaliativos` (`metavaliativosid`),
  ADD CONSTRAINT `fk_cronogramaavmetavaliativos4` FOREIGN KEY (`cronogramaavmetavaliativosid4`) REFERENCES `metavaliativos` (`metavaliativosid`),
  ADD CONSTRAINT `fk_cronogramaavmetavaliativos5` FOREIGN KEY (`cronogramaavmetavaliativosid5`) REFERENCES `metavaliativos` (`metavaliativosid`),
  ADD CONSTRAINT `fk_cronogramaavmetavaliativos6` FOREIGN KEY (`cronogramaavmetavaliativosid6`) REFERENCES `metavaliativos` (`metavaliativosid`),
  ADD CONSTRAINT `fk_cronogramaavmetavaliativos7` FOREIGN KEY (`cronogramaavmetavaliativosid7`) REFERENCES `metavaliativos` (`metavaliativosid`),
  ADD CONSTRAINT `fk_cronogramaavmetavaliativos8` FOREIGN KEY (`cronogramaavmetavaliativosid8`) REFERENCES `metavaliativos` (`metavaliativosid`);

--
-- Restrições para tabelas `dificuldade_semana5`
--
ALTER TABLE `dificuldade_semana5`
  ADD CONSTRAINT `fk_semanadificuldadedificuldade5` FOREIGN KEY (`dificuldade`) REFERENCES `dificuldade` (`dificuldadeid`),
  ADD CONSTRAINT `fk_semanadificuldadesemana5` FOREIGN KEY (`semana`) REFERENCES `semana` (`semanaid`);

--
-- Restrições para tabelas `identificacao`
--
ALTER TABLE `identificacao`
  ADD CONSTRAINT `fk_identificacaocurso2` FOREIGN KEY (`identificacaocursoid2`) REFERENCES `curso` (`cursoid`),
  ADD CONSTRAINT `fk_identificacaoturma3` FOREIGN KEY (`identificacaoturmaid3`) REFERENCES `turma` (`turmaid`),
  ADD CONSTRAINT `fk_identificacaousuario1` FOREIGN KEY (`identificacaousuarioid1`) REFERENCES `usuario` (`usuarioid`);

--
-- Restrições para tabelas `itemeqt_pap3`
--
ALTER TABLE `itemeqt_pap3`
  ADD CONSTRAINT `fk_papitemeqtitemeqt3` FOREIGN KEY (`itemeqt`) REFERENCES `itemeqt` (`itemeqtid`),
  ADD CONSTRAINT `fk_papitemeqtpap3` FOREIGN KEY (`pap`) REFERENCES `pap` (`papid`);

--
-- Restrições para tabelas `nivelamento`
-- 
--
-- Restrições para tabelas `opsemana_semana2`
--
ALTER TABLE `opsemana_semana2`
  ADD CONSTRAINT `fk_semanaopsemanaopsemana2` FOREIGN KEY (`opsemana`) REFERENCES `opsemana` (`opsemanaid`),
  ADD CONSTRAINT `fk_semanaopsemanasemana2` FOREIGN KEY (`semana`) REFERENCES `semana` (`semanaid`);

--
-- Restrições para tabelas `opsemana_semana4`
--
ALTER TABLE `opsemana_semana4`
  ADD CONSTRAINT `fk_semanaopsemanaopsemana4` FOREIGN KEY (`opsemana`) REFERENCES `opsemana` (`opsemanaid`),
  ADD CONSTRAINT `fk_semanaopsemanasemana4` FOREIGN KEY (`semana`) REFERENCES `semana` (`semanaid`);

--
-- Restrições para tabelas `pap`
--
ALTER TABLE `pap`
  ADD CONSTRAINT `fk_paplaboratorios2` FOREIGN KEY (`paplaboratoriosid2`) REFERENCES `laboratorios` (`laboratoriosid`),
  ADD CONSTRAINT `fk_papturnos1` FOREIGN KEY (`papturnosid1`) REFERENCES `turnos` (`turnosid`);

--
-- Restrições para tabelas `pap_planodeensino10`
--
ALTER TABLE `pap_planodeensino10`
  ADD CONSTRAINT `fk_planodeensinopappap10` FOREIGN KEY (`pap`) REFERENCES `pap` (`papid`),
  ADD CONSTRAINT `fk_planodeensinopapplanodeensino10` FOREIGN KEY (`planodeensino`) REFERENCES `planodeensino` (`planodeensinoid`);

--
-- Restrições para tabelas `projeto`
-- 

--
-- Restrições para tabelas `prova_cp1`
--
ALTER TABLE `prova_cp1`
  ADD CONSTRAINT `fk_cpprovacp1` FOREIGN KEY (`cp`) REFERENCES `cp` (`cpid`),
  ADD CONSTRAINT `fk_cpprovaprova1` FOREIGN KEY (`prova`) REFERENCES `prova` (`provaid`);

--
-- Restrições para tabelas `semana`
--
ALTER TABLE `semana`
  ADD CONSTRAINT `fk_semanabloco1` FOREIGN KEY (`semanablocoid1`) REFERENCES `bloco` (`blocoid`),
  ADD CONSTRAINT `fk_semanabloco3` FOREIGN KEY (`semanablocoid3`) REFERENCES `bloco` (`blocoid`);

--
-- Restrições para tabelas `semana_atp1`
--
ALTER TABLE `semana_atp1`
  ADD CONSTRAINT `fk_atpsemanaatp1` FOREIGN KEY (`atp`) REFERENCES `atp` (`atpid`),
  ADD CONSTRAINT `fk_atpsemanasemana1` FOREIGN KEY (`semana`) REFERENCES `semana` (`semanaid`);

--
-- Restrições para tabelas `semana_atvps1`
--
ALTER TABLE `semana_atvps1`
  ADD CONSTRAINT `fk_atvpssemanaatvps1` FOREIGN KEY (`atvps`) REFERENCES `atvps` (`atvpsid`),
  ADD CONSTRAINT `fk_atvpssemanasemana1` FOREIGN KEY (`semana`) REFERENCES `semana` (`semanaid`);

--
-- Restrições para tabelas `semana_bibliografia1`
--
ALTER TABLE `semana_bibliografia1`
  ADD CONSTRAINT `fk_bibliografiasemanabibliografia1` FOREIGN KEY (`bibliografia`) REFERENCES `bibliografia` (`bibliografiaid`),
  ADD CONSTRAINT `fk_bibliografiasemanasemana1` FOREIGN KEY (`semana`) REFERENCES `semana` (`semanaid`);

--
-- Restrições para tabelas `semana_bibliografia2`
--
ALTER TABLE `semana_bibliografia2`
  ADD CONSTRAINT `fk_bibliografiasemanabibliografia2` FOREIGN KEY (`bibliografia`) REFERENCES `bibliografia` (`bibliografiaid`),
  ADD CONSTRAINT `fk_bibliografiasemanasemana2` FOREIGN KEY (`semana`) REFERENCES `semana` (`semanaid`);

--
-- Restrições para tabelas `semana_bibliografia3`
--
ALTER TABLE `semana_bibliografia3`
  ADD CONSTRAINT `fk_bibliografiasemanabibliografia3` FOREIGN KEY (`bibliografia`) REFERENCES `bibliografia` (`bibliografiaid`),
  ADD CONSTRAINT `fk_bibliografiasemanasemana3` FOREIGN KEY (`semana`) REFERENCES `semana` (`semanaid`);

--
-- Restrições para tabelas `semana_cp2`
--
ALTER TABLE `semana_cp2`
  ADD CONSTRAINT `fk_cpsemanacp2` FOREIGN KEY (`cp`) REFERENCES `cp` (`cpid`),
  ADD CONSTRAINT `fk_cpsemanasemana2` FOREIGN KEY (`semana`) REFERENCES `semana` (`semanaid`);

--
-- Restrições para tabelas `semana_nivelamento1`
--
ALTER TABLE `semana_nivelamento1`
  ADD CONSTRAINT `fk_nivelamentosemananivelamento1` FOREIGN KEY (`nivelamento`) REFERENCES `nivelamento` (`nivelamentoid`),
  ADD CONSTRAINT `fk_nivelamentosemanasemana1` FOREIGN KEY (`semana`) REFERENCES `semana` (`semanaid`);

ALTER TABLE `semana_projeto1`
  ADD CONSTRAINT `fk_nivelamentosemananivelamentewo1` FOREIGN KEY (`projeto`) REFERENCES `projeto` (`projetoid`),
  ADD CONSTRAINT `fk_nivelamentosemanasemawna1` FOREIGN KEY (`semana`) REFERENCES `semana` (`semanaid`);

--
-- Restrições para tabelas `tempos_turnos1`
--
ALTER TABLE `tempos_turnos1`
  ADD CONSTRAINT `fk_turnostempostempos1` FOREIGN KEY (`tempos`) REFERENCES `tempos` (`temposid`),
  ADD CONSTRAINT `fk_turnostemposturnos1` FOREIGN KEY (`turnos`) REFERENCES `turnos` (`turnosid`);

--
-- Restrições para tabelas `tempos_turnos2`
--
ALTER TABLE `tempos_turnos2`
  ADD CONSTRAINT `fk_turnostempostempos2` FOREIGN KEY (`tempos`) REFERENCES `tempos` (`temposid`),
  ADD CONSTRAINT `fk_turnostemposturnos2` FOREIGN KEY (`turnos`) REFERENCES `turnos` (`turnosid`);

--
-- Restrições para tabelas `tempos_turnos3`
--
ALTER TABLE `tempos_turnos3`
  ADD CONSTRAINT `fk_turnostempostempos3` FOREIGN KEY (`tempos`) REFERENCES `tempos` (`temposid`),
  ADD CONSTRAINT `fk_turnostemposturnos3` FOREIGN KEY (`turnos`) REFERENCES `turnos` (`turnosid`);

--
-- Restrições para tabelas `verbos_dificuldade1`
--
ALTER TABLE `verbos_dificuldade1`
  ADD CONSTRAINT `fk_dificuldadeverbosdificuldade1` FOREIGN KEY (`dificuldade`) REFERENCES `dificuldade` (`dificuldadeid`),
  ADD CONSTRAINT `fk_dificuldadeverbosverbos1` FOREIGN KEY (`verbos`) REFERENCES `verbos` (`verbosid`);

--
-- Restrições para tabelas `verbos_semana6`
--
ALTER TABLE `verbos_semana6`
  ADD CONSTRAINT `fk_semanaverbossemana6` FOREIGN KEY (`semana`) REFERENCES `semana` (`semanaid`),
  ADD CONSTRAINT `fk_semanaverbosverbos6` FOREIGN KEY (`verbos`) REFERENCES `verbos` (`verbosid`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
