<?php
include_once 'Sample_Header.php';

// New Word document
echo date('H:i:s'), ' Create new PhpWord object', EOL;
$phpWord = new \PhpOffice\PhpWord\PhpWord();

// New portrait section
$section = $phpWord->addSection();

// Add first page header
$header = $section->addHeader();
$header->firstPage();
$table = $header->addTable();
$table->addRow();
$cell = $table->addCell(4500);
$textrun = $cell->addTextRun();
$textrun->addText(' ASASD');
$textrun->addLink('https://github.com/PHPOffice/PHPWord', 'PHPWord on GitHub');
$table->addCell(4500)->addImage('resources/PhpWord.png', array('width' => 80, 'height' => 80, 'alignment' => \PhpOffice\PhpWord\SimpleType\Jc::END));

// Add header for all other pages
$subsequent = $section->addHeader();
$subsequent->addText("<table class='MsoTableGrid' border='1' cellspacing='0' cellpadding='0' align='left' width='671' style='text-align: justify; width: 503.2pt; border: none; margin-left: 4.8pt; margin-right: 4.8pt;'>
 <tbody><tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes;height:15.95pt'>
  <td width='104' valign='top' style='width:77.75pt;border:solid windowtext 1.0pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt;height:15.95pt'>
  <p class='MsoNormal' align='center' style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal;mso-element:frame;mso-element-frame-hspace:
  7.05pt;mso-element-wrap:around;mso-element-anchor-horizontal:page;mso-element-left:
  42.8pt;mso-element-top:24.0pt;mso-height-rule:exactly'>DATA<o:p></o:p></p>
  </td>
  <td width='312' valign='top' style='width:233.85pt;border:solid windowtext 1.0pt;
  border-left:none;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt;height:15.95pt'>
  <p class='MsoNormal' align='center' style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal;mso-element:frame;mso-element-frame-hspace:
  7.05pt;mso-element-wrap:around;mso-element-anchor-horizontal:page;mso-element-left:
  42.8pt;mso-element-top:24.0pt;mso-height-rule:exactly'>NOME<o:p></o:p></p>
  </td>
  <td width='85' valign='top' style='width:63.8pt;border:solid windowtext 1.0pt;
  border-left:none;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt;height:15.95pt'>
  <p class='MsoNormal' align='center' style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal;mso-element:frame;mso-element-frame-hspace:
  7.05pt;mso-element-wrap:around;mso-element-anchor-horizontal:page;mso-element-left:
  42.8pt;mso-element-top:24.0pt;mso-height-rule:exactly'>MATRICULA<o:p></o:p></p>
  </td>
  <td width='85' valign='top' style='width:63.8pt;border:solid windowtext 1.0pt;
  border-left:none;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt;height:15.95pt'>
  <p class='MsoNormal' align='center' style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal;mso-element:frame;mso-element-frame-hspace:
  7.05pt;mso-element-wrap:around;mso-element-anchor-horizontal:page;mso-element-left:
  42.8pt;mso-element-top:24.0pt;mso-height-rule:exactly'>ENTRADA<o:p></o:p></p>
  </td>
  <td width='85' valign='top' style='width:64.0pt;border:solid windowtext 1.0pt;
  border-left:none;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt;height:15.95pt'>
  <p class='MsoNormal' align='center' style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal;mso-element:frame;mso-element-frame-hspace:
  7.05pt;mso-element-wrap:around;mso-element-anchor-horizontal:page;mso-element-left:
  42.8pt;mso-element-top:24.0pt;mso-height-rule:exactly'>SAIDA<o:p></o:p></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:1;height:15.05pt'>
  <td width='104' valign='top' style='width:77.75pt;border:solid windowtext 1.0pt;
  border-top:none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:15.05pt'>
  <p class='MsoNormal' style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;mso-element:frame;mso-element-frame-hspace:7.05pt;mso-element-wrap:
  around;mso-element-anchor-horizontal:page;mso-element-left:42.8pt;mso-element-top:
  24.0pt;mso-height-rule:exactly'><o:p>&nbsp;</o:p></p>
  </td>
  <td width='312' valign='top' style='width:233.85pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt;height:15.05pt'>
  <p class='MsoNormal' style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;mso-element:frame;mso-element-frame-hspace:7.05pt;mso-element-wrap:
  around;mso-element-anchor-horizontal:page;mso-element-left:42.8pt;mso-element-top:
  24.0pt;mso-height-rule:exactly'><o:p>&nbsp;</o:p></p>
  </td>
  <td width='85' valign='top' style='width:63.8pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt;height:15.05pt'>
  <p class='MsoNormal' style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;mso-element:frame;mso-element-frame-hspace:7.05pt;mso-element-wrap:
  around;mso-element-anchor-horizontal:page;mso-element-left:42.8pt;mso-element-top:
  24.0pt;mso-height-rule:exactly'><o:p>&nbsp;</o:p></p>
  </td>
  <td width='85' valign='top' style='width:63.8pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt;height:15.05pt'>
  <p class='MsoNormal' style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;mso-element:frame;mso-element-frame-hspace:7.05pt;mso-element-wrap:
  around;mso-element-anchor-horizontal:page;mso-element-left:42.8pt;mso-element-top:
  24.0pt;mso-height-rule:exactly'><o:p>&nbsp;</o:p></p>
  </td>
  <td width='85' valign='top' style='width:64.0pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt;height:15.05pt'>
  <p class='MsoNormal' style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;mso-element:frame;mso-element-frame-hspace:7.05pt;mso-element-wrap:
  around;mso-element-anchor-horizontal:page;mso-element-left:42.8pt;mso-element-top:
  24.0pt;mso-height-rule:exactly'><o:p>&nbsp;</o:p></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:2;height:15.95pt'>
  <td width='104' valign='top' style='width:77.75pt;border:solid windowtext 1.0pt;
  border-top:none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:15.95pt'>
  <p class='MsoNormal' style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;mso-element:frame;mso-element-frame-hspace:7.05pt;mso-element-wrap:
  around;mso-element-anchor-horizontal:page;mso-element-left:42.8pt;mso-element-top:
  24.0pt;mso-height-rule:exactly'><o:p>&nbsp;</o:p></p>
  </td>
  <td width='312' valign='top' style='width:233.85pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt;height:15.95pt'>
  <p class='MsoNormal' style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;mso-element:frame;mso-element-frame-hspace:7.05pt;mso-element-wrap:
  around;mso-element-anchor-horizontal:page;mso-element-left:42.8pt;mso-element-top:
  24.0pt;mso-height-rule:exactly'><o:p>&nbsp;</o:p></p>
  </td>
  <td width='85' valign='top' style='width:63.8pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt;height:15.95pt'>
  <p class='MsoNormal' style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;mso-element:frame;mso-element-frame-hspace:7.05pt;mso-element-wrap:
  around;mso-element-anchor-horizontal:page;mso-element-left:42.8pt;mso-element-top:
  24.0pt;mso-height-rule:exactly'><o:p>&nbsp;</o:p></p>
  </td>
  <td width='85' valign='top' style='width:63.8pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt;height:15.95pt'>
  <p class='MsoNormal' style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;mso-element:frame;mso-element-frame-hspace:7.05pt;mso-element-wrap:
  around;mso-element-anchor-horizontal:page;mso-element-left:42.8pt;mso-element-top:
  24.0pt;mso-height-rule:exactly'><o:p>&nbsp;</o:p></p>
  </td>
  <td width='85' valign='top' style='width:64.0pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt;height:15.95pt'>
  <p class='MsoNormal' style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;mso-element:frame;mso-element-frame-hspace:7.05pt;mso-element-wrap:
  around;mso-element-anchor-horizontal:page;mso-element-left:42.8pt;mso-element-top:
  24.0pt;mso-height-rule:exactly'><o:p>&nbsp;</o:p></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:3;height:15.05pt'>
  <td width='104' valign='top' style='width:77.75pt;border:solid windowtext 1.0pt;
  border-top:none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:15.05pt'>
  <p class='MsoNormal' style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;mso-element:frame;mso-element-frame-hspace:7.05pt;mso-element-wrap:
  around;mso-element-anchor-horizontal:page;mso-element-left:42.8pt;mso-element-top:
  24.0pt;mso-height-rule:exactly'><o:p>&nbsp;</o:p></p>
  </td>
  <td width='312' valign='top' style='width:233.85pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt;height:15.05pt'>
  <p class='MsoNormal' style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;mso-element:frame;mso-element-frame-hspace:7.05pt;mso-element-wrap:
  around;mso-element-anchor-horizontal:page;mso-element-left:42.8pt;mso-element-top:
  24.0pt;mso-height-rule:exactly'><o:p>&nbsp;</o:p></p>
  </td>
  <td width='85' valign='top' style='width:63.8pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt;height:15.05pt'>
  <p class='MsoNormal' style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;mso-element:frame;mso-element-frame-hspace:7.05pt;mso-element-wrap:
  around;mso-element-anchor-horizontal:page;mso-element-left:42.8pt;mso-element-top:
  24.0pt;mso-height-rule:exactly'><o:p>&nbsp;</o:p></p>
  </td>
  <td width='85' valign='top' style='width:63.8pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt;height:15.05pt'>
  <p class='MsoNormal' style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;mso-element:frame;mso-element-frame-hspace:7.05pt;mso-element-wrap:
  around;mso-element-anchor-horizontal:page;mso-element-left:42.8pt;mso-element-top:
  24.0pt;mso-height-rule:exactly'><o:p>&nbsp;</o:p></p>
  </td>
  <td width='85' valign='top' style='width:64.0pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt;height:15.05pt'>
  <p class='MsoNormal' style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;mso-element:frame;mso-element-frame-hspace:7.05pt;mso-element-wrap:
  around;mso-element-anchor-horizontal:page;mso-element-left:42.8pt;mso-element-top:
  24.0pt;mso-height-rule:exactly'><o:p>&nbsp;</o:p></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:4;height:15.95pt'>
  <td width='104' valign='top' style='width:77.75pt;border:solid windowtext 1.0pt;
  border-top:none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:15.95pt'>
  <p class='MsoNormal' style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;mso-element:frame;mso-element-frame-hspace:7.05pt;mso-element-wrap:
  around;mso-element-anchor-horizontal:page;mso-element-left:42.8pt;mso-element-top:
  24.0pt;mso-height-rule:exactly'><o:p>&nbsp;</o:p></p>
  </td>
  <td width='312' valign='top' style='width:233.85pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt;height:15.95pt'>
  <p class='MsoNormal' style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;mso-element:frame;mso-element-frame-hspace:7.05pt;mso-element-wrap:
  around;mso-element-anchor-horizontal:page;mso-element-left:42.8pt;mso-element-top:
  24.0pt;mso-height-rule:exactly'><o:p>&nbsp;</o:p></p>
  </td>
  <td width='85' valign='top' style='width:63.8pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt;height:15.95pt'>
  <p class='MsoNormal' style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;mso-element:frame;mso-element-frame-hspace:7.05pt;mso-element-wrap:
  around;mso-element-anchor-horizontal:page;mso-element-left:42.8pt;mso-element-top:
  24.0pt;mso-height-rule:exactly'><o:p>&nbsp;</o:p></p>
  </td>
  <td width='85' valign='top' style='width:63.8pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt;height:15.95pt'>
  <p class='MsoNormal' style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;mso-element:frame;mso-element-frame-hspace:7.05pt;mso-element-wrap:
  around;mso-element-anchor-horizontal:page;mso-element-left:42.8pt;mso-element-top:
  24.0pt;mso-height-rule:exactly'><o:p>&nbsp;</o:p></p>
  </td>
  <td width='85' valign='top' style='width:64.0pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt;height:15.95pt'>
  <p class='MsoNormal' style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;mso-element:frame;mso-element-frame-hspace:7.05pt;mso-element-wrap:
  around;mso-element-anchor-horizontal:page;mso-element-left:42.8pt;mso-element-top:
  24.0pt;mso-height-rule:exactly'><o:p>&nbsp;</o:p></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:5;height:15.05pt'>
  <td width='104' valign='top' style='width:77.75pt;border:solid windowtext 1.0pt;
  border-top:none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:15.05pt'>
  <p class='MsoNormal' style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;mso-element:frame;mso-element-frame-hspace:7.05pt;mso-element-wrap:
  around;mso-element-anchor-horizontal:page;mso-element-left:42.8pt;mso-element-top:
  24.0pt;mso-height-rule:exactly'><o:p>&nbsp;</o:p></p>
  </td>
  <td width='312' valign='top' style='width:233.85pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt;height:15.05pt'>
  <p class='MsoNormal' style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;mso-element:frame;mso-element-frame-hspace:7.05pt;mso-element-wrap:
  around;mso-element-anchor-horizontal:page;mso-element-left:42.8pt;mso-element-top:
  24.0pt;mso-height-rule:exactly'><o:p>&nbsp;</o:p></p>
  </td>
  <td width='85' valign='top' style='width:63.8pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt;height:15.05pt'>
  <p class='MsoNormal' style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;mso-element:frame;mso-element-frame-hspace:7.05pt;mso-element-wrap:
  around;mso-element-anchor-horizontal:page;mso-element-left:42.8pt;mso-element-top:
  24.0pt;mso-height-rule:exactly'><o:p>&nbsp;</o:p></p>
  </td>
  <td width='85' valign='top' style='width:63.8pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt;height:15.05pt'>
  <p class='MsoNormal' style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;mso-element:frame;mso-element-frame-hspace:7.05pt;mso-element-wrap:
  around;mso-element-anchor-horizontal:page;mso-element-left:42.8pt;mso-element-top:
  24.0pt;mso-height-rule:exactly'><o:p>&nbsp;</o:p></p>
  </td>
  <td width='85' valign='top' style='width:64.0pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt;height:15.05pt'>
  <p class='MsoNormal' style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;mso-element:frame;mso-element-frame-hspace:7.05pt;mso-element-wrap:
  around;mso-element-anchor-horizontal:page;mso-element-left:42.8pt;mso-element-top:
  24.0pt;mso-height-rule:exactly'><o:p>&nbsp;</o:p></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:6;height:15.95pt'>
  <td width='104' valign='top' style='width:77.75pt;border:solid windowtext 1.0pt;
  border-top:none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:15.95pt'>
  <p class='MsoNormal' style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;mso-element:frame;mso-element-frame-hspace:7.05pt;mso-element-wrap:
  around;mso-element-anchor-horizontal:page;mso-element-left:42.8pt;mso-element-top:
  24.0pt;mso-height-rule:exactly'><o:p>&nbsp;</o:p></p>
  </td>
  <td width='312' valign='top' style='width:233.85pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt;height:15.95pt'>
  <p class='MsoNormal' style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;mso-element:frame;mso-element-frame-hspace:7.05pt;mso-element-wrap:
  around;mso-element-anchor-horizontal:page;mso-element-left:42.8pt;mso-element-top:
  24.0pt;mso-height-rule:exactly'><o:p>&nbsp;</o:p></p>
  </td>
  <td width='85' valign='top' style='width:63.8pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt;height:15.95pt'>
  <p class='MsoNormal' style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;mso-element:frame;mso-element-frame-hspace:7.05pt;mso-element-wrap:
  around;mso-element-anchor-horizontal:page;mso-element-left:42.8pt;mso-element-top:
  24.0pt;mso-height-rule:exactly'><o:p>&nbsp;</o:p></p>
  </td>
  <td width='85' valign='top' style='width:63.8pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt;height:15.95pt'>
  <p class='MsoNormal' style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;mso-element:frame;mso-element-frame-hspace:7.05pt;mso-element-wrap:
  around;mso-element-anchor-horizontal:page;mso-element-left:42.8pt;mso-element-top:
  24.0pt;mso-height-rule:exactly'><o:p>&nbsp;</o:p></p>
  </td>
  <td width='85' valign='top' style='width:64.0pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt;height:15.95pt'>
  <p class='MsoNormal' style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;mso-element:frame;mso-element-frame-hspace:7.05pt;mso-element-wrap:
  around;mso-element-anchor-horizontal:page;mso-element-left:42.8pt;mso-element-top:
  24.0pt;mso-height-rule:exactly'><o:p>&nbsp;</o:p></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:7;height:15.05pt'>
  <td width='104' valign='top' style='width:77.75pt;border:solid windowtext 1.0pt;
  border-top:none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:15.05pt'>
  <p class='MsoNormal' style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;mso-element:frame;mso-element-frame-hspace:7.05pt;mso-element-wrap:
  around;mso-element-anchor-horizontal:page;mso-element-left:42.8pt;mso-element-top:
  24.0pt;mso-height-rule:exactly'><o:p>&nbsp;</o:p></p>
  </td>
  <td width='312' valign='top' style='width:233.85pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt;height:15.05pt'>
  <p class='MsoNormal' style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;mso-element:frame;mso-element-frame-hspace:7.05pt;mso-element-wrap:
  around;mso-element-anchor-horizontal:page;mso-element-left:42.8pt;mso-element-top:
  24.0pt;mso-height-rule:exactly'><o:p>&nbsp;</o:p></p>
  </td>
  <td width='85' valign='top' style='width:63.8pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt;height:15.05pt'>
  <p class='MsoNormal' style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;mso-element:frame;mso-element-frame-hspace:7.05pt;mso-element-wrap:
  around;mso-element-anchor-horizontal:page;mso-element-left:42.8pt;mso-element-top:
  24.0pt;mso-height-rule:exactly'><o:p>&nbsp;</o:p></p>
  </td>
  <td width='85' valign='top' style='width:63.8pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt;height:15.05pt'>
  <p class='MsoNormal' style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;mso-element:frame;mso-element-frame-hspace:7.05pt;mso-element-wrap:
  around;mso-element-anchor-horizontal:page;mso-element-left:42.8pt;mso-element-top:
  24.0pt;mso-height-rule:exactly'><o:p>&nbsp;</o:p></p>
  </td>
  <td width='85' valign='top' style='width:64.0pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt;height:15.05pt'>
  <p class='MsoNormal' style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;mso-element:frame;mso-element-frame-hspace:7.05pt;mso-element-wrap:
  around;mso-element-anchor-horizontal:page;mso-element-left:42.8pt;mso-element-top:
  24.0pt;mso-height-rule:exactly'><o:p>&nbsp;</o:p></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:8;height:15.05pt'>
  <td width='104' valign='top' style='width:77.75pt;border:solid windowtext 1.0pt;
  border-top:none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:15.05pt'>
  <p class='MsoNormal' style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;mso-element:frame;mso-element-frame-hspace:7.05pt;mso-element-wrap:
  around;mso-element-anchor-horizontal:page;mso-element-left:42.8pt;mso-element-top:
  24.0pt;mso-height-rule:exactly'><o:p>&nbsp;</o:p></p>
  </td>
  <td width='312' valign='top' style='width:233.85pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt;height:15.05pt'>
  <p class='MsoNormal' style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;mso-element:frame;mso-element-frame-hspace:7.05pt;mso-element-wrap:
  around;mso-element-anchor-horizontal:page;mso-element-left:42.8pt;mso-element-top:
  24.0pt;mso-height-rule:exactly'><o:p>&nbsp;</o:p></p>
  </td>
  <td width='85' valign='top' style='width:63.8pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt;height:15.05pt'>
  <p class='MsoNormal' style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;mso-element:frame;mso-element-frame-hspace:7.05pt;mso-element-wrap:
  around;mso-element-anchor-horizontal:page;mso-element-left:42.8pt;mso-element-top:
  24.0pt;mso-height-rule:exactly'><o:p>&nbsp;</o:p></p>
  </td>
  <td width='85' valign='top' style='width:63.8pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt;height:15.05pt'>
  <p class='MsoNormal' style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;mso-element:frame;mso-element-frame-hspace:7.05pt;mso-element-wrap:
  around;mso-element-anchor-horizontal:page;mso-element-left:42.8pt;mso-element-top:
  24.0pt;mso-height-rule:exactly'><o:p>&nbsp;</o:p></p>
  </td>
  <td width='85' valign='top' style='width:64.0pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt;height:15.05pt'>
  <p class='MsoNormal' style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;mso-element:frame;mso-element-frame-hspace:7.05pt;mso-element-wrap:
  around;mso-element-anchor-horizontal:page;mso-element-left:42.8pt;mso-element-top:
  24.0pt;mso-height-rule:exactly'><o:p>&nbsp;</o:p></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:9;height:15.05pt'>
  <td width='104' valign='top' style='width:77.75pt;border:solid windowtext 1.0pt;
  border-top:none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:15.05pt'>
  <p class='MsoNormal' style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;mso-element:frame;mso-element-frame-hspace:7.05pt;mso-element-wrap:
  around;mso-element-anchor-horizontal:page;mso-element-left:42.8pt;mso-element-top:
  24.0pt;mso-height-rule:exactly'><o:p>&nbsp;</o:p></p>
  </td>
  <td width='312' valign='top' style='width:233.85pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt;height:15.05pt'>
  <p class='MsoNormal' style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;mso-element:frame;mso-element-frame-hspace:7.05pt;mso-element-wrap:
  around;mso-element-anchor-horizontal:page;mso-element-left:42.8pt;mso-element-top:
  24.0pt;mso-height-rule:exactly'><o:p>&nbsp;</o:p></p>
  </td>
  <td width='85' valign='top' style='width:63.8pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt;height:15.05pt'>
  <p class='MsoNormal' style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;mso-element:frame;mso-element-frame-hspace:7.05pt;mso-element-wrap:
  around;mso-element-anchor-horizontal:page;mso-element-left:42.8pt;mso-element-top:
  24.0pt;mso-height-rule:exactly'><o:p>&nbsp;</o:p></p>
  </td>
  <td width='85' valign='top' style='width:63.8pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt;height:15.05pt'>
  <p class='MsoNormal' style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;mso-element:frame;mso-element-frame-hspace:7.05pt;mso-element-wrap:
  around;mso-element-anchor-horizontal:page;mso-element-left:42.8pt;mso-element-top:
  24.0pt;mso-height-rule:exactly'><o:p>&nbsp;</o:p></p>
  </td>
  <td width='85' valign='top' style='width:64.0pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt;height:15.05pt'>
  <p class='MsoNormal' style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;mso-element:frame;mso-element-frame-hspace:7.05pt;mso-element-wrap:
  around;mso-element-anchor-horizontal:page;mso-element-left:42.8pt;mso-element-top:
  24.0pt;mso-height-rule:exactly'><o:p>&nbsp;</o:p></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:10;height:15.05pt'>
  <td width='104' valign='top' style='width:77.75pt;border:solid windowtext 1.0pt;
  border-top:none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:15.05pt'>
  <p class='MsoNormal' style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;mso-element:frame;mso-element-frame-hspace:7.05pt;mso-element-wrap:
  around;mso-element-anchor-horizontal:page;mso-element-left:42.8pt;mso-element-top:
  24.0pt;mso-height-rule:exactly'><o:p>&nbsp;</o:p></p>
  </td>
  <td width='312' valign='top' style='width:233.85pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt;height:15.05pt'>
  <p class='MsoNormal' style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;mso-element:frame;mso-element-frame-hspace:7.05pt;mso-element-wrap:
  around;mso-element-anchor-horizontal:page;mso-element-left:42.8pt;mso-element-top:
  24.0pt;mso-height-rule:exactly'><o:p>&nbsp;</o:p></p>
  </td>
  <td width='85' valign='top' style='width:63.8pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt;height:15.05pt'>
  <p class='MsoNormal' style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;mso-element:frame;mso-element-frame-hspace:7.05pt;mso-element-wrap:
  around;mso-element-anchor-horizontal:page;mso-element-left:42.8pt;mso-element-top:
  24.0pt;mso-height-rule:exactly'><o:p>&nbsp;</o:p></p>
  </td>
  <td width='85' valign='top' style='width:63.8pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt;height:15.05pt'>
  <p class='MsoNormal' style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;mso-element:frame;mso-element-frame-hspace:7.05pt;mso-element-wrap:
  around;mso-element-anchor-horizontal:page;mso-element-left:42.8pt;mso-element-top:
  24.0pt;mso-height-rule:exactly'><o:p>&nbsp;</o:p></p>
  </td>
  <td width='85' valign='top' style='width:64.0pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt;height:15.05pt'>
  <p class='MsoNormal' style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;mso-element:frame;mso-element-frame-hspace:7.05pt;mso-element-wrap:
  around;mso-element-anchor-horizontal:page;mso-element-left:42.8pt;mso-element-top:
  24.0pt;mso-height-rule:exactly'><o:p>&nbsp;</o:p></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:11;height:15.05pt'>
  <td width='104' valign='top' style='width:77.75pt;border:solid windowtext 1.0pt;
  border-top:none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:15.05pt'>
  <p class='MsoNormal' style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;mso-element:frame;mso-element-frame-hspace:7.05pt;mso-element-wrap:
  around;mso-element-anchor-horizontal:page;mso-element-left:42.8pt;mso-element-top:
  24.0pt;mso-height-rule:exactly'><o:p>&nbsp;</o:p></p>
  </td>
  <td width='312' valign='top' style='width:233.85pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt;height:15.05pt'>
  <p class='MsoNormal' style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;mso-element:frame;mso-element-frame-hspace:7.05pt;mso-element-wrap:
  around;mso-element-anchor-horizontal:page;mso-element-left:42.8pt;mso-element-top:
  24.0pt;mso-height-rule:exactly'><o:p>&nbsp;</o:p></p>
  </td>
  <td width='85' valign='top' style='width:63.8pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt;height:15.05pt'>
  <p class='MsoNormal' style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;mso-element:frame;mso-element-frame-hspace:7.05pt;mso-element-wrap:
  around;mso-element-anchor-horizontal:page;mso-element-left:42.8pt;mso-element-top:
  24.0pt;mso-height-rule:exactly'><o:p>&nbsp;</o:p></p>
  </td>
  <td width='85' valign='top' style='width:63.8pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt;height:15.05pt'>
  <p class='MsoNormal' style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;mso-element:frame;mso-element-frame-hspace:7.05pt;mso-element-wrap:
  around;mso-element-anchor-horizontal:page;mso-element-left:42.8pt;mso-element-top:
  24.0pt;mso-height-rule:exactly'><o:p>&nbsp;</o:p></p>
  </td>
  <td width='85' valign='top' style='width:64.0pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt;height:15.05pt'>
  <p class='MsoNormal' style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;mso-element:frame;mso-element-frame-hspace:7.05pt;mso-element-wrap:
  around;mso-element-anchor-horizontal:page;mso-element-left:42.8pt;mso-element-top:
  24.0pt;mso-height-rule:exactly'><o:p>&nbsp;</o:p></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:12;height:15.05pt'>
  <td width='104' valign='top' style='width:77.75pt;border:solid windowtext 1.0pt;
  border-top:none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:15.05pt'>
  <p class='MsoNormal' style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;mso-element:frame;mso-element-frame-hspace:7.05pt;mso-element-wrap:
  around;mso-element-anchor-horizontal:page;mso-element-left:42.8pt;mso-element-top:
  24.0pt;mso-height-rule:exactly'><o:p>&nbsp;</o:p></p>
  </td>
  <td width='312' valign='top' style='width:233.85pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt;height:15.05pt'>
  <p class='MsoNormal' style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;mso-element:frame;mso-element-frame-hspace:7.05pt;mso-element-wrap:
  around;mso-element-anchor-horizontal:page;mso-element-left:42.8pt;mso-element-top:
  24.0pt;mso-height-rule:exactly'><o:p>&nbsp;</o:p></p>
  </td>
  <td width='85' valign='top' style='width:63.8pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt;height:15.05pt'>
  <p class='MsoNormal' style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;mso-element:frame;mso-element-frame-hspace:7.05pt;mso-element-wrap:
  around;mso-element-anchor-horizontal:page;mso-element-left:42.8pt;mso-element-top:
  24.0pt;mso-height-rule:exactly'><o:p>&nbsp;</o:p></p>
  </td>
  <td width='85' valign='top' style='width:63.8pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt;height:15.05pt'>
  <p class='MsoNormal' style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;mso-element:frame;mso-element-frame-hspace:7.05pt;mso-element-wrap:
  around;mso-element-anchor-horizontal:page;mso-element-left:42.8pt;mso-element-top:
  24.0pt;mso-height-rule:exactly'><o:p>&nbsp;</o:p></p>
  </td>
  <td width='85' valign='top' style='width:64.0pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt;height:15.05pt'>
  <p class='MsoNormal' style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;mso-element:frame;mso-element-frame-hspace:7.05pt;mso-element-wrap:
  around;mso-element-anchor-horizontal:page;mso-element-left:42.8pt;mso-element-top:
  24.0pt;mso-height-rule:exactly'><o:p>&nbsp;</o:p></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:13;height:15.05pt'>
  <td width='104' valign='top' style='width:77.75pt;border:solid windowtext 1.0pt;
  border-top:none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:15.05pt'>
  <p class='MsoNormal' style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;mso-element:frame;mso-element-frame-hspace:7.05pt;mso-element-wrap:
  around;mso-element-anchor-horizontal:page;mso-element-left:42.8pt;mso-element-top:
  24.0pt;mso-height-rule:exactly'><o:p>&nbsp;</o:p></p>
  </td>
  <td width='312' valign='top' style='width:233.85pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt;height:15.05pt'>
  <p class='MsoNormal' style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;mso-element:frame;mso-element-frame-hspace:7.05pt;mso-element-wrap:
  around;mso-element-anchor-horizontal:page;mso-element-left:42.8pt;mso-element-top:
  24.0pt;mso-height-rule:exactly'><o:p>&nbsp;</o:p></p>
  </td>
  <td width='85' valign='top' style='width:63.8pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt;height:15.05pt'>
  <p class='MsoNormal' style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;mso-element:frame;mso-element-frame-hspace:7.05pt;mso-element-wrap:
  around;mso-element-anchor-horizontal:page;mso-element-left:42.8pt;mso-element-top:
  24.0pt;mso-height-rule:exactly'><o:p>&nbsp;</o:p></p>
  </td>
  <td width='85' valign='top' style='width:63.8pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt;height:15.05pt'>
  <p class='MsoNormal' style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;mso-element:frame;mso-element-frame-hspace:7.05pt;mso-element-wrap:
  around;mso-element-anchor-horizontal:page;mso-element-left:42.8pt;mso-element-top:
  24.0pt;mso-height-rule:exactly'><o:p>&nbsp;</o:p></p>
  </td>
  <td width='85' valign='top' style='width:64.0pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt;height:15.05pt'>
  <p class='MsoNormal' style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;mso-element:frame;mso-element-frame-hspace:7.05pt;mso-element-wrap:
  around;mso-element-anchor-horizontal:page;mso-element-left:42.8pt;mso-element-top:
  24.0pt;mso-height-rule:exactly'><o:p>&nbsp;</o:p></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:14;height:15.05pt'>
  <td width='104' valign='top' style='width:77.75pt;border:solid windowtext 1.0pt;
  border-top:none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:15.05pt'>
  <p class='MsoNormal' style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;mso-element:frame;mso-element-frame-hspace:7.05pt;mso-element-wrap:
  around;mso-element-anchor-horizontal:page;mso-element-left:42.8pt;mso-element-top:
  24.0pt;mso-height-rule:exactly'><o:p>&nbsp;</o:p></p>
  </td>
  <td width='312' valign='top' style='width:233.85pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt;height:15.05pt'>
  <p class='MsoNormal' style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;mso-element:frame;mso-element-frame-hspace:7.05pt;mso-element-wrap:
  around;mso-element-anchor-horizontal:page;mso-element-left:42.8pt;mso-element-top:
  24.0pt;mso-height-rule:exactly'><o:p>&nbsp;</o:p></p>
  </td>
  <td width='85' valign='top' style='width:63.8pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt;height:15.05pt'>
  <p class='MsoNormal' style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;mso-element:frame;mso-element-frame-hspace:7.05pt;mso-element-wrap:
  around;mso-element-anchor-horizontal:page;mso-element-left:42.8pt;mso-element-top:
  24.0pt;mso-height-rule:exactly'><o:p>&nbsp;</o:p></p>
  </td>
  <td width='85' valign='top' style='width:63.8pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt;height:15.05pt'>
  <p class='MsoNormal' style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;mso-element:frame;mso-element-frame-hspace:7.05pt;mso-element-wrap:
  around;mso-element-anchor-horizontal:page;mso-element-left:42.8pt;mso-element-top:
  24.0pt;mso-height-rule:exactly'><o:p>&nbsp;</o:p></p>
  </td>
  <td width='85' valign='top' style='width:64.0pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt;height:15.05pt'>
  <p class='MsoNormal' style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;mso-element:frame;mso-element-frame-hspace:7.05pt;mso-element-wrap:
  around;mso-element-anchor-horizontal:page;mso-element-left:42.8pt;mso-element-top:
  24.0pt;mso-height-rule:exactly'><o:p>&nbsp;</o:p></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:15;height:15.05pt'>
  <td width='104' valign='top' style='width:77.75pt;border:solid windowtext 1.0pt;
  border-top:none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:15.05pt'>
  <p class='MsoNormal' style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;mso-element:frame;mso-element-frame-hspace:7.05pt;mso-element-wrap:
  around;mso-element-anchor-horizontal:page;mso-element-left:42.8pt;mso-element-top:
  24.0pt;mso-height-rule:exactly'><o:p>&nbsp;</o:p></p>
  </td>
  <td width='312' valign='top' style='width:233.85pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt;height:15.05pt'>
  <p class='MsoNormal' style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;mso-element:frame;mso-element-frame-hspace:7.05pt;mso-element-wrap:
  around;mso-element-anchor-horizontal:page;mso-element-left:42.8pt;mso-element-top:
  24.0pt;mso-height-rule:exactly'><o:p>&nbsp;</o:p></p>
  </td>
  <td width='85' valign='top' style='width:63.8pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt;height:15.05pt'>
  <p class='MsoNormal' style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;mso-element:frame;mso-element-frame-hspace:7.05pt;mso-element-wrap:
  around;mso-element-anchor-horizontal:page;mso-element-left:42.8pt;mso-element-top:
  24.0pt;mso-height-rule:exactly'><o:p>&nbsp;</o:p></p>
  </td>
  <td width='85' valign='top' style='width:63.8pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt;height:15.05pt'>
  <p class='MsoNormal' style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;mso-element:frame;mso-element-frame-hspace:7.05pt;mso-element-wrap:
  around;mso-element-anchor-horizontal:page;mso-element-left:42.8pt;mso-element-top:
  24.0pt;mso-height-rule:exactly'><o:p>&nbsp;</o:p></p>
  </td>
  <td width='85' valign='top' style='width:64.0pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt;height:15.05pt'>
  <p class='MsoNormal' style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;mso-element:frame;mso-element-frame-hspace:7.05pt;mso-element-wrap:
  around;mso-element-anchor-horizontal:page;mso-element-left:42.8pt;mso-element-top:
  24.0pt;mso-height-rule:exactly'><o:p>&nbsp;</o:p></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:16;height:15.05pt'>
  <td width='104' valign='top' style='width:77.75pt;border:solid windowtext 1.0pt;
  border-top:none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:15.05pt'>
  <p class='MsoNormal' style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;mso-element:frame;mso-element-frame-hspace:7.05pt;mso-element-wrap:
  around;mso-element-anchor-horizontal:page;mso-element-left:42.8pt;mso-element-top:
  24.0pt;mso-height-rule:exactly'><o:p>&nbsp;</o:p></p>
  </td>
  <td width='312' valign='top' style='width:233.85pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt;height:15.05pt'>
  <p class='MsoNormal' style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;mso-element:frame;mso-element-frame-hspace:7.05pt;mso-element-wrap:
  around;mso-element-anchor-horizontal:page;mso-element-left:42.8pt;mso-element-top:
  24.0pt;mso-height-rule:exactly'><o:p>&nbsp;</o:p></p>
  </td>
  <td width='85' valign='top' style='width:63.8pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt;height:15.05pt'>
  <p class='MsoNormal' style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;mso-element:frame;mso-element-frame-hspace:7.05pt;mso-element-wrap:
  around;mso-element-anchor-horizontal:page;mso-element-left:42.8pt;mso-element-top:
  24.0pt;mso-height-rule:exactly'><o:p>&nbsp;</o:p></p>
  </td>
  <td width='85' valign='top' style='width:63.8pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt;height:15.05pt'>
  <p class='MsoNormal' style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;mso-element:frame;mso-element-frame-hspace:7.05pt;mso-element-wrap:
  around;mso-element-anchor-horizontal:page;mso-element-left:42.8pt;mso-element-top:
  24.0pt;mso-height-rule:exactly'><o:p>&nbsp;</o:p></p>
  </td>
  <td width='85' valign='top' style='width:64.0pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt;height:15.05pt'>
  <p class='MsoNormal' style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;mso-element:frame;mso-element-frame-hspace:7.05pt;mso-element-wrap:
  around;mso-element-anchor-horizontal:page;mso-element-left:42.8pt;mso-element-top:
  24.0pt;mso-height-rule:exactly'><o:p>&nbsp;</o:p></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:17;height:15.05pt'>
  <td width='104' valign='top' style='width:77.75pt;border:solid windowtext 1.0pt;
  border-top:none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:15.05pt'>
  <p class='MsoNormal' style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;mso-element:frame;mso-element-frame-hspace:7.05pt;mso-element-wrap:
  around;mso-element-anchor-horizontal:page;mso-element-left:42.8pt;mso-element-top:
  24.0pt;mso-height-rule:exactly'><o:p>&nbsp;</o:p></p>
  </td>
  <td width='312' valign='top' style='width:233.85pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt;height:15.05pt'>
  <p class='MsoNormal' style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;mso-element:frame;mso-element-frame-hspace:7.05pt;mso-element-wrap:
  around;mso-element-anchor-horizontal:page;mso-element-left:42.8pt;mso-element-top:
  24.0pt;mso-height-rule:exactly'><o:p>&nbsp;</o:p></p>
  </td>
  <td width='85' valign='top' style='width:63.8pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt;height:15.05pt'>
  <p class='MsoNormal' style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;mso-element:frame;mso-element-frame-hspace:7.05pt;mso-element-wrap:
  around;mso-element-anchor-horizontal:page;mso-element-left:42.8pt;mso-element-top:
  24.0pt;mso-height-rule:exactly'><o:p>&nbsp;</o:p></p>
  </td>
  <td width='85' valign='top' style='width:63.8pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt;height:15.05pt'>
  <p class='MsoNormal' style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;mso-element:frame;mso-element-frame-hspace:7.05pt;mso-element-wrap:
  around;mso-element-anchor-horizontal:page;mso-element-left:42.8pt;mso-element-top:
  24.0pt;mso-height-rule:exactly'><o:p>&nbsp;</o:p></p>
  </td>
  <td width='85' valign='top' style='width:64.0pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt;height:15.05pt'>
  <p class='MsoNormal' style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;mso-element:frame;mso-element-frame-hspace:7.05pt;mso-element-wrap:
  around;mso-element-anchor-horizontal:page;mso-element-left:42.8pt;mso-element-top:
  24.0pt;mso-height-rule:exactly'><o:p>&nbsp;</o:p></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:18;height:15.05pt'>
  <td width='104' valign='top' style='width:77.75pt;border:solid windowtext 1.0pt;
  border-top:none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:15.05pt'>
  <p class='MsoNormal' style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;mso-element:frame;mso-element-frame-hspace:7.05pt;mso-element-wrap:
  around;mso-element-anchor-horizontal:page;mso-element-left:42.8pt;mso-element-top:
  24.0pt;mso-height-rule:exactly'><o:p>&nbsp;</o:p></p>
  </td>
  <td width='312' valign='top' style='width:233.85pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt;height:15.05pt'>
  <p class='MsoNormal' style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;mso-element:frame;mso-element-frame-hspace:7.05pt;mso-element-wrap:
  around;mso-element-anchor-horizontal:page;mso-element-left:42.8pt;mso-element-top:
  24.0pt;mso-height-rule:exactly'><o:p>&nbsp;</o:p></p>
  </td>
  <td width='85' valign='top' style='width:63.8pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt;height:15.05pt'>
  <p class='MsoNormal' style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;mso-element:frame;mso-element-frame-hspace:7.05pt;mso-element-wrap:
  around;mso-element-anchor-horizontal:page;mso-element-left:42.8pt;mso-element-top:
  24.0pt;mso-height-rule:exactly'><o:p>&nbsp;</o:p></p>
  </td>
  <td width='85' valign='top' style='width:63.8pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt;height:15.05pt'>
  <p class='MsoNormal' style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;mso-element:frame;mso-element-frame-hspace:7.05pt;mso-element-wrap:
  around;mso-element-anchor-horizontal:page;mso-element-left:42.8pt;mso-element-top:
  24.0pt;mso-height-rule:exactly'><o:p>&nbsp;</o:p></p>
  </td>
  <td width='85' valign='top' style='width:64.0pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt;height:15.05pt'>
  <p class='MsoNormal' style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;mso-element:frame;mso-element-frame-hspace:7.05pt;mso-element-wrap:
  around;mso-element-anchor-horizontal:page;mso-element-left:42.8pt;mso-element-top:
  24.0pt;mso-height-rule:exactly'><o:p>&nbsp;</o:p></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:19;height:15.05pt'>
  <td width='104' valign='top' style='width:77.75pt;border:solid windowtext 1.0pt;
  border-top:none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:15.05pt'>
  <p class='MsoNormal' style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;mso-element:frame;mso-element-frame-hspace:7.05pt;mso-element-wrap:
  around;mso-element-anchor-horizontal:page;mso-element-left:42.8pt;mso-element-top:
  24.0pt;mso-height-rule:exactly'><o:p>&nbsp;</o:p></p>
  </td>
  <td width='312' valign='top' style='width:233.85pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt;height:15.05pt'>
  <p class='MsoNormal' style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;mso-element:frame;mso-element-frame-hspace:7.05pt;mso-element-wrap:
  around;mso-element-anchor-horizontal:page;mso-element-left:42.8pt;mso-element-top:
  24.0pt;mso-height-rule:exactly'><o:p>&nbsp;</o:p></p>
  </td>
  <td width='85' valign='top' style='width:63.8pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt;height:15.05pt'>
  <p class='MsoNormal' style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;mso-element:frame;mso-element-frame-hspace:7.05pt;mso-element-wrap:
  around;mso-element-anchor-horizontal:page;mso-element-left:42.8pt;mso-element-top:
  24.0pt;mso-height-rule:exactly'><o:p>&nbsp;</o:p></p>
  </td>
  <td width='85' valign='top' style='width:63.8pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt;height:15.05pt'>
  <p class='MsoNormal' style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;mso-element:frame;mso-element-frame-hspace:7.05pt;mso-element-wrap:
  around;mso-element-anchor-horizontal:page;mso-element-left:42.8pt;mso-element-top:
  24.0pt;mso-height-rule:exactly'><o:p>&nbsp;</o:p></p>
  </td>
  <td width='85' valign='top' style='width:64.0pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt;height:15.05pt'>
  <p class='MsoNormal' style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;mso-element:frame;mso-element-frame-hspace:7.05pt;mso-element-wrap:
  around;mso-element-anchor-horizontal:page;mso-element-left:42.8pt;mso-element-top:
  24.0pt;mso-height-rule:exactly'><o:p>&nbsp;</o:p></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:20;height:15.05pt'>
  <td width='104' valign='top' style='width:77.75pt;border:solid windowtext 1.0pt;
  border-top:none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:15.05pt'>
  <p class='MsoNormal' style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;mso-element:frame;mso-element-frame-hspace:7.05pt;mso-element-wrap:
  around;mso-element-anchor-horizontal:page;mso-element-left:42.8pt;mso-element-top:
  24.0pt;mso-height-rule:exactly'><o:p>&nbsp;</o:p></p>
  </td>
  <td width='312' valign='top' style='width:233.85pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt;height:15.05pt'>
  <p class='MsoNormal' style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;mso-element:frame;mso-element-frame-hspace:7.05pt;mso-element-wrap:
  around;mso-element-anchor-horizontal:page;mso-element-left:42.8pt;mso-element-top:
  24.0pt;mso-height-rule:exactly'><o:p>&nbsp;</o:p></p>
  </td>
  <td width='85' valign='top' style='width:63.8pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt;height:15.05pt'>
  <p class='MsoNormal' style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;mso-element:frame;mso-element-frame-hspace:7.05pt;mso-element-wrap:
  around;mso-element-anchor-horizontal:page;mso-element-left:42.8pt;mso-element-top:
  24.0pt;mso-height-rule:exactly'><o:p>&nbsp;</o:p></p>
  </td>
  <td width='85' valign='top' style='width:63.8pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt;height:15.05pt'>
  <p class='MsoNormal' style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;mso-element:frame;mso-element-frame-hspace:7.05pt;mso-element-wrap:
  around;mso-element-anchor-horizontal:page;mso-element-left:42.8pt;mso-element-top:
  24.0pt;mso-height-rule:exactly'><o:p>&nbsp;</o:p></p>
  </td>
  <td width='85' valign='top' style='width:64.0pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso");
$subsequent->addImage('resources/_mars.jpg', array('width' => 80, 'height' => 80));

// Add footer
$footer = $section->addFooter();
$footer->addPreserveText('Page {PAGE} of {NUMPAGES}.', null, array('alignment' => \PhpOffice\PhpWord\SimpleType\Jc::CENTER));
$footer->addLink('https://github.com/PHPOffice/PHPWord', 'PHPWord on GitHub');

// Write some text
$section->addTextBreak();
$section->addText('Some text...');

// Create a second page
$section->addPageBreak();

// Write some text
$section->addTextBreak();
$section->addText('Some text...');

// Create a third page
$section->addPageBreak();

// Write some text
$section->addTextBreak();
$section->addText('Some text...');

// New portrait section
$section2 = $phpWord->addSection();

$sec2Header = $section2->addHeader();
$sec2Header->addText('All pages in Section 2 will Have this!');

// Write some text
$section2->addTextBreak();
$section2->addText('Some text...');

// Save file
echo write($phpWord, basename(__FILE__, '.php'), $writers);
if (!CLI) {
    include_once 'Sample_Footer.php';
}
