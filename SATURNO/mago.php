<?php
if (isset($_POST['f'])) {
    switch ($_POST['f']) {
        case 'listar':
            listar("apps/");
            break;
        case 'adicionar':
            adicionar($_POST['pasta']);
            break;
        case 'campos':
            campos($_POST['pasta']);
            break;
    }
}

function listar($path) {
    $diretorio = dir($path);
    echo "Lista de Arquivos do diretório '<strong>" . $path . "</strong>':<br />";
    while ($arquivo = $diretorio->read()) {
        echo "<a href='" . $path . $arquivo . "'>" . $arquivo . "</a><br />";
    }
    $diretorio->close();
}

function adicionar($pasta, $path = "apps/") {
    mkdir($path . $pasta, 777);
    chmod($path, 777);
    criarmapa($path . $pasta);
    listar($path . $pasta . '/');
}

function criarmapa($path) {
    $campos = [];
    for ($i = 0; $i < 10; $i++) {
        $atributos = ["tipo" => "text", "legenda" => "LEGENDA ...  " . rand(), "tamanho" => "4 4 8 12"];
        $campo = ['nome' => "campo $i", "atributos" => $atributos];
        array_push($campos, $campo);
    }
    $classe = ['classe' => "modelo", "campos" => $campos];
    $filename = $path . '/mapa.txt';
    $fp = fopen($filename, "w");
    fputs($fp, json_encode($classe));
    fclose($fp);
}

function campos($path) {
    $path .= '/mapa.txt';
    $arquivo = fopen($path, 'r');
    $file = file_get_contents($path);
    $d = json_decode($file, true);
   // print_r($d['campos']);
    // fputs($arquivo,"  ".$linha);
    foreach ($d['campos'] as $campo) {
        campotxt($campo);
    }
    fclose($arquivo);
}

function campotxt($campo) {
    ?> 
    <div class="col-<?php echo rand(2,4);?>">
        <div class="form-group has-success">
            <label class="form-control-label" for="inputSuccess1">Valid input</label>
            <input type="text" value="correct value" class="form-control is-valid" id="inputValid">
            <div class="valid-feedback">Success! You've done it.</div>
        </div>
    </div>
    <?php
}
