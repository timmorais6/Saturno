<html> 
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">


        <script src="bootswatch-master/js/jquery.js" type="text/javascript"></script>
        <script src="bootswatch-master/js/popper.js" type="text/javascript"></script>
        <script src="bootswatch-master/js/bootstrap.js" type="text/javascript"></script>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.0/jquery-ui.js"></script> 
        <script src="https://cdnjs.cloudflare.com/ajax/libs/lodash.js/4.17.0/lodash.min.js"></script>
        <script src="js/gridstack.all.js" type="text/javascript"></script>
        <script src="js/gridstack.jQueryUI.js" type="text/javascript"></script>




        <link id="sl" href="bootswatch-master/dist/yeti/bootstrap.css" rel="stylesheet" type="text/css"/>  
        <link href="../assets/css/ionicons.css" rel="stylesheet" />

        <style>
            .hover:hover{
                background: #e0e0e0;
            }
        </style>       
    </head> 
    <body> 
        https://johnny.github.io/jquery-sortable/
        https://johnny.github.io/jquery-sortable/
        https://www.jqueryscript.net/form/Drag-Drop-Form-Builder-Bootstrap-4.html


        <nav class="navbar  navbar-expand-lg navbar-dark bg-dark">
            <a class="navbar-brand col-2 col-md-2 mr-0 " href="#">Saturno</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor01" aria-controls="navbarColor01" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarColor01">

                <ul class="navbar-nav mr-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href="#">Salvar</a>
                    </li>
                    <li class="nav-item  nav-fill">  
                        <div class="btn-group" role="group">
                            <a href="#" class="dropdown-toggle nav-link" data-toggle="dropdown" role="button" aria-expanded="true">Restaurar<span class="caret"></span></a>
                            <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                                <a class="dropdown-item" href="#">Dropdown link</a> 
                            </div>
                        </div>
                    </li> 

                </ul>
                <form class="form-inline my-2 my-lg-0">
                    <input class="form-control mr-sm-2" type="text" placeholder="Search">
                    <button class="btn btn-secondary my-2 my-sm-0" type="submit">Buscar</button>
                </form>
            </div>
        </nav>


        <div class="container-fluid">
            <div class="row" >
                <nav class="col-2 bg-light sidebar">
                    <div class="sidebar-sticky">
                        <div class="nav flex-column"> 

                            <div class="form-group nav-item">
                                <h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">
                                    <span>Aplicação</span> 
                                </h6>
                                <div class="form-group">
                                    <div class="input-group mb-3"> 
                                        <input id="nome"  title="Nome do projeto" type="text" class="form-control" placeholder="Nome do projeto" aria-label="Amount (to the nearest dollar)">
                                        <div class="input-group-append"  title="Nome do projeto" data-container="body" data-toggle="popover" data-placement="right" data-content="Nome da sua apliação, lembrando que também será o prefixo da base de dados" data-original-title="Nome do projeto">
                                            <span class="input-group-text badge-warning">?</span> 
                                        </div>
                                    </div>
                                </div> 

                                <script>
                                    function tem($tema) {
                                        $("#sl").attr('href', 'bootswatch-master/dist/' + $tema + '/bootstrap.css');
                                    }
                                </script>
                                <div class="form-group">
                                    <div class="input-group mb-3">   
                                        <select data-toggle="tooltip" data-placement="rigth" data-original-title="Selecione o tema que deseja para sua aplicação."  onchange="tem($(this).val())" id="ss" value="yeti" name="tema" class="form-control">

                                            <?php
                                            $path = "bootswatch-master/dist";
                                            $diretorio = dir($path);
                                            $vai = 0;
                                            while ($arquivo = $diretorio->read()) {
                                                $vai++;
                                                if ($vai > 2) {
                                                    echo '<option value="' . $arquivo . '">' . $arquivo . "</option>";
                                                }
                                            }
                                            $diretorio->close();
                                            ?>

                                        </select>
                                        <div class="input-group-append"  title="Tema" data-container="body" data-toggle="popover" data-placement="right" data-content="Paleta principal de cores para a aplicação." data-original-title="Tema">
                                            <span class="input-group-text badge-warning">?</span> 
                                        </div>
                                    </div>
                                </div> 

                                <h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">
                                    <span>Editar</span> 
                                </h6>
                                <div class="form-group">
                                    <div class="input-group mb-3"> 
                                        <input onclick="adicionar()"  title="Novo módulo" type="button" class="btn form-control btn-info" value="Adicionar" >
                                        <div class="input-group-append"  title="Nome do projeto" data-container="body" data-toggle="popover" data-placement="right" data-content="Nome da sua apliação, lembrando que também será o prefixo da base de dados" data-original-title="Nome do projeto">
                                            <span class="input-group-text badge-warning">?</span> 
                                        </div>
                                    </div>
                                    <div class="input-group mb-3"> 
                                        <input onclick="listar()"  title="Novo módulo" type="button" class="btn form-control btn-info" value="Listar" >
                                        <div class="input-group-append"  title="Nome do projeto" data-container="body" data-toggle="popover" data-placement="right" data-content="Nome da sua apliação, lembrando que também será o prefixo da base de dados" data-original-title="Nome do projeto">
                                            <span class="input-group-text badge-warning">?</span> 
                                        </div>
                                    </div>
                                    <div class="input-group mb-3"> 
                                        <input onclick="campos()"  title="Novo módulo" type="button" class="btn form-control btn-info" value="Campos" >
                                        <div class="input-group-append"  title="Nome do projeto" data-container="body" data-toggle="popover" data-placement="right" data-content="Nome da sua apliação, lembrando que também será o prefixo da base de dados" data-original-title="Nome do projeto">
                                            <span class="input-group-text badge-warning">?</span> 
                                        </div>
                                    </div>
                                </div> 


                            </div>

                        </div> 
                    </div>
                </nav>



                <script>

                    function campos() {
                        $nome = $('#nome').val();
                        $.post("mago.php", {f: "campos", pasta: $nome}, function (e) {
                            $('.visao').html(e);
                        });
                    }
                    function adicionar() {
                        $nome = $('#nome').val();
                        $.post("mago.php", {f: "adicionar", pasta: $nome}, function (e) {
                            $('.visao').html(e);
                        });
                    }
                    function listar() {

                        $.post("mago.php", {f: "listar"}, function (e) {
                            $('.visao').html(e);
                        });
                    }

                </script>
                <link href="drag/assets/css/custom.css" rel="stylesheet" type="text/css"/>
                <div class="col-7">
                    <div class="container  ">
                        <div class="item visao row">

                            <div class="span4"> 
                                <ul class="simple_with_drop vertical">
                                    <?php for ($i = 1; $i < 10; $i++) { ?>
                                        <li> 
                                            <i class="icon-move">Mover</i>
                                         Item <?php echo $i;?>
                                        </li>
                                        <?php
                                    }
                                    ?>
                                </ul>  



                            </div>
                            <div class="span4"> 
                                <ol class="simple_with_no_drop vertical">

                                    <?php for ($i = 1; $i < 10; $i++) { ?>
                                        <li class="bg-dark"> 
                                            <i class="icon-move">Mover</i>
                                    Item <?php echo $i;?>
                                        </li>
                                        <?php
                                    }
                                    ?>
                                </ol> 
                                <ol class="simple_with_no_drag vertical">

                                    <?php for ($i = 1; $i < 10; $i++) { ?>
                                        <li> 
                                            <i class="icon-move">Mover</i>
                                            Item <?php echo $i;?>
                                        </li>
                                        <?php
                                    }
                                    ?>
                                </ol>
                            </div>


                        </div> 
                    </div> 
                </div>   




                <nav class="col-3 bg-light sidebar msl-auto">
                    <div class="sidebar-sticky">
                        <div class="nav flex-column"> 

                            <div class="form-group nav-item">
                                <?php
                                include './tipodecampo.php';
                                ?>
                            </div>
                        </div>
                    </div>
                </nav>

            </div>

        </div>
        <script> 
            $("ol.simple_with_drop").sortable({
                group: 'no-drop',
                handle: 'i.icon-move',
                onDragStart: function ($item, container, _super) {
                    // Duplicate items of the no drop area
                    if (!container.options.drop)
                        $item.clone().insertAfter($item);
                    _super($item, container);
                }
            });
            $("ol.simple_with_no_drop").sortable({
                group: 'no-drop',
                drop: false
            });
            $("ol.simple_with_no_drag").sortable({
                group: 'no-drop' 
            });
        </script>

    </body>  
    <script>
        setInterval(function () {

            $('input').change(function () {
                $(this).attr('value', $(this).val());
            });

            $('select').change(function () {
                $(this).attr('value', $(this).val());
            });
        }
        , 100);

        function tog() {
            var btns = $("input, select, ul");
            var $btns = $(btns);

//you can use $.each to assign at each element, or simply apply by the array

            $.each(btns, function (i, btn) {
                //$(btn).tooltip({});
            });

            $('[data-toggle="popover"]').popover();

//2 event handlers
            $alert = $($(".alert")[0]);
            var tooltips = 0;

//destroying tooltips cause the event handler not to work anymore
            $btns.on('show.bs.tooltip hidden.bs.tooltip', function (e) {

                if (e.type == "show") {
                    tooltips = Math.min(tooltips + 1, btns.length);
                } else {
                    tooltips = Math.max(0, tooltips - 1);
                }

                $alert.text("showing tooltips: " + tooltips);
            });


            $('.noesp').keypress(function (event) {

                var regex = new RegExp("[a-zA-Z\b]");
                var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
                if (!regex.test(key))
                {
                    event.preventDefault();
                    return false;
                }
            });


           // 
        }
        setInterval(tog(), 500);




    </script>


</html> 