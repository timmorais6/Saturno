<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of model
 *
 * @author Artur Romao Rocha
 */
class model {

    //put your code here
    //put your code here
    function __construct() {
        
    }

    public function criar($qtd, $pj) {




        for ($i = 1; $i <= $qtd; $i++) {
            if (isset($_POST['x' . $i])) {
                $classe = @$_POST['x' . $i];
                $dir = 'bin/' . $pj . '/model/' . $classe;
                $file = $dir . '/m' . $classe . '.php';
                $limt = $_POST['qtd' . $i];
                $posts = '';
                $aux1 = 0;
                $insert = '';
                $cinsert = '';
                $objs = '""';
                $busc='';
                $xid = 1;
                $update = '';
 
                for ($j = 1; $j <= $limt; $j++) {
                    if (isset($_POST['c' . $i . '_n' . $j])) {
                       
                        $nome = $classe . $_POST['c' . $i . '_n' . $j];

                        if ($aux1 == 0) {
                            $aux1 = 1;
                        } else {
                            $posts.=', $_POST["' . $nome . '"]';
                        }

                        $objs.=', $this->rs[$i]["' . $nome . '"]';
                    }


                    if (isset($_POST['c' . $i . '_s' . $j])) {


                        if ($_POST['c' . $i . '_s' . $j] == 1 || $_POST['c' . $i . '_s' . $j] == 2 || $_POST['c' . $i . '_s' . $j] == 5 || $_POST['c' . $i . '_s' . $j] == 6 || $_POST['c' . $i . '_s' . $j] == 7) {




                            if (@$update == '') {
                                $update.="`" . $nome . '`=#$%s#$';
                                @$insert.="#$%s#$";

                                @$cinsert.="`" . $nome . '`';
                                
                                $busc = "`" . $nome . '`';
                                $buscX = $nome;
                            } else {
                                $update.=",`" . $nome . '`=#$%s#$';
                                $insert.=",#$%s#$";
                                $cinsert.=",`" . $nome . '`';
                            }
                        }



                        if ($_POST['c' . $i . '_s' . $j] == 3 || $_POST['c' . $i . '_s' . $j] == 8 || $_POST['c' . $i . '_s' . $j] == 4) {
                            if ($xid == 1) {
                                $identificado = '' . $nome . '';
                                $xid = 2;
                            } else {
                                if ($update == '') {
                                    $update.="`" . $nome . '`=%s';
                                    $insert.="%s";
                                    $cinsert.="`" . $nome . '`';
                                } else {
                                    $update.=",`" . $nome . '`=%s';
                                    $insert.=",%s";
                                    $cinsert.=",`" . $nome . '`';
                                }
                            }
                        }
                    }
                }


                $limt = $_POST['qtdFk' . $i];                $limt=100;

                for ($j = 1; $j <= $limt; $j++) {
                    if (isset($_POST['c' . $i . '_PK' . $j . 'id'])) {

                        $mod = $_POST['c' . $i . '_PK' . $j . 'm'];

                        $t = $_POST['c' . $i . '_PK' . $j . 'id'];
                        $a = explode('_', $t);
                        $a = explode('c', $a[0]);
                        $nome = $classe . $_POST['x' . $a[1]] . $_POST[$t] . $j;

                        if (isset($_POST[$t])) {

                            if ($mod == 1 || $mod == 2) {
                                if ($aux1 == 0) {
                                    $aux1 = 1;
                                } else {
                                    $posts.=', $_POST["' . $nome . '"]';
                                }

                                $objs.=', $this->rs[$i]["' . $nome . '"]';
                                if ($update == '') {
                                    $update.="`" . $nome . '`=%s';
                                    $insert.="%s";
                                    $cinsert.="`" . $nome . '`';
                                } else {
                                    $update.=",`" . $nome . '`=%s';
                                    $insert.=",%s";
                                    $cinsert.=",`" . $nome . '`';
                                }
                            }
                        }
                    }
                }

                if ($busc ==''){
                    $busc = $identificado;
                }
                $mod = '<?php
/**
 * Description of 
 *
 * @author Artur Romão Rocha
 */
require "model/' . $classe . '/' . $classe . '.php";

class m' . $classe . ' extends ' . $classe . ' {

    private $rs; 
    private $tipo;
    public $all;
    private $tabela = "' . strtolower($classe) . '";   
    public $campochave = "' . $busc . '";  
    public $campochaveID = "' . @$identificado . '"; 
    private $campoupdate = "' . $update . '";
   
    private $campoInsert = "' . $cinsert . '";  
    private $campoInsertValor = "' . $insert . '";  

    public function __construct() {
        $this->maxTipoPage = 40; 
    }

    public function add() {
        $tabela = $this->tabela;
        $campo = $this->campoInsert;
        $this->setCampoValor();

        $valor = $this->campoInsertValor;

        $this->adicionar($tabela, $campo, $valor);
    }

    public function del($id) {
        $tabela = $this->tabela;
        $campochaveID = $this->campochaveID;
        $this->deletar($id, $tabela, $campochaveID);
    }

    public function up($id) {
        $tabela = $this->tabela;
        $campoChaveID = $this->campochaveID;
        $this->valorUpdade();
        $valores = $this->campoupdate;

        $this->editar($tabela, $valores, $id, $campoChaveID);
    }

    public function ls($id) {
        $tabela = $this->tabela;
        $campochaveID = $this->campochaveID;
        $this->rs = $this->listarPorId($id, $tabela, $campochaveID);
        $this->criarobjeto();
        return $this->all;
    }

    public function lst($l=true) {
        $tabela = $this->tabela;
        $campochave = $this->campochave;
        $this->rs = $this->listaTodos($tabela, $campochave,$l);
        $this->criarobjeto();
        return $this->all;
    }
    

    public function lstU() {
        $tabela = $this->tabela;
        $campochaveID = $this->campochaveID;
        $sql="select * from $tabela order by $campochaveID desc;";
        $this->rs = $this->RunSelect($sql);     
        return $this->rs[0]["$campochaveID"];
    }

    public function lstall() {
        $tabela = $this->tabela;
        $campochaveID = $this->campochaveID;
        $sql="select * from $tabela;";
        $this->rs = $this->RunSelect($sql);   
        $this->criarobjeto();
        return $this->all;
    }

    public function lstOr($id) {
        $tabela = $this->tabela;
        $campochave = $this->campochaveID;
        $this->rs = $this->listaOr($id,$tabela, $campochave);
        $this->criarobjeto();
        return $this->all;
    }
  
    public function delFk($id,$tabela,$campo) {
                $this->deletar($id, $tabela, $campo);
    }
    
    public function addFk($tabela,$campo,$valor) {
          $this->adicionar($tabela, $campo, $valor);
    }
    


    private function valor($s) {
        $valor = $this->setCampoValor($s);
        $this->campoInsertValor = $valor;
    }

    private function valorUpdade() {
        $this->campoupdate = sprintf($this->campoupdate' . $posts . ');
        $this->campoupdate = $this->noSql($this->campoupdate);
    }

    public function criarobjeto() {
        $i = 0;
        foreach ($this->rs as $valor) {

            $this->all[$i] = new ' . $classe . '(
               ' . $objs . '
            );
            $i++;
        }
        $this->setTotal($i--);
    }

    public function getAll() {
        return $this->all;
    }

    public function setCampoValor() {
        // $valSimb = "1&*&1";
        $this->campoInsertValor = sprintf($this->campoInsertValor' . $posts . ');
        $this->campoInsertValor = $this->noSql($this->campoInsertValor);
    }
  
}
';
                $fp = fopen($file, 'a');
                $esc = fwrite($fp,  $mod . "\n");
                fclose($fp);
            }
        }
    }

}
