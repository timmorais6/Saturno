<?php

require "db/db.php";


$db = new bd(); 

$sql = "show tables
     ";
$txt = "  ";
$rs = $db->RunSelect($sql);


$xsql = "";
$xalt = "";
$xdados = "";
foreach ($rs as $rs) {
    $sql = "
           Show create table " . $rs['Tables_in_' . $db->getDatabase()];

    $e = $db->RunSelect($sql);
    $txt = $e[0]['Create Table'] . ';
                ';

    $xsql.= x($txt);
    $xalt.= y($txt, $e[0]['Table']);
    $xdados.= z($e[0]['Table']);
}

function x($sql) {

    //iniciando procedimento de fitagem de sql iguinorando os FK
    $inicio = '';
    $fim = '';
    $alter = "";

    if (strripos($sql, "CONSTRAINT")) {

        $txt = explode("CONSTRAINT", $sql);
        $inicio = $txt[0];

        $inicio = substr($inicio, 0, -4);
        $fim = explode(') ENGINE', $txt[count($txt) - 1]);
        $fim = ") ENGINE " . $fim[1];
        $sql = $inicio . $fim . " 
            ";
    }

    return $sql;
}

function y($sql, $tabela) {

    //iniciando procedimento de fitagem de sql iguinorando os FK
    $inicio = '';
    $fim = '';
    $alter = "";

    if (strripos($sql, "CONSTRAINT")) {

        $txt = explode("CONSTRAINT", $sql);
        $d = count($txt);
        $t = '';
        for ($i = 1; $i < $d; $i++) {
            $t.= $txt[$i];
        }

        $x = explode("`", $t);
        $c = "";
        $l = count($x);
        $QTDFK = ($l - 1) / 8;


        for ($i = 1; $i <= $QTDFK; $i+=8) {

            $c.="
                  ALTER TABLE `$tabela` ADD CONSTRAINT `$x[$i]` FOREIGN KEY (`" . $x[$i + 2] . "`) REFERENCES `" . $x[$i + 4] . "`(`" . $x[$i + 6] . "`);
                  
                  ";
        }
        return $c;
    }
}

function z($tabela) {
      $db = new bd();
    $sql = "select * from $tabela";
    $rsz = $db->RunSelect($sql);
    $dados = '';
    $d = '';

    if (count($rsz)) {
        $sql = "show COLUMNS from $tabela";
      
        $rs = $db->RunSelect($sql);
        $ins = "insert into `$tabela` (";
        $campo = '';
        $MS = '';
        foreach ($rs as $rs) {
            $campo.= "`" . $rs["Field"] . "`,";
            if (substr_count($rs['Type'], 'char') || substr_count($rs['Type'], 'text') || substr_count($rs['Type'], 'dat') || substr_count($rs['Type'], 'tim')|| substr_count($rs['Type'], 'blo')) {
                $MS[] = 1;
            } else {
                $MS[] = 0;
            }
        }


        foreach ($rsz as $rs) {
            $i = -1;
            foreach ($rs as $rss) {
                $i++;
                if ($MS[$i] == 1) {
                    $d.= '"' . str_replace('"', "'", $rss) . '",';
                } else {
                    $cc=$rss;
                    if($cc==''){
                        $cc = "NULL";
                    }
                    $d.= $cc . ',';
                }
            }
            $dados.='
            (' . substr($d, 0, -1) . "),";
            $d = '';
        }
        $dados = substr($dados, 0, -1) . ";
         ";



        return $ins = $ins . substr($campo, 0, -1) . ") VALUES " . $dados;
    }
}

$txt = $xsql;

$nNome = date("d-m-Y-H-i-s") . "-backup.sql";


header('Content-Description: File Transfer');
header('Content-Disposition: attachment; filename="' . $nNome . '"');
header('Content-Type: application/octet-stream');
header('Content-Transfer-Encoding: binary');
header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
header('Pragma: public');
header('Expires: 0');
$txts = str_replace("MyISAM", "InnoDB", $txt);
echo $txt;
echo $xdados;
echo $xalt;
